<?php
$protocol = 'http';
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on"){
    $protocol = 'https';
}


if(isset($_SERVER['APPLICATION_ENV']) && $_SERVER['APPLICATION_ENV'] == "local"){
    $data = [
        "host_static" => $protocol . '://localhost:4050/api',
        // "host_static" => $protocol . '://42.119.252.91:4050/api',
        "add_on_path" => public_path()."\images\avatars\\",
        "image_patch" => public_path()."\images\avatars\\",
        "add_on_page" => "_page_path",
    ];
} else if(isset($_SERVER['APP_ENV']) && $_SERVER['APP_ENV'] == "staging"){
    $data = [
        "host_static" => $protocol . '://localhost:4050/api',
        "add_on_path" => "/mnt/playstatic/static/eball/",
        "image_patch" => public_path()."/images/avatars/",
        "add_on_page" => "_page_path",
    ];
} else{
    $data = [
        "host_static" => 'http://localhost:4050/api',
        "add_on_path" => "https://static.fptplay.net/static/img/daily/",
        "image_patch" => "/mnt/playstatic/static/daily/",
        "add_on_page" => "_page_path",
    ];
}

return $data;