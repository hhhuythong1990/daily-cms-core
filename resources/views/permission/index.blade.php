@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Danh sách {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="overlay"></div>
                <div class="loading-img"></div>
                <div class="col-xs-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div>
                </div>
                @if(isset($list_permission))
                    <?php 
                        $is_created = in_array(Utilities::constantPermissions()["PERMISSION_CREATE"], $list_permission);
                        $is_viewed = in_array(Utilities::constantPermissions()["PERMISSION_VIEW"], $list_permission);
                        $is_edited = in_array(Utilities::constantPermissions()["PERMISSION_EDIT"], $list_permission);                        
                        $is_deleted = in_array(Utilities::constantPermissions()["PERMISSION_DELETE"], $list_permission);
                        
                    ?>
                    <div class="clearfix">
                        <div class="pull-right tableTools-container">                            
                            @if($is_created)
                                <a href="{{ route('tao_moi_quyen_han_page_path') }}" class="btn btn-white btn-info btn-bold">
                                    <i class="ace-icon fa fa-pencil bigger-120 blue"></i> Tạo mới quyền hạn
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="pull-right tableTools-container">                            
                            @if($is_viewed)
                            <form method="POST" action="{{ route('quyen_han_page_path') }}">
                                <div class="input-group">
                                    <div class="input-group">          
                                        {{ csrf_field() }}
                                        <input placeholder="Tên quyền hạn" type="text" class="form-control" name="permission_name" id="txtSearch" value="{{ $search_data }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-info no-radius search" type="submit">
                                                <i class="ace-icon fa fa-search"></i>   
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                            @endif
                        </div>
                    </div>
                    
                    @if($is_viewed)
                        <table id="table_detail" class="table table-bordered table-hover">
                            <thead class="thin-border-bottom">
                                <tr>
                                    <th>#</th>
                                    <th>Định danh</th>
                                    <th>Tên quyền hạn</th>
                                    <th>Trạng thái</th>
                                    <th>Thời gian tạo</th>
                                    <th>Thời giàn cập nhật</th>
                                    <th>Người tạo</th>
                                    <th>Người cập nhật</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($data))
                                    @foreach($data as $index => $permission)
                                        <tr>
                                            <td> {{ ++$index }} </td>
                                            <td> {{ $permission->_id }} </td>
                                            <td> {{ $permission->permission_name }} </td>
                                            <td> {{ ($permission->is_publish)?"Công khai":"Không công khai" }} </td>
                                            <td> {{ $permission->day_create_string }} </td>
                                            <td> {{ $permission->day_update_string }} </td>
                                            <td> {{ $permission->user_create->full_name}}</td>
                                            <td> {{ (isset($permission->user_update)) ? $permission->user_update->full_name : "" }}</td>
                                            <td>
                                                @if($is_edited)
                                                    <a href="{{route('cap_nhat_quyen_han_page_path',['id' => $permission->_id ])}}" class="btn btn-white btn-warning btn-xs"><i class="ace-icon fa fa-wrench  bigger-110 icon-only"></i></a>  
                                                @endif
                                                &nbsp;
                                                @if($is_deleted)
                                                    <button data-toggle="modal" data-target="#myModal" onclick="storeId('{{ $permission->_id }}')" class="btn btn-white btn-danger btn-xs">
                                                        <i class="ace-icon fa fa-trash-o  bigger-110 icon-only"></i>
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif                                       
                            </tbody>
                        </table>
                        <div class="clearfix pull-right">
                            @include("layouts.pagination", ["pages" => $pages])
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>
    @include("layouts.confirm", ["title"=> "mô-dun"])
    <script>
        $('p.alert').delay(5000).slideUp();
        $daily.paging_item_click("{{ route('danh_sach_quyen_han_theo_so_trang_path') }}", "{{ $search_data }}");

        @if($is_deleted)
            function storeId(id){
                $daily.store_id(id);
            }
            $daily.delete_item("{{ route('xoa_quyen_han_path') }}");
        @endif
        
    </script>
@stop