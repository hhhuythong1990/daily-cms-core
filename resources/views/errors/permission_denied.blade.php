@extends('home')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <div class="error-container">
                    <div class="well">
                        <h1 class="grey lighter smaller">
                            <span class="blue bigger-125">
                                <i class="ace-icon fa fa-random"></i>
                                404
                            </span>
                            Rất tiếc bạn không có được cấp quyền vào trang này!!
                        </h1>    
                    </div>                
                </div>
            </div>
        </div>
    </div>    
@stop