<?php
use \App\Helpers\Utilities;
?>
@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Danh sách {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="overlay"></div>
                <div class="loading-img"></div>
                <div class="col-xs-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div>
                </div>
                @if(isset($list_permission))
                    <?php 
                        $is_viewed = in_array(Utilities::constantPermissions()["REVENUE_VIEW"], $list_permission);
                        $is_searched = in_array(Utilities::constantPermissions()["REVENUE_SEARCH"], $list_permission);
                        $is_exported = in_array(Utilities::constantPermissions()["REVENUE_EXPORT"], $list_permission);
                    ?>
                    <form class="form-horizontal" role="form" action="{{ route('tim_kiem_doanh_thu_page_path') }}" method="POST" autocomplete="off">
                        {{ csrf_field() }}
                        @if($is_exported || $is_searched)
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="agency">Mã đại lý</label>
                            <input class="input-sm" type="text" name="agency_name" placeholder="Mã đại lý" 
                            value="{{ ($search_data['agency_name']!='')?$search_data['agency_name']:'' }}">
                        </div>
                        @endif
                        @if($is_exported)
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="agency">Thời gian tìm kiếm từ</label>
                            <input class="input-sm date-picker" type="text" name="date_start" data-date-format="dd-mm-yyyy" placeholder="Thời gian tìm kiếm" 
                            value="{{ ($search_data['date_start']!='')?$search_data['date_start']:'' }}">
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="agency">Đến ngày</label>
                            <input class="input-sm date-picker" type="text" name="date_end" data-date-format="dd-mm-yyyy" placeholder="Thời gian tìm kiếm" 
                            value="{{ ($search_data['date_end']!='')?$search_data['date_end']:'' }}">
                        </div>
                        @endif
                        <div class="form-group">
                            <div class="col-sm-offset-4">
                                <button type="submit" class="btn btn-primary">TÌm kiếm</button>
                            </div>
                        </div>
                    </form>
                    <div class="clearfix">
                        <div class="pull-right tableTools-container">                            
                            @if($is_exported)
                            <form method="POST" action="{{route('bao_cao_doanh_thu_path')}}" autocomplete="off">
                                <div class="input-group">
                                    <div class="input-group">          
                                        {{ csrf_field() }}
                                        <input type="hidden" class="form-control" name="agency_name" value="{{ ($search_data['agency_name']!='')?$search_data['agency_name']:'' }}">
                                        <input type="hidden" class="form-control" name="date_start" value="{{ ($search_data['date_start']!='')?$search_data['date_start']:'' }}">
                                        <input type="hidden" class="form-control" name="date_end" value="{{ ($search_data['date_end']!='')?$search_data['date_end']:'' }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-success no-radius search" type="submit">
                                                <i class="ace-icon fa fa-download"></i> Báo cáo 
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                            @endif
                        </div>
                    </div>
                    @if($is_viewed)
                        <table id="table_detail" class="table table-bordered table-hover">
                            <thead class="thin-border-bottom">
                                <tr>
                                    <th>#</th>
                                    <th>Mã giao dịch</th>
                                    <th>Mã đại lý</th>
                                    <th>Tên gói</th>
                                    <th>Tiền gốc</th>
                                    <th>Chiết khấu</th>
                                    <th>Số code tạo</th>
                                    <th>Tiền trả</th>
                                    <th>Tiền chênh lệch</th>
                                    <th>Ngày tạo</th>
                                    <th>Người tạo</th>
                                    <th class="center">Chi tiết</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($data->listPaid))
                                    @foreach($data->listPaid as $index => $value)
                                        <tr>
                                            <td> {{ ++$index }} </td>
                                            <td> {{ $value->transaction }} </td>
                                            <td> {{ $value->agency_name }} </td>
                                            <td> {{ $value->agency_package_name }} </td>
                                            <td> {{ Utilities::convertVND($value->price_original_paid, 0, "VNĐ") }} </td>
                                            <td> {{ $value->price_cycle }} </td>
                                            <td> {{ $value->amount_code_created }} </td>
                                            <td> {{ Utilities::convertVND($value->price_paid, 0, "VNĐ") }} </td>
                                            <td> {{Utilities::convertVND($value->price_paid_difference, 0, "VNĐ") }} </td>
                                            <td> {{ $value->day_create_string }} </td>
                                            <td> {{ $value->user_create->full_name }} </td>
                                            <td class="center">
                                                <div class="action-buttons">
                                                    <a href="#" class="green bigger-140 show-details-btn" title="Show Details">
                                                        <i class="ace-icon fa fa-angle-double-up"></i>
                                                        <span class="sr-only">Details</span>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @if(!empty($value->gift_code))
                                        <tr class="detail-row">
                                            <td colspan="12">
                                                <table class="table table-bordered table-hover">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Mã PIN</th>
                                                        <th>Serial</th>
                                                        <th>Thời gian tạo</th>
                                                        <th>Thời gian hết hạn</th>
                                                        {{-- <th>Trạng thái</th> --}}
                                                    </tr>
                                                    @foreach($value->gift_code as $i => $v)
                                                        <tr>
                                                            <td>{{++$i}}</td>
                                                            <td>{{$v->pin_code}}</td>
                                                            <td>{{$v->serial}}</td>
                                                            <td>{{$v->created_date}}</td>
                                                            <td>{{$v->expired_date}}</td>
                                                            {{-- <td>{{($v->active_status)?"Đã kích hoạt":"Chưa kích hoạt" }} </td> --}}
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                @endif                                                        
                            </tbody>
                            <tbody>
                                @if(isset($data->sumPaid))
                                    @foreach($data->sumPaid as $index => $item)
                                        <tr>
                                            <td class="center" colspan="6"><b>Tổng doanh thu</b></td>
                                            <td><b> {{ $item->totalAmount }} </b></td>
                                            <td><b> {{ Utilities::convertVND($item->totalPaid, 0, "VNĐ") }} </b></td>
                                            <td><b> {{ Utilities::convertVND($item->totalDifference, 0, "VNĐ") }} </b></td>
                                            <td colspan="3"></td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        <div class="clearfix pull-right">      
                            @include("layouts.pagination", ["pages" => $pages])
                        </div>
                    @endif    
                @endif    
            </div>
        </div>
    </div>
    @include("layouts.confirm", ["title"=> "mô-dun"])
    <script>
        $('p.alert').delay(5000).slideUp();
        
        @if($is_searched || $is_exported)
            $("input[name='agency_name']").keyup(function() {
                this.value = this.value.toLocaleUpperCase();
            });
        @endif

        @if($is_exported)
            $('.date-picker').datepicker({
                todayHighlight: true,
                autoclose: true,
                clearBtn: true,
            });
            $('.date-picker').on('keypress paste', function (e) {
                e.preventDefault();
                return false;
            });
        @endif

        @if($is_viewed)
            $daily.open_detail();
            $daily.paging_item("{{ route('quan_ly_doanh_thu_theo_so_trang_path') }}", 
                "{{ ($search_data['agency_name']!='')?$search_data['agency_name']:'' }}",
                "{{ ($search_data['date_start']!='')?$search_data['date_start']:'' }}",
                "{{ ($search_data['date_end']!='')?$search_data['date_end']:'' }}");
        @endif
    </script>
@stop