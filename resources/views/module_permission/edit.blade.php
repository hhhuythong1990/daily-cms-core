@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Cập nhật {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="overlay"></div>
            <div class="loading-img"></div>
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
            @if(isset($list_permission))
                <?php 
                    $is_edited = in_array(Utilities::constantPermissions()["MODULE_PERMISSION_EDIT"], $list_permission);    
                ?>
                @if($is_edited)
                    @if(isset($data))
                        <form id="moduleForm" class="form-horizontal" role="form" action="{{ route('cap_nhat_mo_dun_va_quyen_han_path') }}" method="POST">     
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="_id" value="{{ $data->_id }}" />
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="module">Tên mô-đun và quyền hạn</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="module_permission" placeholder="Tên mô-đun và quyền hạn" value="{{$data->module_permission_name}}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="user_name">Mô-đun</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="user_name" placeholder="Nhập tên đăng nhập" value="{{$data->module->module_name}}" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="permissions">Quyền hạn</label>
                                <div class="col-sm-5">
                                    <select class="form-control" multiple="multiple" name="permissions[]" style="height:350px">                            
                                        @if(!empty($permissions))  
                                            @foreach($permissions as $permission)
                                                <?php       
                                                    $selected = "";
                                                    foreach($data->permissions as $selectPermission){
                                                        if($selectPermission->_id == $permission->_id){
                                                            $selected = "selected";
                                                        }
                                                    }
                                                ?>
                                                <option value="{{ $permission->_id }}"  {{ $selected }} >{{ $permission->permission_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="publish">Công bố</label>
                                <div class="col-sm-5">
                                    <select class="form-control" name="publish">
                                        <option value="">Chọn loại công bố</option>
                                        <option value="true" {{ ($data->is_publish == true)?'selected':'' }}>Hiển thị</option>
                                        <option value="false" {{ ($data->is_publish == false)?'selected':'' }}>Ẩn hiển thị</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4">
                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                </div>
                            </div>
                        </form>        
                    @endif
                @endif
            @endif
        </div>
    </div>
    <script>
        $('p.alert').delay(5000).slideUp();
    </script>
@stop