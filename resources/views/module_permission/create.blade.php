@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Tạo mới {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="overlay"></div>
            <div class="loading-img"></div>
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
            <form id="createForm" class="form-horizontal" role="form" method="POST" action="{{route('tao_moi_mo_dun_va_quyen_han_path')}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="module">Tên mô-đun và quyền hạn</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="module_permission" placeholder="Tên mô-đun và quyền hạn" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="module">Mô-đun</label>
                    <div class="col-sm-5">
                        <select class="form-control" name="module">       
                            <option value="">Chọn mô-đun</option>                     
                            @if(!empty($modules))  
                                @foreach($modules as $module)
                                    <option value="{{ $module->_id }}">{{ $module->module_name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="permissions">Quyền hạn</label>
                    <div class="col-sm-5">
                        <select class="form-control" multiple="multiple" name="permissions[]" style="height:350px">                            
                            @if(!empty($permissions))  
                                @foreach($permissions as $permission)
                                    <option value="{{ $permission->_id }}">{{ $permission->permission_name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="publish">Công bố</label>
                    <div class="col-sm-5">
                        <select class="form-control" name="publish">
                            <option value="">Chọn loại công bố</option>
                            <option value="true">Hiển thị</option>
                            <option value="false">Ẩn hiển thị</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4">
                        <button type="submit" class="btn btn-primary" >Tạo mới</button>
                    </div>
                </div>
            </form>        
        </div>
    </div>
    <script>
        $('p.alert').delay(5000).slideUp();
    </script>
@stop