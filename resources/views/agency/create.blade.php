@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Tạo mới {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="overlay"></div>
            <div class="loading-img"></div>
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
            <form id="createForm" class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('tao_moi_dai_ly_path') }}">
                {{ csrf_field() }}                
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="full_name">Tên đại lý</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="full_name" placeholder="Nhập tên đại lý" >
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="user_name">Tên đăng nhập</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="user_name" placeholder="Nhập tên đăng nhập" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="agency_code">Mã đại lý</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="agency_code" placeholder="Nhập mã đại lý" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="password">Mật khẩu</label>
                    <div class="col-sm-5">
                        <input minlength=8 maxlength=16 type="password" class="form-control" name="password" placeholder="Nhập mật khẩu" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="modules">Nhóm mô-đun</label>
                    <div class="col-sm-5">
                        @if(!empty($groupModules))  
                            <ul class="item-list ui-sortable">
                                @foreach($groupModules as $groupModule)
                                    <?php 
                                        $page = null;
                                        foreach($groupModule->modules as $key => $value){
                                            $page .=" - Trang " . strtolower($value->module_name);
                                        }
                                    ?>
                                    <li class="item-orange clearfix ui-sortable-handle">
                                        <span>
                                        {{ $groupModule->group_module_description }} {{ $page }}
                                        </span>
                                        <span class="pull-right">
                                            <input type="checkbox" name="group_modules[]" checked value="{{ $groupModule->_id }}">
                                        </span>
                                    </tr>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="permissions">Quyền hạn</label>
                    <div class="col-sm-5">
                        @if(!empty($permissions))
                            <ul class="item-list ui-sortable">
                                @foreach($permissions as $permission)
                                    <li class="item-red clearfix ui-sortable-handle">
                                        <strong>{{ $permission->module->module_name }}</strong>
                                    </li>
                                    @foreach($permission->permissions as $subpermission)
                                        <li class="item-default clearfix ui-sortable-handle">
                                            <span>
                                                {{ $subpermission->permission_name }}
                                            </span>
                                            <span class="pull-right">
                                                <input type="checkbox" name="permissions[]" checked value="{{ $subpermission->_id }}">
                                            </span>
                                        </li>
                                    @endforeach
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="full_name_represent">Họ và tên đại diện</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="full_name_represent" placeholder="Nhập họ &amp; tên đại diện" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="id_card">Số CMND</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="id_card" placeholder="Nhập CMND" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="image">Hình ảnh xác thực CMND</label>
                    <div class="col-sm-2">
                        <input type="file" class="form-control" name="txt_id_card_front" placeholder="Mặt trước CMND" onchange="onFileChange('imgIdCardFront', 'card_front',event)" />
                        <input type="hidden" id="card_front" name="card_front" value="" />
                        <br />
                        <img id="imgIdCardFront" style="width: 100%;"/>                        
                    </div>
                    <div class="col-sm-2">
                        <input type="file" class="form-control" name="txt_id_card_behind" placeholder="Mặt sau CMND" onchange="onFileChange('imgIdCardBehind', 'card_behind', event)" />
                        <input type="hidden" id="card_behind" name="card_behind" value="" />
                        <br />
                        <img id="imgIdCardBehind" style="width: 100%;"/>                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="phone">Số điện thoại</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="phone" placeholder="Nhập số điện thoại" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="email">Email</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="email" placeholder="Nhập email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="address">Địa chỉ liên lạc</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="address" placeholder="Nhập địa chỉ liên lạc" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="payment_limit">Hạn mức thanh toán</label>
                    <div class="col-sm-5">
                        <input id="payment_limit" type="text" class="form-control" name="payment_limit" placeholder="Nhập hạn mức thanh toán" step="1" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4">
                        <button type="submit" class="btn btn-primary" >Tạo mới đại lý</button>
                    </div>
                </div>
            </form>        
        </div>
    </div>
    <script>
        $('p.alert').delay(5000).slideUp();
        function onFileChange(idImg, idHiddern, event) {
            let fileReader = new FileReader();
            let files = event.target.files || event.dataTransfer.files;
            
            if (!files.length)
                return;
            fileReader.readAsDataURL(files[0]);
            fileReader.onload = function (event) {
                document.getElementById(idImg).src = event.target.result;
                document.getElementById(idHiddern).value = event.target.result;
            };
        }

        $("input[name='agency_code']").keyup(function() {
            this.value = this.value.toLocaleUpperCase();
        });

        $("input[name='user_name']").keyup(function() {
            this.value = this.value.toLocaleLowerCase();
        });

        $("input[name='payment_limit'], input[name='phone'], input[name='id_card'],  input[name='id_card']").keydown(function (e) {
            console.log(e.keyCode);
            if (!(
                // Allow: backspace, tab, enter, esc, end, home, left, right, del, point, period
                $.inArray(e.keyCode, [8, 9, 13, 27, 35, 36, 37, 39, 46]) !== -1 ||
                // Allow: Ctrl+A, Ctrl+C, Ctrl+X
                ((e.ctrlKey || e.metaKey) && $.inArray(e.keyCode, [65, 67, 88]) !== -1) ||
                // Allow: keys 0-9
                (!e.shiftKey && e.keyCode >= 48 && e.keyCode <= 57) ||
                // Allow: numpad 0-9
                (e.keyCode >= 96 && e.keyCode <= 105))) {
                    // Disallow: every other keypress
                    e.preventDefault();
            }
        });
        
        // $("#createForm").submit(function(e) {
        //     e.preventDefault();
        // }).validate({
        //     rules: {
        //         full_name: "required",
        //         user_name: {
        //             "required": true,
        //             "remote": {
        //                 url: "{{ route('kiem_tra_ten_dang_nhap_path') }}",
        //                 type: "POST",
        //                 dataType: "json",
        //                 data: {
        //                     user_name: function() {
        //                         return $("input[name='user_name']").val();
        //                     }
        //                 },
        //                 beforeSend: function(xhr){
        //                     xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
        //                 }
        //             }
        //         },
        //         agency_code: {
        //             "required": true,
        //             "remote": {
        //                 url: "{{ route('kiem_tra_ma_dai_ly_path') }}",
        //                 type: "POST",
        //                 dataType: "json",
        //                 data: {
        //                     agency_code: function() {
        //                         return $("input[name='agency_code']").val();
        //                     }
        //                 },
        //                 beforeSend: function(xhr){
        //                     xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
        //                 }
        //             }
        //         },
        //         password: "required",
        //         group_modules: "required",
        //         permissions: "required",
        //         full_name_represent: "required",
        //         id_card: "required",
        //         txt_id_card_front: "required",
        //         txt_id_card_behind: "required",
        //         phone: "required",
        //         email: {
        //             required: true,
        //             email: true
        //         },
        //         address: "required",
        //         payment_limit: "required"
        //     },
        //     messages: {
        //         full_name: "Họ và tên không được để trống",
        //         user_name: {
        //             required: "Tên đăng nhập không được để trống",
        //             remote: "Tên đăng nhập đã tồn tại",
        //         },
        //         agency_code: {
        //             required: "Mã đại lý không được để trống",
        //             remote: "Mã đại lý đã tồn tại",
        //         },
        //         password: "Mật khẩu không được để trống",
        //         group_modules: "Nhóm mô-đun phải được chọn",
        //         permissions: "Quyền hạn phải được chọn",
        //         full_name_represent: "Họ và tên người đại diện không được để trống",
        //         id_card: "Số chứng minh nhân dân không được để trống",
        //         txt_id_card_front: "Hình chứng minh mặt trước không được để trống",
        //         txt_id_card_behind: "Hình chứng minh mặt sau không được để trống",
        //         phone: "Số điện thoại không được để trống",
        //         email: {
        //             required: "Email không được để trống",
        //             email: "Email sai định dạng"
        //         },
        //         address: "Địa chỉ không được để trống",
        //         payment_limit: "Hạn mức không được để trống"
        //     },
        //     highlight: function(element) {
        //         $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        //     },
        //     unhighlight: function(element) {
        //         $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        //     },
        //     errorElement: 'span',
        //     errorClass: 'help-block',
        //     errorPlacement: function(error, element) {
        //         if(element.parent('.input-group').length) {
        //             error.insertAfter(element.parent());
        //         } else {
        //             error.insertAfter(element);
        //         }
        //     },
        //     submitHandler: function(form) {
        //         $daily.loading_waiting("show");
        //         form.submit();
        //         // var dataForm = {
        //         //     "full_name": $("input[name='full_name']").val(),
        //         //     "user_name": $("input[name='user_name']").val(),
        //         //     "agency_code": $("input[name='agency_code']").val(),
        //         //     "password": $("input[name='password']").val(),
        //         //     "group_modules": $("select[name='group_modules']").val(),
        //         //     "group_user": $("select[name='group_user']").val(),
        //         //     "permissions": $("select[name='permissions']").val(),
        //         //     "full_name_represent": $("input[name='full_name_represent']").val(),
        //         //     "id_card": $("input[name='id_card']").val(),
        //         //     "image_id_card_front": document.getElementById("imgIdCardFront").src,
        //         //     "image_id_card_behind": document.getElementById("imgIdCardBehind").src,
        //         //     "phone": $("input[name='phone']").val(),
        //         //     "email": $("input[name='email']").val(),
        //         //     "address": $("input[name='address']").val(),
        //         //     "payment_limit": $("input[name='payment_limit']").val(),
        //         // };
        //         // console.log(dataForm);
        //         // $.ajax({
        //         //     url: "{{ route('tao_moi_dai_ly_path') }}",
        //         //     type: "POST",
        //         //     data: dataForm, 
        //         //     beforeSend: function(xhr){
        //         //         xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
        //         //     },
        //         //     success: function(response) {
        //         //         if(response.status == 200 && response.refresh == true){
        //         //             location.reload(); 
        //         //         }      
        //         //     },
        //         //     error: function(err) {
        //         //         console.log(err)
        //         //     }
        //         // });
        //     }
        // });
    </script>
@stop
