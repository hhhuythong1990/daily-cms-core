@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Tạo mới {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="overlay"></div>
            <div class="loading-img"></div>
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
            <form id="createForm" class="form-horizontal" role="form">  
                <input type="hidden" class="form-control" name="agency_id" value="{{$data->is_agency->_id}}">
                <input type="hidden" class="form-control" name="profile_id" value="{{$data->_id}}">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="full_name">Tên đại lý</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="full_name" placeholder="Nhập tên đại lý" value="{{$data->full_name}}">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="user_name">Tên đăng nhập</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="user_name" placeholder="Nhập tên đăng nhập" value="{{$data->user_name}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="password">Mật khẩu đăng nhập</label>
                    <div class="col-sm-5">
                        <input minlength=8 maxlength=16 type="password" class="form-control" name="password" placeholder="Nhập mật khẩu đăng nhập">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="agency_code">Mã đại lý</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" disabled placeholder="Nhập mã đại lý" value="{{$data->is_agency->agency_code}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="modules">Nhóm mô-đun</label>
                    <div class="col-sm-5">                           
                        @if(!empty($groupModules)) 
                            <ul class="item-list ui-sortable">
                                @foreach($groupModules as $groupModule)
                                <?php 
                                    $page = null;
                                    $selected = "";
                                    foreach($groupModule->modules as $key => $value){
                                        $page .=" - Trang " . strtolower($value->module_name);
                                    }

                                    foreach($data->group_modules as $selectGroupModule){
                                        if($selectGroupModule->_id == $groupModule->_id){
                                            $selected = "checked";
                                        }
                                    }
                                ?>
                                    <li class="item-orange clearfix ui-sortable-handle">
                                        <span>
                                            {{ $groupModule->group_module_description }} {{ $page }}
                                        </span>
                                        <span class="pull-right">
                                            <input type="checkbox" name="group_modules" {{ $selected }} value="{{ $groupModule->_id }}"> 
                                        </span>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="permissions">Quyền hạn</label>
                    <div class="col-sm-5">                        
                        @if(!empty($permissions))
                            <ul class="item-list ui-sortable">
                                @foreach($permissions as $permission)
                                    <li class="item-red clearfix ui-sortable-handle">
                                        <strong>{{ $permission->module->module_name }}</strong>
                                    </li>
                                        @foreach($permission->permissions as $subpermission)
                                        <?php       
                                            $selected = "";
                                            foreach($data->permissions as $selectPermission){
                                                if($selectPermission->_id == $subpermission->_id){
                                                    $selected = "checked";
                                                }
                                            }
                                        ?>
                                        <li class="item-default clearfix ui-sortable-handle">
                                            <span>
                                                </i> {{ $subpermission->permission_name }}
                                            </span>
                                            <span class="pull-right">
                                                <input type="checkbox" name="permissions" {{ $selected }} value="{{ $subpermission->_id }}">
                                            </span>
                                        </li>
                                        @endforeach
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="full_name_represent">Họ và tên đại diện</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="full_name_represent" placeholder="Nhập họ &amp; tên đại diện" value="{{$data->is_agency->full_name_represent}}" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="id_card">Số CMND</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="id_card" placeholder="Nhập CMND" value="{{$data->is_agency->id_card}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="image">Hình ảnh xác thực CMND</label>
                    <div class="col-sm-2">
                        <input type="file" class="form-control" name="txt_id_card_front" placeholder="Mặt trước CMND" 
                            onchange="onFileChange('imgIdCardFront', event)" />
                        <br />
                        <img id="imgIdCardFront" src="{{$data->is_agency->image_id_card_front}}" style="width: 100%;"/>
                    </div>
                    <div class="col-sm-2">
                        <input type="file" class="form-control" name="txt_id_card_behind" placeholder="Mặt sau CMND"
                            onchange="onFileChange('imgIdCardBehind', event)"/>
                        <br />
                        <img id="imgIdCardBehind" src="{{$data->is_agency->image_id_card_behind}}" style="width: 100%;"/>                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="phone">Số điện thoại</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="phone" placeholder="Nhập số điện thoại" value="{{$data->is_agency->phone}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="email">Email</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="email" placeholder="Nhập email" value="{{$data->is_agency->email}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="address">Địa chỉ liên lạc</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="address" placeholder="Nhập địa chỉ liên lạc" value="{{$data->is_agency->address}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="payment_limit">Hạn mức thanh toán</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="payment_limit" placeholder="Nhập hạn mức thanh toán" value="{{$data->is_agency->payment_limit}}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4">
                        <button type="submit" class="btn btn-primary" >Cập nhật đại lý</button>
                    </div>
                </div>
            </form>        
        </div>
    </div>
    <script>
        $("input[name='payment_limit'], input[name='phone'], input[name='id_card'],  input[name='id_card']").keydown(function (e) {
            if (!(
                // Allow: backspace, tab, enter, esc, end, home, left, right, del, point, period
                $.inArray(e.keyCode, [8, 9, 13, 27, 35, 36, 37, 39, 46]) !== -1 ||
                // Allow: Ctrl+A, Ctrl+C, Ctrl+X
                ((e.ctrlKey || e.metaKey) && $.inArray(e.keyCode, [65, 67, 88]) !== -1) ||
                // Allow: keys 0-9
                (!e.shiftKey && e.keyCode >= 48 && e.keyCode <= 57) ||
                // Allow: numpad 0-9
                (e.keyCode >= 96 && e.keyCode <= 105))) {
                    // Disallow: every other keypress
                    e.preventDefault();
                }
        });
        $('p.alert').delay(5000).slideUp();
        function onFileChange(idImg, event) {
            let fileReader = new FileReader();
            let files = event.target.files || event.dataTransfer.files;
            
            if (!files.length)
                return;
            fileReader.readAsDataURL(files[0]);
            fileReader.onload = function (event) {
                document.getElementById(idImg).src = event.target.result;
            };
        }
        // $("input[name='agency_code']").keyup(function() {
        //     this.value = this.value.toLocaleUpperCase();
        // });

        $("input[name='user_name']").keyup(function() {
            this.value = this.value.toLocaleLowerCase();
        });
        $("#createForm").submit(function(e) {
            e.preventDefault();
        }).validate({
            rules: {
                full_name: "required",
                user_name: "required",
                // agency_code: "required",
                group_modules: "required",
                permissions: "required",
                full_name_represent: "required",
                id_card: "required",
                phone: "required",
                email: "required",
                address: "required",
                payment_limit: "required"
            },
            messages: {
                full_name: "Họ và tên không được để trống",
                user_name: "Tên đăng nhập không được để trống",
                // agency_code: "Mã đại lý không được để trống",
                group_modules: "Nhóm mô-đun phải được chọn",
                permissions: "Quyền hạn phải được chọn",
                full_name_represent: "Họ và tên người đại diện không được để trống",
                id_card: "Số chứng minh nhân dân không được để trống",
                phone: "Số điện thoại không được để trống",
                email: "Email không được để trống",
                address: "Địa chỉ không được để trống",
                payment_limit: "Hạn mức không được để trống",
                password:  {
                    minlength: "Mật khẩu phải lớn hơn 8 kí tự"
                },
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element[0].type === 'checkbox') {
                    error.insertAfter(element.closest('.item-list'));
                }
                else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                $daily.loading_waiting("show");
                let permissionChecked = [];
                let groupModuleChecked = [];
                $("input:checkbox[name=permissions]:checked").each(function(){
                    permissionChecked.push($(this).val());
                });

                $("input:checkbox[name=group_modules]:checked").each(function(){
                    groupModuleChecked.push($(this).val());
                });
                $.ajax({
                    url: "{{ route('cap_nhat_dai_ly_path') }}",
                    type: "POST",
                    data: {
                        "profile_id": $("input[name='profile_id']").val(),
                        "agency_id": $("input[name='agency_id']").val(),
                        "full_name": $("input[name='full_name']").val(),
                        "user_name": $("input[name='user_name']").val(),
                        // "agency_code": $("input[name='agency_code']").val(),
                        "password": $("input[name='password']").val(),
                        "group_modules": groupModuleChecked,
                        "permissions": permissionChecked,
                        "full_name_represent": $("input[name='full_name_represent']").val(),
                        "id_card": $("input[name='id_card']").val(),
                        "image_id_card_front": document.getElementById("imgIdCardFront").src,
                        "image_id_card_behind": document.getElementById("imgIdCardBehind").src,
                        "phone": $("input[name='phone']").val(),
                        "email": $("input[name='email']").val(),
                        "address": $("input[name='address']").val(),
                        "payment_limit": $("input[name='payment_limit']").val()
                    },
                    dataType: "json",
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                    },
                    success: function(response) {
                        if(response.status == 200 && response.refresh == true){
                            location.reload(); 
                        }      
                    },
                    error: function(err) {
                        console.log(err)
                    }
                });
            }
        });
    </script>
@stop
