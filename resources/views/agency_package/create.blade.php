@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Tạo mới {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="overlay"></div>
            <div class="loading-img"></div>
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
            <form id="AgencyPackageForm" class="form-horizontal" role="form" method="POST" action="{{ route('tao_moi_dai_ly_va_goi_path') }}"  >
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input id="totalPackage" type="hidden" name="total_package" value="0">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="agency">Đại lý</label>
                    <div class="col-sm-5">
                        <select class="form-control" name="agency">
                            <option value="">Chọn đại lý</option>
                            @if(!empty($agencies)) 
                                @foreach($agencies as $key => $value)
                                <option value="{{$value->_id}}-{{$value->is_agency->agency_code}}">{{$value->full_name}} - {{$value->is_agency->agency_code}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="package">Gói - Chiết khấu - Kích hoạt</label>
                    <div class="col-sm-5" id="package">
                        <div class="row">
                            <div class="col-sm-6">
                                <select class="form-control" name="package">
                                    <option value="">Chọn gói</option>
                                    @if(!empty($packages)) 
                                        @foreach($packages as $key => $value)
                                        <option value="{{$value->_id}}-{{$value->package_name}}">{{$value->package_name}} - plan-id {{$value->plan_id}} - giá {{$value->package_price}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control cycle" name="cycle" placeholder="Nhập % chiết khấu"/>
                            </div>
                            <div class="col-sm-2">
                                Kích hoạt <input type="checkbox" value="true" name="agency_package_status">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4">
                        <button class="btn btn-success" onclick="createNewPackage(event)"><i class="fa fa-plus"></i> Thêm gói</button>
                        <button type="submit" class="btn btn-primary" >Tạo mới</button>
                    </div>
                </div>
            </form>        
        </div>
    </div>
    <script>
        inputNumber();

        var position = 1;
        function createNewPackage(event) {
            event.preventDefault();
            let addPackage = createElementDOM(position);
            let packageDOM = document.querySelector("#package");
            let brDOM = document.createElement("br");
            packageDOM.appendChild(brDOM);
            packageDOM.appendChild(addPackage);
            document.getElementById("totalPackage").value = position;
            position++;
            inputNumber();
        }

        function inputNumber () {
            $(".cycle").keydown(function (e) {
                if (!(
                    // Allow: backspace, tab, enter, esc, end, home, left, right, del, point, period
                    $.inArray(e.keyCode, [8, 9, 13, 27, 35, 36, 37, 39, 46]) !== -1 ||
                    // Allow: Ctrl+A, Ctrl+C, Ctrl+X
                    ((e.ctrlKey || e.metaKey) && $.inArray(e.keyCode, [65, 67, 88]) !== -1) ||
                    // Allow: keys 0-9
                    (!e.shiftKey && e.keyCode >= 48 && e.keyCode <= 57) ||
                    // Allow: numpad 0-9
                    (e.keyCode >= 96 && e.keyCode <= 105))) {
                        // Disallow: every other keypress
                        e.preventDefault();
                }
            });
        }

        function createElementDOM(position) {
            let dataPackage = {!! json_encode($packages) !!};

            let packageDOM = document.createElement("div");
            packageDOM.classList.add("row");
            let selectContainDOM = document.createElement("div");
            selectContainDOM.classList.add("col-sm-6");
            let selectDOM = document.createElement("select");
            selectDOM.classList.add("form-control");
            selectDOM.name = "package" + position;            
            let optionFirstDOM = document.createElement("option");
            optionFirstDOM.value = "";
            optionFirstDOM.text = "Chọn gói";
            selectDOM.add(optionFirstDOM, selectDOM.options[0]);
            dataPackage.forEach(function(element, index){
                let optionDOM = document.createElement("option");
                optionDOM.value = element._id+"-"+element.package_name;
                optionDOM.text = element.package_name +" - plan-id " + element.plan_id +" - giá " + element.package_price;
                selectDOM.add(optionDOM, selectDOM.options[index+1]);
            });
            selectContainDOM.appendChild(selectDOM);
            packageDOM.appendChild(selectContainDOM);

            let cycleContainDOM = document.createElement("div");
            cycleContainDOM.classList.add("col-sm-4");
            let cycleDOM = document.createElement("input");
            cycleDOM.classList.add("form-control");
            cycleDOM.classList.add("cycle");
            cycleDOM.name = "cycle" + position;
            cycleDOM.setAttribute("type", "text");
            cycleDOM.setAttribute("placeholder", "Nhập % chiết khấu");
            cycleContainDOM.appendChild(cycleDOM);
            packageDOM.appendChild(cycleContainDOM);

            let statusContainDOM = document.createElement("div");
            statusContainDOM.classList.add("col-sm-2");
            statusContainDOM.innerHTML = "Kích hoạt ";
            let statusDOM = document.createElement("input");
            statusDOM.name = "agency_package_status" + position;
            statusDOM.setAttribute("type", "checkbox");
            statusDOM.setAttribute("value", true);
            statusContainDOM.appendChild(statusDOM);
            packageDOM.appendChild(statusContainDOM);

            return packageDOM;
        }

        $("#AgencyPackageForm").submit(function(e) {
            e.preventDefault();
        }).validate({
            rules: {
                agency: "required",
                package: "required",
                cycle: "required"
            },
            messages: {
                agency: "Đại lý phải được chọn",
                package: "Gói phải được chọn",
                cycle: "Chiết khấu không được để trống"
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                $daily.loading_waiting("show");
                form.submit();
            }
        });
    </script>
@stop
