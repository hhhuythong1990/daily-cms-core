<?php
use \App\Helpers\Utilities;
?>
@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Danh sách {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="overlay"></div>
                <div class="loading-img"></div>
                <div class="col-xs-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div>
                </div>
                @if(isset($list_permission))
                    <?php 
                        $is_viewed = in_array(Utilities::constantPermissions()["AGENCY_PACKAGE_VIEW"], $list_permission);
                    ?>

                    <div class="clearfix">
                        <div class="pull-right tableTools-container">                            
                            @if($is_viewed)
                            <form method="POST" action="{{ route('tim_kiem_code_path') }}">
                                <div class="input-group">
                                    <div class="input-group">          
                                        {{ csrf_field() }}
                                        <input placeholder="Mã pin" type="text" class="form-control" name="code_id" value="{{ $search_data }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-info no-radius search" type="submit">
                                                <i class="ace-icon fa fa-search"></i>   
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                            @endif
                        </div>
                    </div>
                    @if($is_viewed)
                        <table id="table_detail" class="table table-bordered table-hover">
                            <thead class="thin-border-bottom">
                                <tr>
                                    <th>#</th>
                                    <th>Mã PIN</th>
                                    <th>Serial</th>
                                    <th>Thời gian tạo</th>
                                    <th>Thời gian hết hạn</th>
                                    <!--<th>Trạng thái</th>-->
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($data))
                                    @foreach($data as $index => $data)
                                        <tr>
                                            <td> {{ ++$index }} </td>
                                            <td> {{ $data->pin_code }} </td>
                                            <td> {{ $data->serial }} </td>
                                            <td> {{ $data->created_date }} </td>
                                            <td> {{ $data->expired_date }} </td>
                                            {{-- <td> {{ ($data->active_status)?"Đã kích hoạt":"Chưa kích hoạt" }} </td> --}}
                                        </tr>
                                    @endforeach
                                @endif                                                        
                            </tbody>
                        </table>
                        <div class="clearfix pull-right">      
                            @include("layouts.pagination", ["pages" => $pages])
                        </div>
                    @endif    
                @endif    
            </div>
        </div>
    </div>
    @include("layouts.confirm", ["title"=> "mô-dun"])
    <script>
        $('p.alert').delay(5000).slideUp();
        @if($is_viewed)
            $daily.paging_item_click("{{ route('code_theo_so_trang_path') }}", "{{ $search_data }}");
        @endif
    </script>
@stop