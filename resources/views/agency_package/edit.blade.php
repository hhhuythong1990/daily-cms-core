@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Tạo mới {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="overlay"></div>
            <div class="loading-img"></div>
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
            <form id="AgencyPackageForm" class="form-horizontal" role="form">           
                <input type="hidden" name="agency_package_id" value="{{ $data->_id }}" />     
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="agency">Đại lý</label>
                    <div class="col-sm-5">
                        <select class="form-control" name="agency">
                            <option value="">Chọn đại lý</option>
                            @if(!empty($agencies)) 
                                @foreach($agencies as $key => $value)
                                <option value="{{$value->_id}}-{{$value->is_agency->agency_code}}" @if($value->_id == $data->agency) selected @endif>{{$value->full_name}} - {{$value->is_agency->agency_code}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="package">Gói</label>
                    <div class="col-sm-5">
                        <select class="form-control" name="package">
                            <option value="">Chọn gói</option>
                            @if(!empty($packages)) 
                                @foreach($packages as $key => $value)
                                <option value="{{$value->_id}}-{{$value->package_name}}" @if($value->_id == $data->package) selected @endif>{{$value->package_name}} - plan-id {{$value->plan_id}} - giá {{$value->package_price}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="cycle">Chiết khấu</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="cycle" placeholder="Nhập % chiết khấu" value="{{$data->cycle}}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="agency_package_status">Trạng thái</label>
                    <div class="col-sm-5">
                        <select class="form-control" name="agency_package_status">
                            <option value="">Chọn trạng thái</option>
                            <option value="true" @if($data->agency_package_status == true) selected @endif>Kích hoạt</option>
                            <option value="false" @if($data->agency_package_status == false) selected @endif>Vô hiệu hóa</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4">
                        <button type="submit" class="btn btn-primary" >Cập nhật</button>
                    </div>
                </div>
            </form>        
        </div>
    </div>
    <script>
        $("input[name='cycle']").keydown(function (e) {
            if (!(
                // Allow: backspace, tab, enter, esc, end, home, left, right, del, point, period
                $.inArray(e.keyCode, [8, 9, 13, 27, 35, 36, 37, 39, 46]) !== -1 ||
                // Allow: Ctrl+A, Ctrl+C, Ctrl+X
                ((e.ctrlKey || e.metaKey) && $.inArray(e.keyCode, [65, 67, 88]) !== -1) ||
                // Allow: keys 0-9
                (!e.shiftKey && e.keyCode >= 48 && e.keyCode <= 57) ||
                // Allow: numpad 0-9
                (e.keyCode >= 96 && e.keyCode <= 105))) {
                    // Disallow: every other keypress
                    e.preventDefault();
            }
        });

        $("#AgencyPackageForm").submit(function(e) {
            e.preventDefault();
        }).validate({
            rules: {
                agency_package_id: "required",
                agency: "required",
                package: "required",
                cycle: "required",
                agency_package_status: "required"
            },
            messages: {
                agency_package_id: "Mã đình danh đại lý và gói không được để trống",
                agency: "Đại lý phải được chọn",
                package: "Gói phải được chọn",
                cycle: "Chiết khấu không được để trống",
                agency_package_status: "Công bố phải được chọn"
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                $daily.loading_waiting("show");
                $.ajax({
                    url: "{{ route('cap_nhat_dai_ly_va_goi_path') }}",
                    type: "POST",
                    data: {
                        "id": $("input[name='agency_package_id']").val(),
                        "agency": $("select[name='agency']").val(),
                        "package": $("select[name='package']").val(),
                        "cycle": $("input[name='cycle']").val(),
                        "agency_package_status": $("select[name='agency_package_status']").val(),
                    },
                    dataType: "json",
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                    },
                    success: function(response) {
                        if(response.status == 200 && response.refresh == true){
                            location.reload(); 
                        }    
                    },
                    error: function(err) {
                        console.log(err)
                    }
                });
            }
        });
    </script>
@stop
