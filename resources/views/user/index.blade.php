@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Danh sách {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="overlay"></div>
                <div class="loading-img"></div>
                <div class="col-xs-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div>
                </div>
                @if(isset($list_permission))
                    <?php 
                        $is_created = in_array(Utilities::constantPermissions()["USER_CREATE"], $list_permission);
                        $is_viewed = in_array(Utilities::constantPermissions()["USER_VIEW"], $list_permission);
                        $is_edited = in_array(Utilities::constantPermissions()["USER_EDIT"], $list_permission);                        
                        $is_deleted = in_array(Utilities::constantPermissions()["USER_DELETE"], $list_permission);
                        $is_offline = in_array(Utilities::constantPermissions()["USER_OFFLINE"], $list_permission);
                        $is_locked = in_array(Utilities::constantPermissions()["USER_BLOCK"], $list_permission);
                    ?>
                    <div class="clearfix">
                        <div class="pull-right tableTools-container">
                            @if($is_created)
                                <a href="{{ route('tao_moi_nguoi_dung_page_path') }}" class="btn btn-white btn-info btn-bold">
                                    <i class="ace-icon fa fa-pencil bigger-120 blue"></i> Tạo mới người dùng
                                </a>
                            @endif    
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="pull-right tableTools-container">                            
                            @if($is_viewed)
                            <form method="POST" action="{{ route('nguoi_dung_page_path') }}">
                                <div class="input-group">
                                    <div class="input-group">          
                                        {{ csrf_field() }}
                                        <input placeholder="Tên đăng nhập" type="text" class="form-control" name="user_name" value="{{ $search_data }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-info no-radius search" type="submit">
                                                <i class="ace-icon fa fa-search"></i>   
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                            @endif
                        </div>
                    </div>
                    @if($is_viewed)
                        <table id="table_detail" class="table table-bordered table-hover">
                            <thead class="thin-border-bottom">
                                <tr>
                                    <th>#</th>
                                    <th>Họ tên</th>
                                    <th>Tên đăng nhập</th>
                                    <th>Trạng thái</th>
                                    <th>Mô-đun</th>
                                    <th>Nhóm cha</th>
                                    <th>Quyền hạn</th>
                                    @if($is_locked)
                                    <th>Khóa</th>
                                    @endif
                                    @if($is_offline)
                                    <th>Online</th>
                                    @endif
                                    <th>Thời gian tạo</th>
                                    <th>Thời giàn cập nhật</th>
                                    <th>Người tạo</th>
                                    <th>Người cập nhật</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($data))
                                    @foreach($data as $index => $user)
                                        <tr>
                                            <td> {{ ++$index }} </td>
                                            <td> {{ $user->full_name }} </td>
                                            <td> {{ $user->user_name }} </td>
                                            <td> {{ ($user->is_publish)?"Công khai":"Không công khai" }} </td>
                                            <td>
                                                @if(isset($user->group_modules))
                                                    <ul>  
                                                        @foreach($user->group_modules as $value)
                                                            <li>{{ $value->group_name }}</li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </td>
                                            <td>
                                                @if(isset($user->group_parent))
                                                    <ul>  
                                                        @foreach($user->group_parent as $value)
                                                            <li>{{ $value->group_user_name }}</li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </td>
                                            <td>
                                                @if(isset($user->permissions))
                                                    <ul>  
                                                        @foreach($user->permissions as $value)
                                                            <li>{{ $value->permission_name }}</li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </td>
                                            @if($is_locked)
                                            <td> 
                                                <label class="inline">
                                                    <input {{ ($user->is_locked)?"checked":"" }} type="checkbox" class="ace ace-switch ace-switch-5"
                                                        onchange="changeBlockStatus({{ json_encode($user->_id) }}, {{($user->is_locked)?0:1}})" />
                                                    <span class="lbl middle"></span>
                                                </label>
                                            </td>
                                            @endif
                                            @if($is_offline)
                                            <td>
                                                <label class="inline">
                                                    <input {{ ($user->is_online)?"checked":"" }} type="checkbox" class="ace ace-switch ace-switch-5" {{ ($user->is_online)?"":"disabled" }}
                                                        onchange="changeOnlineStatus({{ json_encode($user->_id) }}, {{($user->is_online)?1:0}})" />
                                                    <span class="lbl middle"></span>
                                                </label>
                                            </td>
                                            @endif
                                            <td> {{ $user->day_create_string }} </td>
                                            <td> {{ $user->day_update_string }} </td>
                                            <td> {{ (isset($user->user_create)) ? $user->user_create->full_name : "" }}</td>
                                            <td> {{ (isset($user->user_update)) ? $user->user_update->full_name : "" }}</td>
                                            <td>                                                
                                                <a href="/cai-dat/nguoi-dung/cap-nhat-nguoi-dung/{{$user->_id}}" class="btn btn-white btn-warning btn-xs"><i class="ace-icon fa fa-wrench  bigger-110 icon-only"></i></a>  
                                                &nbsp;                                                
                                                <button data-toggle="modal" data-target="#myModal" onclick="storeId('{{ $user->_id }}')" class="btn btn-white btn-danger btn-xs">
                                                    <i class="ace-icon fa fa-trash-o  bigger-110 icon-only"></i>
                                                </button>                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif            
                            </tbody>
                        </table>
                        <div class="clearfix pull-right">
                            @include("layouts.pagination", ["pages" => $pages])
                        </div>
                    @endif
                @endif    
            </div>
        </div>
    </div>
    @include("layouts.confirm", ["title"=> "mô-dun"])
    <script>
        $('p.alert').delay(5000).slideUp();
        @if($is_viewed)
            $daily.paging_item_click("{{ route('nguoi_dung_theo_so_trang_path') }}", "{{ $search_data }}");
        @endif

        @if($is_deleted)
            function storeId(id){
                $daily.store_id(id);
            }
            $daily.delete_item("{{ route('xoa_ho_so_path') }}");
        @endif
        @if($is_offline)
            function changeOnlineStatus(profileId, status){
                $daily.loading_waiting("show");
                if(status == 1){
                    $.ajax({
                        url: "{{ route('offline_agency_path') }}",
                        type: "POST",
                        data: {
                            "profile_id": profileId,
                            "status": false
                        }, 
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                        },
                        success: function(response) {
                            if(response.status == 200 && response.refresh == true){
                                location.reload(); 
                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });
                }
            }
        @endif
        @if($is_locked)
            function changeBlockStatus(profileId, status){
                $daily.loading_waiting("show");
                $.ajax({
                    url: "{{ route('block_user_path') }}",
                    type: "POST",
                    data: {
                        "profile_id": profileId,
                        "status": status
                    }, 
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                    },
                    success: function(response) {
                        if(response.status == 200 && response.refresh == true){
                            location.reload(); 
                        }
                    },
                    error: function(err) {
                        console.log(err)
                    }
                });
            }
        @endif 
    </script>
@stop