@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Tạo mới {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="overlay"></div>
            <div class="loading-img"></div>
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
            @if(isset($data))
                <form id="moduleForm" class="form-horizontal" role="form">    
                    <input type="hidden" name="profile_id" value="{{ $data->profile->_id }}" />            
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="full_name">Họ và tên</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="full_name" placeholder="Nhập họ và tên" value="{{ $data->profile->full_name }}">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="user_name">Tên đăng nhập</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="user_name" placeholder="Nhập tên đăng nhập" value="{{ $data->profile->user_name }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="password">Mật khẩu</label>
                        <div class="col-sm-5">
                            <input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="group_user">Nhóm người dùng</label>
                        <div class="col-sm-5">
                            <select class="form-control" name="group_user">
                                <option value="">Chọn nhóm người dùng</option>
                                @if(!empty($data->collection->groupUsers))
                                    @foreach($data->collection->groupUsers as $groupUser)
                                        <option value="{{ $groupUser->_id }}" {{ $groupUser->temp }}>{{ $groupUser->group_user_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="modules">Nhóm mô-đun</label>
                        <div class="col-sm-5">
                            @if(!empty($data->collection->groupModules )) 
                                <ul class="item-list ui-sortable">
                                    @foreach($data->collection->groupModules  as $groupModule)
                                    <?php 
                                        $page = null;
                                        $selected = "";
                                        foreach($groupModule->modules as $key => $value){
                                            $page .=" - Trang " . strtolower($value->module_name);
                                        }

                                        foreach($data->profile->group_modules as $selectGroupModule){
                                            if($selectGroupModule == $groupModule->_id){
                                                $selected = "checked";
                                            }
                                        }
                                    ?>
                                        <li class="item-orange clearfix ui-sortable-handle">
                                            <span class="col-sm-10">
                                                <b>{{ $groupModule->group_module_description }}</b><i>{{ $page }}</i>
                                            </span>
                                            <span class="pull-right">
                                                <input type="checkbox" name="group_modules" {{ $selected }} value="{{ $groupModule->_id }}"> 
                                            </span>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="permissions">Quyền hạn</label>
                        <div class="col-sm-5" style="height:450px; overflow-y: scroll">
                            @if(!empty($data->collection->permissions))
                                <ul class="item-list ui-sortable">
                                    @foreach($data->collection->permissions as $permission)
                                        <li class="item-red clearfix ui-sortable-handle">
                                            <strong>{{ $permission->module_permission_name }}</strong>
                                        </li>
                                            @foreach($permission->permissions as $subpermission)
                                            <?php       
                                                $selected = "";
                                                foreach($data->profile->permissions as $selectPermission){
                                                    if($selectPermission == $subpermission->_id){
                                                        $selected = "checked";
                                                    }
                                                }
                                            ?>
                                            <li class="item-default clearfix ui-sortable-handle">
                                                <span>
                                                    </i> {{ $subpermission->permission_name }}
                                                </span>
                                                <span class="pull-right">
                                                    <input type="checkbox" name="permissions" {{ $selected }} value="{{ $subpermission->_id }}">
                                                </span>
                                            </li>
                                            @endforeach
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="publish">Công bố</label>
                        <div class="col-sm-5">
                            <select class="form-control" name="publish">
                                <option value="">Chọn loại công bố</option>
                                <option value="true" {{ ($data->profile->is_publish == true)?'selected':'' }}>Hiển thị</option>
                                <option value="false" {{ ($data->profile->is_publish == false)?'selected':'' }}>Ẩn hiển thị</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-4">
                            <button type="submit" class="btn btn-primary" >Cập nhật người dùng</button>
                        </div>
                    </div>
                </form>    
            @endif    
        </div>
    </div>
    <script>
        var groupModules = [];
        var permissions = [];
        $("#moduleForm").submit(function(e) {
            e.preventDefault();
        }).validate({
            rules: {
                full_name: "required",
                user_name: {
                    "required": true,
                    "remote": {
                        url: "{{ route('kiem_tra_ten_dang_nhap_cap_nhat_path', ['userId' => $data->profile->_id ]) }}",
                        type: "POST",
                        dataType: "json",
                        data: {
                            user_name: function() {
                                return $("input[name='user_name']").val();
                            }
                        },
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                        }
                    }
                },
                group_modules: "required",
                group_user: "required",
                permissions: "required",
                publish: "required"
            },
            messages: {
                full_name: "Họ và tên không được để trống",
                user_name: {
                    required: "Tên đăng nhập không được để trống",
                    remote: "Tên đăng nhập đã tồn tại",
                },
                group_modules: "Nhóm mô-đun phải được chọn",
                group_user: "Nhóm nguời dùng phải được chọn",
                permissions: "Quyền hạn phải được chọn",
                publish: "Công bố phải được chọn"
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                $daily.loading_waiting("show");
                $.each($("input[name='group_modules']:checked"), function(){            
                    groupModules.push($(this).val());
                });
                $.each($("input[name='permissions']:checked"), function(){            
                    permissions.push($(this).val());
                });
                $.ajax({
                    url: "{{ route('cap_nhat_nguoi_dung_path') }}",
                    type: "POST",
                    data: {
                        "profile_id": $("input[name='profile_id']").val(),
                        "full_name": $("input[name='full_name']").val(),
                        "user_name": $("input[name='user_name']").val(),
                        "password": $("input[name='password']").val(),
                        "group_user": $("select[name='group_user']").val(),
                        "publish": $("select[name='publish']").val(),
                        "group_modules": groupModules,
                        "permissions": permissions,
                    },
                    dataType: "json",
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                    },
                    success: function(response) {
                        if(response.status == 200 && response.refresh == true){
                            location.reload(); 
                        }      
                    },
                    error: function(err) {
                        console.log(err)
                    }
                });
            }
        });
    </script>
@stop