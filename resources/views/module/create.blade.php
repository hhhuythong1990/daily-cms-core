@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Tạo mới {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="overlay"></div>
            <div class="loading-img"></div>
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
            @if(isset($list_permission))
                <?php 
                    $is_created = in_array(Utilities::constantPermissions()["PERMISSION_CREATE"], $list_permission);
                ?>
                @if(isset($is_created))
                    <form id="moduleForm" class="form-horizontal" role="form" autocomplete="off">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="module_name">Tên mô-đun</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="module_name" placeholder="Nhập tên mô-đun" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="publish">Công bố</label>
                            <div class="col-sm-5">
                                <select class="form-control" name="publish">
                                    <option value="">Chọn loại công bố</option>
                                    <option value="true">Hiển thị</option>
                                    <option value="false">Ẩn hiển thị</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4">
                                <button type="submit" class="btn btn-primary" >Tạo mới mô-đun</button>
                            </div>
                        </div>
                    </form>
                @endif
            @endif
        </div>
    </div>
    <script>
        $('p.alert').delay(5000).slideUp();
        @if(isset($is_created))
            $("#moduleForm").submit(function(e) {
                e.preventDefault();
            }).validate({
                rules: {
                    module_name: {                        
                        "required": true,
                        "remote": {
                            url: "{{ route('kiem_tra_ten_mo_dun_path') }}",
                            type: "POST",
                            dataType: "json",
                            data: {
                                module_name: function() {
                                    return $("input[name='module_name']").val();
                                }
                            },
                            beforeSend: function(xhr){
                                xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                            }
                        }
                    },
                    publish: "required",
                },
                messages: {
                    module_name:  {
                        required: "Tên mô-đun không được để trống",
                        remote: "Tên mô-đun đã tồn tại",
                    },
                    publish: "Công bố phải được chọn",
                },
                highlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    $daily.loading_waiting("show");
                    $.ajax({
                        url: "{{ route('tao_moi_mo_dun_path') }}",
                        type: "POST",
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                        },
                        success: function(response) {
                            if(response.status == 200 && response.refresh == true){
                                location.reload(); 
                            }             
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });
                }
            });
        @endif
    </script>
@stop