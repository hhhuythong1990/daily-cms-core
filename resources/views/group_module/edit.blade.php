@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Cập nhật {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="overlay"></div>
            <div class="loading-img"></div>
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
            @if(isset($list_permission))
                <?php 
                    $is_edited = in_array(Utilities::constantPermissions()["PERMISSION_EDIT"], $list_permission);    
                ?>
                @if($is_edited)
                    @if(isset($data))
                        <form id="moduleForm" class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="group_module_description">Mô tả nhóm mô-đun</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="group_module_description" placeholder="Mô tả tên nhóm mô-đun" value="{{$data->group_module_description}}">
                                </div>
                            </div>         
                            <div class="form-group">
                                <input type="hidden" name="group_module_id" value="{{$data->_id}}"/>
                                <label class="col-sm-4 control-label" for="group_module_name">Tên nhóm quyền hạn</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="group_module_name" placeholder="Nhập tên quyền hạn"  value="{{$data->group_name}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="modules">Mô-đun thuộc nhóm</label>
                                <div class="col-sm-5">
                                    <select class="form-control" multiple="multiple" name="modules" style="height:350px">
                                        @if(!empty($modules))  
                                            @foreach($modules as $module)
                                                <option value="{{ $module->_id }}" @if(in_array($module->_id, $data->modules)) selected @endif>
                                                    {{ $module->module_name }}
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="publish">Công bố</label>
                                <div class="col-sm-5">
                                    <select class="form-control" name="publish">
                                        <option value="">Chọn loại công bố</option>
                                        <option value="true" @if($data->is_publish == true) selected @endif>Hiển thị</option>
                                        <option value="false" @if($data->is_publish == false) selected @endif>Ẩn hiển thị</option>
                                    </select>
                                </div>
                            </div>
    
                            <div class="form-group">
                                <div class="col-sm-offset-4">
                                    <button type="submit" class="btn btn-primary" >Cập nhật nhóm mô-đun</button>
                                </div>
                            </div>
                        </form>
                    @endif
                @endif    
            @endif
        </div>
    </div>
    <script>     
        $('p.alert').delay(5000).slideUp();
        @if($is_edited)
            $("#moduleForm").submit(function(e) {
                e.preventDefault();
            }).validate({
                rules: {
                    group_module_description: {
                        "required": true,
                        "remote": {
                            url: "{{ route('kiem_tra_nhom_mo_dun_cap_nhat_path', ['groupModuleId' => $data->_id ]) }}",
                            type: "POST",
                            dataType: "json",
                            data: {
                                group_module_description: function() {
                                    return $("input[name='group_module_description']").val();
                                }
                            },
                            beforeSend: function(xhr){
                                xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                            }
                        }
                    },
                    modules: "required",
                    publish: "required"
                },
                messages: {
                    group_module_description: {
                        required: "Mô tả nhóm quyền hạn không được để trống",
                        remote: "Mô tả nhóm quyền hạn đã tồn tại",
                    },
                    modules: "Mô-đun thuộc nhóm không được để trống",
                    publish: "Công bố phải được chọn",
                },
                highlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    $daily.loading_waiting("show");
                    $.ajax({
                        url: "{{ route('cap_nhat_nhom_mo_dun_path') }}",
                        type: "POST",
                        data: {
                            "group_module_description": $("input[name='group_module_description']").val(),
                            "group_module_id": $("input[name='group_module_id']").val(),
                            "group_module_name": $("input[name='group_module_name']").val(),
                            "modules": $("select[name='modules']").val(),
                            "publish": $("select[name='publish']").val()
                        },
                        dataType: "json",
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                        },
                        success: function(response) {
                            console.log(response);
                            if(response.status == 200 && response.refresh == true){
                                location.reload(); 
                            }
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });
                }
            });
        @endif
    </script>
@stop