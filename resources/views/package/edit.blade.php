@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Cập nhật {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="overlay"></div>
            <div class="loading-img"></div>
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
            <form id="editPackageForm" class="form-horizontal" role="form">                
                <input type="hidden" name="package_id" value="{{ $data->_id }}" />
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="package_name">Tên gói</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="package_name" placeholder="Nhập tên gói" value="{{$data->package_name}}">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="plan_id">Plan id</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="plan_id" placeholder="Nhập plan id" value="{{$data->plan_id}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="package_price">Giá gói</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="package_price" placeholder="Nhập giá gói" value="{{$data->package_price}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="package_description">Mô tả gói</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="package_description" placeholder="Nhập mô tả gói" value="{{$data->package_description}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="package_status">Trạng thái gói</label>
                    <div class="col-sm-5">
                        <select class="form-control" name="package_status">
                            <option value="">Chọn trạng thái gói</option>
                            <option value="true" @if($data->package_status == true) selected @endif>Hoạt động</option>
                            <option value="false" @if($data->package_status == false) selected @endif>Vô hiệu quá</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4">
                        <button type="submit" class="btn btn-primary">Cập nhật gói</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>     
        $("input[name='plan_id'], input[name='package_price']").keydown(function (e) {
            if (!(
                // Allow: backspace, tab, enter, esc, end, home, left, right, del, point, period
                $.inArray(e.keyCode, [8, 9, 13, 27, 35, 36, 37, 39, 46]) !== -1 ||
                // Allow: Ctrl+A, Ctrl+C, Ctrl+X
                ((e.ctrlKey || e.metaKey) && $.inArray(e.keyCode, [65, 67, 88]) !== -1) ||
                // Allow: keys 0-9
                (!e.shiftKey && e.keyCode >= 48 && e.keyCode <= 57) ||
                // Allow: numpad 0-9
                (e.keyCode >= 96 && e.keyCode <= 105))) {
                    // Disallow: every other keypress
                    e.preventDefault();
            }
        });
        $('p.alert').delay(5000).slideUp();
        $("#editPackageForm").submit(function(e) {
            e.preventDefault();
        }).validate({
            rules: {
                package_id: "required",
                package_name:  {
                    "required": true,
                    "remote": {
                        url: "{{ route('kiem_tra_name_goi_cap_nhat_path', ['packageId' => $data->_id ]) }}",
                        type: "POST",
                        dataType: "json",
                        data: {
                            package_name: function() {
                                return $("input[name='package_name']").val();
                            }
                        },
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                        }
                    }
                },
                plan_id: {
                    "required": true,
                    "remote": {
                        url: "{{ route('kiem_tra_plan_goi_cap_nhat_path', ['packageId' => $data->_id ]) }}",
                        type: "POST",
                        dataType: "json",
                        data: {
                            plan_id: function() {
                                return $("input[name='plan_id']").val();
                            }
                        },
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                        }
                    }
                },
                package_price: "required",
                package_description: "required",
                package_status: "required",
            },
            messages: {
                package_id: "Mã định danh gói không được trống",
                package_name: {
                    required: "Tên gói không được để trống",
                    remote: "Tên gói đã tồn tại",
                },
                plan_id: {
                    required: "Plan id không được để trống",
                    remote: "Plan id đã tồn tại",
                },
                package_price: "Giá của gói không được để trống",
                package_description: "Mô tả của gói không được để trống",
                package_status: "Trạng thái của gói phải được chọn",
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                $daily.loading_waiting("show");
                $.ajax({
                    url: "{{ route('cap_nhat_goi_path') }}",
                    type: "POST",
                    data: $(form).serialize(),
                    dataType: "json",
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                    },
                    success: function(response) {
                        if(response.status == 200 && response.refresh == true){
                            location.reload(); 
                        }
                    },
                    error: function(err) {
                        console.log(err)
                    }
                });
            }
        });
    </script>
@stop
