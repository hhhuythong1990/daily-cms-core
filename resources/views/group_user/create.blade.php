@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Tạo mới {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="overlay"></div>
            <div class="loading-img"></div>
            <div class="col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
            </div>
            <form id="moduleForm" class="form-horizontal" role="form">                
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="group_user_name">Tên nhóm người dùng</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="group_user_name" placeholder="Nhập tên nhóm ngườ dùng" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="publish">Công bố</label>
                    <div class="col-sm-5">
                        <select class="form-control" name="publish">
                            <option value="">Chọn loại công bố</option>
                            <option value="true">Hiển thị</option>
                            <option value="false">Ẩn hiển thị</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4">
                        <button type="submit" class="btn btn-primary" >Tạo mới nhóm người dùng</button>
                    </div>
                </div>
            </form>        
        </div>
    </div>
    <script>       

        $("#moduleForm").submit(function(e) {
            e.preventDefault();
        }).validate({
            rules: {
                group_user_name: "required",
                publish: "required"
            },
            messages: {
                group_user_name: "Tên nhóm người dùng không được để trống",
                publish: "Công bố phải được chọn",
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                $daily.loading_waiting("show");
                $.ajax({
                    url: "{{ route('tao_moi_nhom_nguoi_dung_path') }}",
                    type: "POST",
                    data: $(form).serialize(),
                    dataType: "json",
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                    },
                    success: function(response) {
                        if(response.status == 200 && response.refresh == true){
                            location.reload(); 
                        }                 
                    },
                    error: function(err) {
                        console.log(err)
                    }
                });
            }
        });
    </script>
@stop