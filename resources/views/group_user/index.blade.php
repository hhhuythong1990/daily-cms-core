@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Danh sách {{strtolower($title)}}
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="overlay"></div>
                <div class="loading-img"></div>
                <div class="col-xs-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div>
                </div>
                @if(isset($list_permission))
                    <?php 
                        $is_created = in_array(Utilities::constantPermissions()["MODULE_CREATE"], $list_permission);
                        $is_viewed = in_array(Utilities::constantPermissions()["MODULE_VIEW"], $list_permission);
                        $is_edited = in_array(Utilities::constantPermissions()["MODULE_EDIT"], $list_permission);                        
                        $is_deleted = in_array(Utilities::constantPermissions()["MODULE_DELETE"], $list_permission);
                    ?>
                    <!--<div class="clearfix">
                        <div class="pull-right tableTools-container">
                            @if($is_created)
                                <a href="{{ route('tao_moi_nhom_nguoi_dung_page_path') }}" class="btn btn-white btn-info btn-bold">
                                    <i class="ace-icon fa fa-pencil bigger-120 blue"></i> Tạo mới nhóm người dùng
                                </a>
                            @endif
                        </div>
                    </div> -->
                    @if($is_viewed)
                        <table id="module_detail" class="table table-bordered table-hover">
                            <thead class="thin-border-bottom">
                                <tr>
                                    <th>#</th>
                                    <th>Định danh</th>
                                    <th>Tên nhóm</th>
                                    <th>Tên nhóm không dấu</th>
                                    <th>Trạng thái</th>
                                    <th>Thời gian tạo</th>
                                    <!--<th>Thời gian cập nhật</th>-->
                                    <th>Người tạo</th>
                                    <!--<th>Người cập nhật</th>-->
                                    <!--<th></th>-->
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($data))
                                    @foreach($data as $index => $groupUser)
                                        <tr>
                                            <td> {{ ++$index }} </td>
                                            <td> {{ $groupUser->_id }} </td>
                                            <td> {{ $groupUser->group_user_name }} </td>
                                            <td> {{ $groupUser->group_user_name_slug }} </td>
                                            <td> {{ ($groupUser->is_publish)?"Công khai":"Không công khai" }} </td>
                                            <td> {{ $groupUser->day_create_string }} </td>
                                            <!--<td> {{ $groupUser->day_update_string }} </td>-->
                                            <td> {{ (isset($groupUser->user_create)) ? $groupUser->user_create->full_name : "" }}</td>
                                            <!--<td> {{ (isset($groupUser->user_update)) ? $groupUser->user_update->full_name : "" }}</td>-->
                                            <!--<td>
                                                @if($is_edited)
                                                    <a href="/cai-dat/mo-dun/cap-nhat-mo-dun/{{$groupUser->_id}}" class="btn btn-white btn-warning btn-xs"><i class="ace-icon fa fa-wrench  bigger-110 icon-only"></i></a>  
                                                @endif
                                                &nbsp;
                                                @if($is_deleted)
                                                    <button data-toggle="modal" data-target="#myModal" onclick="storeId('{{ $groupUser->_id }}')" class="btn btn-white btn-danger btn-xs">
                                                        <i class="ace-icon fa fa-trash-o  bigger-110 icon-only"></i>
                                                    </button>
                                                @endif
                                            </td>-->
                                        </tr>
                                    @endforeach
                                @endif                                       
                            </tbody>
                        </table>
                        <div class="clearfix pull-right">
                            @include("layouts.pagination", ["pages" => $pages])
                        </div>
                    @endif
                @endif    
            </div>
        </div>
    </div>
     @include("layouts.confirm", ["title"=> "mô-dun"])
    <script>
        $(document).on("click", ".page-item", function(){
            $daily.loading_waiting("show");
            $(".page-item").each(function() {
                $(this).removeClass("active");
            });
            $(this).addClass("active");
            $.ajax({
                url: `{{ url('cai-dat/mo-dun/') }}/${ $(this).text() }`,
                type: "GET",
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                },
                success: function(response) {
                    if(response.status == 200 && response.refresh == true){
                        location.reload(); 
                    }else{
                        $("#module_detail > tbody").html("")
                        $("#module_detail").append(response);
                        $daily.loading_waiting("hide");     
                    } 
                },
                error: function(err) {
                    console.log(err)
                }
            });
        });

        function storeId(id){
            $daily.store_id(id);
        }        

        $(document).on("click", "#delButton", function() {
            $('#myModal').modal('toggle');
            $daily.loading_waiting("show");
            $.ajax({
                url: "{{ route('xoa_mo_dun_path') }}",
                type: "POST",
                data: { "module_id": selectId },
                dataType: "json",
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                },
                success: function(response) {
                    if(response.status == 200 && response.refresh == true){
                        location.reload(); 
                    }
                },
                error: function(err) {
                    console.log(err)
                }
            });
        });       
    
    </script>
@stop