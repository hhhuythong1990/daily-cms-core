<div id="sidebar" class="sidebar responsive ace-save-state">
    <script type="text/javascript">
        try{ace.settings.loadState('sidebar')}catch(e){}
    </script>
    
    <ul class="nav nav-list">  
        @if(isset($list_menu))
            @foreach($list_menu as $menu)
                <li class="{{ $menu->group_name_slug }}">
                    <a href="" class="dropdown-toggle">
                        <i class="menu-icon fa fa-list-alt"></i>
                        <span class="menu-text"> {{ $menu->group_name }}</span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        @foreach($menu->modules as $subMenu)
                            <li id="{{ $subMenu->module_name_slug }}" class="">
                                <a href="{{ route( $subMenu->route_path ) }}">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    {{ $subMenu->module_name }}
                                </a>
                                <b class="arrow"></b>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endforeach
        @endif
    </ul>

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>