<ul class="pagination">    
    @if(isset($pages))
        <?php $is_active = "" ?>
        @for ($i = 1; $i <= $pages; $i++)
            @if($i == 1)
                <?php $is_active = "active" ?>
            @else
                <?php $is_active = "" ?>
            @endif
            <li class="page-item my-pointer {{ $is_active }}"><a class="page-link">{{ $i }}</a></li>
        @endfor
    @endif
</ul>