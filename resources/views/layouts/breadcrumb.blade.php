<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/">Trang chủ</a>
        </li>
        <li class="active">{{$title_active}}</li>
    </ul>
</div>