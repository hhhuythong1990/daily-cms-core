@extends('home')
@section('content')
    @include("layouts.breadcrumb", ["title_active" => $title])
    <div class="page-content">
        <div class="page-header">
            <h1>
                Thông tin tài khoản
            </h1>
        </div>
        <div class="overlay"></div>
        <div class="loading-img"></div>
        <div class="col-xs-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="row">
            <form id="changePassword" class="form-horizontal" role="form" method="POST">        
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="group_user_name">Tên nhóm người dùng</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" value="{{$data->full_name}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="group_user_name">Tên nhóm đăng nhập</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" value="{{$data->user_name}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="group_user_name">Thời gian tạo</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" value="{{$data->day_create_string}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="group_user_name">Mật khẩu cũ</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" name="old_password" placeholder="Nhập mật khẩu cũ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="group_user_name">Mật khẩu mới</label>
                    <div class="col-sm-5">
                        <input type="password" id="new_password" class="form-control" name="new_password" placeholder="Mật khẩu mới">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="group_user_name">Nhập lại mật khẩu mới</label>
                    <div class="col-sm-5">
                        <input type="password" id="renew_password" class="form-control" name="renew_password" placeholder="Nhập lại mật khẩu mới">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary" >Cập nhật mật khẩu</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        jQuery.validator.addMethod("checkPassword", function(value, element) {
            var regex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/;
            var key = value;
            if (!regex.test(key)) {
                return false;
            }
            return true;
        }, "Mật khẩu phải dài hơn 8 kí tự, có kí tự đặc biệt, có chữ số");

        $("#changePassword").submit(function(e) {
                e.preventDefault();
            }).validate({
                rules: {
                    new_password: {
                        required: true,
                        checkPassword: true,
                        equalTo: "#renew_password"
                    },
                    renew_password: {
                        required: true,
                        checkPassword: true,
                        equalTo: "#new_password"
                    },
                },
                messages: {
                    new_password:  {
                        required: "Mật khẩu mới không được để trống",
                        equalTo: "Mật khẩu mới không giống nhập lại mật khẩu mới",
                        minlength: "Mật khẩu mới phải lớn hơn 8 kí tự"
                    },
                    renew_password:  {
                        required: "Nhập lại mật khẩu mới không được để trống",
                        equalTo: "Nhập lại mật khẩu mới không giống mật khẩu mới",
                        minlength: "Nhập lại mật khẩu mới phải lớn hơn 8 kí tự"
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    $daily.loading_waiting("show");
                    $.ajax({
                        url: "{{ route('thay_doi_mat_khau_path') }}",
                        type: "POST",
                        data: {
                            "user_id": "{{$data->_id}}",
                            "old_password": $("input[name='old_password']").val(),
                            "new_password": $("input[name='new_password']").val(),
                        },
                        dataType: "json",
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token','{{ csrf_token() }}');
                        },
                        success: function(response) {
                            if(response.status == 200 && response.refresh == true){
                                location.reload(); 
                            }             
                        },
                        error: function(err) {
                            console.log(err)
                        }
                    });
                }
            });
    </script>
@stop