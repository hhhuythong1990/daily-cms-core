$(document).ready(function(){

	$(".sort-area").mCustomScrollbar({
	    scrollInertia: 300
	});

	$(".panel-collapse .btn-close").click(function(e){
		e.preventDefault();
		$(this).parent().removeClass('in');
	});

	$('.single-slider').jRange({
        from: 0,
        to: 10000,
        step: 500,
        scale: [0,10000],
        format: '%s',
        width: 500,
        showLabels: true,
        snap: true
    });

    $('.SlectBox').SumoSelect();
});
