/**
 * Created by User on 6/26/2017.
 */
let selectedId; 
let $daily={
    active_sidebar: function() {
        let url = window.location.pathname;
        let urlParts = url.split("/");
        let groupSidebar = urlParts[1];
        let childActivated = urlParts[2];
        $("."+groupSidebar).addClass("open active");
        $("#"+childActivated).addClass("active");
    },
    close_alert_msg: function() {
        $('div.alert').delay(5000).slideUp();
    },
    store_id: function(id) {
        selectedId = id;
    },
    loading_waiting: function(status){
        if(status == "show"){
            $(".overlay").show();
            $(".loading-img").show();
        }else{
            $(".overlay").hide();
            $(".loading-img").hide();
        }
    },
    paging_item_click: (url, searchData) => {
        $(document).on("click", ".page-item", function(){
            $daily.loading_waiting("show");
            $(".page-item").each(function() {
                $(this).removeClass("active");
            });
            $(this).addClass("active");
            $.ajax({
                url: `${ url }`,
                data: {
                    page: $(this).text(),
                    data_search: searchData
                },
                type: "POST",
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $("meta[name=csrf-token]").attr("content"));
                },
                success: function(response) {
                    if(response.status == 200 && response.refresh == true){
                        location.reload(); 
                    }else{                        
                        $("#table_detail > tbody").html("");
                        $("#table_detail").append(response.data);
                        $daily.loading_waiting("hide");
                    }
                },
                error: function(err) {
                    console.log(err)
                }
            });
        });
    },
    paging_item: (url, searchData, dateStart, dateEnd) => {
        $(document).on("click", ".page-item", function(){
            $daily.loading_waiting("show");
            $(".page-item").each(function() {
                $(this).removeClass("active");
            });
            $(this).addClass("active");
            $.ajax({
                url: `${ url }`,
                data: {
                    page: $(this).text(),
                    agency_name: searchData,
                    date_start: dateStart,
                    date_end: dateEnd
                },
                type: "POST",
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $("meta[name=csrf-token]").attr("content"));
                },
                success: function(response) {
                    if(response.status == 200 && response.refresh == true){
                        location.reload(); 
                    }else{                      
                        $("#table_detail > tbody").html("");
                        $("#table_detail").append(response.data);
                        $daily.open_detail();
                        $daily.loading_waiting("hide");
                    }
                },
                error: function(err) {
                    console.log(err)
                }
            });
        });
    },
    delete_item: (url) => {
        $(document).on("click", "#delButton", function() {
            $('#myModal').modal('toggle');
            $daily.loading_waiting("show");
            $.ajax({
                url: url,
                type: "POST",
                data: { "item_id": selectedId },
                dataType: "json",
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $("meta[name=csrf-token]").attr("content"));
                },
                success: function(response) {
                    if(response.status == 200 && response.refresh == true){
                        location.reload(); 
                    }
                },
                error: function(err) {
                    console.log(err)
                }
            });
        });
    },
    open_detail: () => {
        $('.show-details-btn').on('click', function(e) {
            e.preventDefault();
            $(this).closest('tr').next().toggleClass('open');
            $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
        });
    }

};
