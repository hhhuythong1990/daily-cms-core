<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Route::get("dang-nhap", function () {
//     return view("login");
// });
Route::get("dang-nhap", ["as" => "dang_nhap_path", "uses" => "HomeController@dangNhap"]);

Route::get('/', ["as" => "trang_chu_path", "uses" => "HomeController@index"]);

Route::post("dang-nhap", ["as" => "dang_nhap_path", "uses" => "AuthenticateController@login"]);
Route::get("dang-xuat", ["as" => "dang_xuat_path", "uses" => "AuthenticateController@logout"]);

Route::get('thong-tin-tai-khoan', ["as" => "thong_tin_tai_khoan_path", "uses" => "UserController@userInfo"]);

Route::group(["prefix" => "quan-ly-admin"], function() {
    //Danh sách tài khoản
    Route::get("danh-sach-tai-khoan", ["as" => "danh_sach_tai_khoan_page_path", "uses" => "PermissionController@index"])->middleware("AuthenticateLogin");
});

Route::group(["prefix" => "quan-ly-goi"], function() {
    //Danh sách gói
    Route::get("danh-sach-goi", ["as" => "danh_sach_goi_page_path", "uses" => "PackageController@index"])->middleware("AuthenticateLogin");
    Route::post("danh-sach-goi", ["as" => "tim_kiem_goi_path", "uses" => "PackageController@takePackageSearchData"])->middleware("AuthenticateLogin");
    Route::get("danh-sach-goi/tao-moi-goi", ["as" => "tao_moi_goi_page_path", "uses" => "PackageController@create"])->middleware("AuthenticateLogin");
    Route::get("danh-sach-goi/cap-nhat-goi/{id}", ["as" => "cap_nhat_goi_page_path", "uses" => "PackageController@edit"])->middleware("AuthenticateLogin");
    //Call Ajax
    Route::post("tao-moi-goi", ["as" => "tao_moi_goi_path", "uses" => "PackageController@createPackageAjax"]);
    Route::post("goi-theo-trang", ["as" => "goi_theo_so_trang_path", "uses" => "PackageController@takeModulePageAjax"]);
    Route::post("kiem-tra-ten-goi", ["as" => "kiem_tra_ten_goi_path", "uses" => "PackageController@checkPackageNameAjax"]);
    Route::post("kiem-tra-plan-id", ["as" => "kiem_tra_plan_id_path", "uses" => "PackageController@checkPlanIdAjax"]);
    Route::post("kiem-tra-plan-goi-cap-nhat/{packageId}", ["as" => "kiem_tra_plan_goi_cap_nhat_path", "uses" => "PackageController@checkPackagePlanExist"]);
    Route::post("kiem-tra-name-goi-cap-nhat/{packageId}", ["as" => "kiem_tra_name_goi_cap_nhat_path", "uses" => "PackageController@checkPackageNameExist"]);
    Route::post("cap-nhat-goi", ["as" => "cap_nhat_goi_path", "uses" => "PackageController@editPackageAjax"]);
    Route::post("xoa-goi", ["as" => "xoa_goi_path", "uses" => "PackageController@deletePackageAjax"]);

    //Phân quyền bán gói
    Route::get("danh-sach-goi-ban", ["as" => "danh_sach_goi_ban_page_path", "uses" => "AgencyPackageController@index"])->middleware("AuthenticateLogin");
    Route::post("danh-sach-goi-ban", ["as" => "tim_kiem_dai_ly_va_goi_path", "uses" => "AgencyPackageController@takeAgencyPackageSearchData"])->middleware("AuthenticateLogin");
    Route::get("danh-sach-goi-ban/tao-moi-dai-ly-va-goi", ["as" => "tao_moi_goi_va_dai_ly_page_path", "uses" => "AgencyPackageController@create"])->middleware("AuthenticateLogin");    
    Route::get("danh-sach-goi-ban/cap-nhat-dai-ly-va-goi/{id}", ["as" => "cap_nhat_dai_ly_va_goi_page_path", "uses" => "AgencyPackageController@edit"])->middleware("AuthenticateLogin");
    //Call Ajax
    Route::post("tao-moi-dai-ly-va-goi", ["as" => "tao_moi_dai_ly_va_goi_path", "uses" => "AgencyPackageController@createAgencyPackageAjax"]);
    Route::post("dai-ly-va-goi-theo-trang", ["as" => "dai_ly_va_goi_theo_so_trang_path", "uses" => "AgencyPackageController@takeAgencyPackagePageAjax"]);
    Route::post("cap-nhat-dai-ly-va-goi", ["as" => "cap_nhat_dai_ly_va_goi_path", "uses" => "AgencyPackageController@editAgencyPackageAjax"]);
    Route::post("xoa-dai-ly-va-goi", ["as" => "xoa_dai_ly_va_goi_path", "uses" => "AgencyPackageController@deleteAgencyPackageAjax"]);
    
    //Danh sách code
    Route::get("danh-sach-code", ["as" => "danh_sach_code_page_path", "uses" => "AgencyPackageController@getCode"])->middleware("AuthenticateLogin");
    Route::post("danh-sach-code", ["as" => "tim_kiem_code_path", "uses" => "AgencyPackageController@takeCodeSearchData"])->middleware("AuthenticateLogin");
    //Call Ajax
    Route::post("code-theo-trang", ["as" => "code_theo_so_trang_path", "uses" => "AgencyPackageController@takeCodePageAjax"]);

    //Doanh thu
    Route::get("quan-ly-doanh-thu", ["as" => "quan_ly_doanh_thu_page_path", "uses" => "RevenueController@index"])->middleware("AuthenticateLogin");
    Route::post("quan-ly-doanh-thu", ["as" => "tim_kiem_doanh_thu_page_path", "uses" => "RevenueController@searchRevenue"])->middleware("AuthenticateLogin");
    Route::post("bao-cao-doanh-thu", ["as" => "bao_cao_doanh_thu_path", "uses" => "RevenueController@exportData"])->middleware("AuthenticateLogin");
    //Call Ajax
    Route::post("quan-ly-doanh-thu-theo-trang", ["as" => "quan_ly_doanh_thu_theo_so_trang_path", "uses" => "RevenueController@pagingRevenueAjax"]);
});

Route::group(["prefix" => "quan-ly-dai-ly"], function() {
    //Danh sách đại lý
    Route::get("danh-sach-dai-ly", ["as" => "danh_sach_dai_ly_page_path", "uses" => "AgencyController@index"])->middleware("AuthenticateLogin");    
    Route::post("danh-sach-dai-ly", ["as" => "tim_kiem_dai_ly_so_trang_path", "uses" => "AgencyController@takeAgencySearchData"]);
    Route::get("danh-sach-dai-ly/tao-moi-dai-ly", ["as" => "tao_moi_dai_ly_page_path", "uses" => "AgencyController@create"])->middleware("AuthenticateLogin");
    Route::get("danh-sach-dai-ly/cap-nhat-dai-ly/{id}", ["as" => "cap_nhat_dai_ly_page_path", "uses" => "AgencyController@edit"])->middleware("AuthenticateLogin");
    //Call Ajax
    Route::post("tao-moi-dai-ly", ["as" => "tao_moi_dai_ly_path", "uses" => "AgencyController@createAgencyAjax"]);
    Route::post("dai-ly-theo-trang", ["as" => "dai_ly_theo_so_trang_path", "uses" => "AgencyController@takeAgencySearchPageAjax"]);
    Route::post("kiem-tra-ma-dai-ly", ["as" => "kiem_tra_ma_dai_ly_path", "uses" => "AgencyController@checkAgencyCodeAjax"]);
    Route::post("kiem-tra-ten-dai-ly-cap-nhat", ["as" => "kiem_tra_ten_dai_ly_cap_nhat_path", "uses" => "AgencyController@checkAgencyExist"]);
    Route::post("cap-nhat-dai-ly", ["as" => "cap_nhat_dai_ly_path", "uses" => "AgencyController@editAgencyAjax"]);
    Route::post("xoa-dai-ly", ["as" => "xoa_dai_ly_path", "uses" => "AgencyController@deleteAgencyAjax"]);
});

Route::group(["prefix" => "cai-dat"], function() {
    //Quyền hạn
    Route::get("quyen-han", ["as" => "quyen_han_page_path", "uses" => "PermissionController@index"])->middleware("AuthenticateLogin");
    Route::post("quyen-han", ["as" => "quyen_han_page_path", "uses" => "PermissionController@takePermissionSearchData"])->middleware("AuthenticateLogin");
    Route::get("quyen-han/tao-moi-quyen-han", ["as" => "tao_moi_quyen_han_page_path", "uses" => "PermissionController@create"])->middleware("AuthenticateLogin");
    Route::get("quyen-han/cap-nhat-quyen-han/{id}", ["as" => "cap_nhat_quyen_han_page_path", "uses" => "PermissionController@update"])->middleware("AuthenticateLogin");
    //Call Ajax
    Route::post("tao-moi-quyen-han", ["as" => "tao_moi_quyen_han_path", "uses" => "PermissionController@createPermissionAjax"]);
    Route::post("quyen-han-theo-trang", ["as" => "danh_sach_quyen_han_theo_so_trang_path", "uses" => "PermissionController@takePermissionPageAjax"]);    
    Route::post("xoa-quyen-han", ["as" => "xoa_quyen_han_path", "uses" => "PermissionController@deletePermissionAjax"]);
    Route::post("kiem-tra-quyen-han-cap-nhat/{permissionId}", ["as" => "kiem_tra_quyen_han_cap_nhat_path", "uses" => "PermissionController@checkPermissionExist"]);
    Route::post("cap-nhat-quyen-han", ["as" => "cap_nhat_quyen_han_path", "uses" => "PermissionController@updatePermission"]);
    Route::post("kiem-tra-ten-quyen-han", ["as" => "kiem_tra_ten_quyen_han_path", "uses" => "PermissionController@checkPermissionNameExist"]);

    //Mô-đun
    Route::get("mo-dun", ["as" => "mo_dun_page_path", "uses" => "ModuleController@index"])->middleware("AuthenticateLogin");
    Route::post("mo-dun", ["as" => "mo_dun_page_path", "uses" => "ModuleController@takeModuleSearchData"])->middleware("AuthenticateLogin");
    Route::get("mo-dun/tao-moi-mo-dun", ["as" => "tao_moi_mo_dun_page_path", "uses" => "ModuleController@create"])->middleware("AuthenticateLogin");
    Route::get("mo-dun/cap-nhat-mo-dun/{id}", ["as" => "cap_nhat_mo_dun_page_path", "uses" => "ModuleController@edit"])->middleware("AuthenticateLogin");
    //Call Ajax
    Route::post("tao-moi-mo-dun", ["as" => "tao_moi_mo_dun_path", "uses" => "ModuleController@createModuleAjax"]);
    Route::post("mo-dun-theo-trang", ["as" => "danh_sach_mo_dun_theo_so_trang_path", "uses" => "ModuleController@takeModulePageAjax"]);
    Route::post("xoa-mo-dun", ["as" => "xoa_mo_dun_path", "uses" => "ModuleController@deleteModuleAjax"]);
    Route::post("kiem-tra-mo-dun-cap-nhat/{moduleId}", ["as" => "kiem_tra_mo_dun_cap_nhat_path", "uses" => "ModuleController@checkModuleExist"]);
    Route::post("cap-nhat-mo-dun", ["as" => "cap_nhat_mo_dun_path", "uses" => "ModuleController@editModuleAjax"]);
    Route::post("kiem-tra-ten-mo-dun", ["as" => "kiem_tra_ten_mo_dun_path", "uses" => "ModuleController@checkModuleNameExist"]);

    //Nhóm mô-dun
    Route::get("nhom-mo-dun", ["as" => "nhom_mo_dun_page_path", "uses" => "GroupModuleController@index"])->middleware("AuthenticateLogin");
    Route::post("nhom-mo-dun", ["as" => "nhom_mo_dun_page_path", "uses" => "GroupModuleController@takeGroupModuleSearchData"])->middleware("AuthenticateLogin");
    Route::get("nhom-mo-dun/tao-moi-nhom-mo-dun", ["as" => "tao_moi_nhom_mo_dun_page_path", "uses" => "GroupModuleController@create"])->middleware("AuthenticateLogin");
    Route::get("nhom-mo-dun/cap-nhat-nhom-mo-dun/{id}", ["as" => "cap_nhat_nhom_mo_dun_page_path", "uses" => "GroupModuleController@edit"])->middleware("AuthenticateLogin");
    //Call Ajax
    Route::post("tao-moi-nhom-mo-dun", ["as" => "tao_moi_nhom_mo_dun_path", "uses" => "GroupModuleController@createGroupModuleAjax"]);
    Route::post("nhom-mo-dun-theo-trang", ["as" => "danh_sach_nhom_mo_dun_theo_so_trang_path", "uses" => "GroupModuleController@takeGroupModulePageAjax"]);
    Route::post("xoa-nhom-mo-dun", ["as" => "xoa_nhom_mo_dun_path", "uses" => "GroupModuleController@deleteGroupModuleAjax"]);
    Route::post("kiem-tra-nhom-mo-dun-cap-nhat/{groupModuleId}", ["as" => "kiem_tra_nhom_mo_dun_cap_nhat_path", "uses" => "GroupModuleController@checkGroupModuleExist"]);
    Route::post("cap-nhat-nhom-mo-dun", ["as" => "cap_nhat_nhom_mo_dun_path", "uses" => "GroupModuleController@editGroupModuleAjax"]);
    Route::post("kiem-tra-nhom-mo-dun", ["as" => "kiem_tra_nhom_mo_dun_path", "uses" => "GroupModuleController@checkGroupModuleNameExist"]);


    //Nhóm người dùng
    Route::get("nhom-nguoi-dung", ["as" => "nhom_nguoi_dung_page_path", "uses" => "GroupUserController@index"])->middleware("AuthenticateLogin");
    Route::get("nhom-nguoi-dung/tao-nhom-moi-nguoi-dung", ["as" => "tao_moi_nhom_nguoi_dung_page_path", "uses" => "GroupUserController@create"])->middleware("AuthenticateLogin");
    //Call Ajax
    Route::post("tao-moi-nhom-nguoi-dung", ["as" => "tao_moi_nhom_nguoi_dung_path", "uses" => "GroupUserController@createGroupUserAjax"]);


    //Người dùng
    Route::get("nguoi-dung", ["as" => "nguoi_dung_page_path", "uses" => "UserController@index"])->middleware("AuthenticateLogin");
    Route::post("nguoi-dung", ["as" => "nguoi_dung_page_path", "uses" => "UserController@takeProfileSearchData"])->middleware("AuthenticateLogin");
    Route::get("nguoi-dung/tao-moi-nguoi-dung", ["as" => "tao_moi_nguoi_dung_page_path", "uses" => "UserController@create"])->middleware("AuthenticateLogin");
    Route::get("nguoi-dung/cap-nhat-nguoi-dung/{id}", ["as" => "cap_nhat_nguoi_dung_page_path", "uses" => "UserController@edit"])->middleware("AuthenticateLogin");
    //Call Ajax
    Route::post("tao-moi-nguoi-dung", ["as" => "tao_moi_nguoi_dung_path", "uses" => "UserController@createUserAjax"]);
    Route::post("nguoi-dung-theo-trang", ["as" => "nguoi_dung_theo_so_trang_path", "uses" => "UserController@takeProfilePageAjax"]);
    Route::post("xoa-ho-so", ["as" => "xoa_ho_so_path", "uses" => "UserController@deleteProfileAjax"]);
    Route::post("kiem-tra-ten-dang-nhap-cap-nhat/{userId}", ["as" => "kiem_tra_ten_dang_nhap_cap_nhat_path", "uses" => "UserController@checkProfileExist"]);
    Route::post("cap-nhat-nguoi-dung", ["as" => "cap_nhat_nguoi_dung_path", "uses" => "UserController@editProfileAjax"]);
    Route::post("kiem-tra-ten-dang-nhap", ["as" => "kiem_tra_ten_dang_nhap_path", "uses" => "UserController@checkUserNameExist"]);    
    Route::post("thay-doi-mat-khau", ["as" => "thay_doi_mat_khau_path", "uses" => "UserController@changePasswordAjax"]);
    Route::post("offlineAgency", ["as" => "offline_agency_path", "uses" => "UserController@offlineAgencyAjax"]);
    Route::post("blockUser", ["as" => "block_user_path", "uses" => "UserController@blockAgencyAjax"]);

    //Mô-đun và quyền hạn
    Route::get("mo-dun-va-quyen-han", ["as" => "mo_dun_va_quyen_han_page_path", "uses" => "ModulePermissionController@index"])->middleware("AuthenticateLogin");
    Route::get("mo-dun-va-quyen-han/tao-moi-mo-dun-va-quyen-han", ["as" => "tao_moi_mo_dun_va_quyen_han_page_path", "uses" => "ModulePermissionController@create"])->middleware("AuthenticateLogin");
    Route::post("tao-moi-mo-dun-va-quyen-han", ["as" => "tao_moi_mo_dun_va_quyen_han_path", "uses" => "ModulePermissionController@createModulePermission"])->middleware("AuthenticateLogin");
    Route::get("mo-dun-va-quyen-han/cap-nhat-mo-dun-va-quyen-han/{id}", ["as" => "cap_nhat_mo_dun_va_quyen_han_page_path", "uses" => "ModulePermissionController@edit"])->middleware("AuthenticateLogin");
    Route::post("cap-nhat-mo-dun-va-quyen-han", ["as" => "cap_nhat_mo_dun_va_quyen_han_path", "uses" => "ModulePermissionController@editModulePermission"])->middleware("AuthenticateLogin");
    //Call Ajax
    Route::post("mo-dun-va-quyen-han-theo-trang", ["as" => "tao_moi_mo_dun_va_quyen_han_theo_so_trang_path", "uses" => "ModulePermissionController@takeModulePermissionPageAjax"]);
    Route::post("xoa-mo-dun-va-quyen-han", ["as" => "xoa_mo_dun_va_quyen_han_path", "uses" => "ModulePermissionController@deleteModulePermissionAjax"]);
});

Route::group(["prefix" => "quan-ly-code"], function() {
    //Danh sách giftcode
    Route::get("danh-sach-goi", ["as" => "danh_sach_giftcode_page_path", "uses" => "PermissionController@index"])->middleware("AuthenticateLogin");

    //Chương trình ưu đãi
    Route::get("chuong-trinh-uu-dai", ["as" => "chuong_trinh_uu_dai_page_path", "uses" => "PermissionController@index"])->middleware("AuthenticateLogin");

    //Lich sử giao dich
    Route::get("lich-su-giao-dich", ["as" => "lich_su_giao_dich_page_path", "uses" => "PermissionController@index"])->middleware("AuthenticateLogin");
});

Route::group(["prefix" => "nhan-ma-giftcode"], function() {
    //Danh sách đã mua
    Route::get("danh-sach-da-mua", ["as" => "danh_sach_da_mua_page_path", "uses" => "PermissionController@index"])->middleware("AuthenticateLogin");

    //Nhận mã giftcode
    Route::get("nhan-ma-giftcode", ["as" => "nhan_ma_giftcode_page_path", "uses" => "PermissionController@index"])->middleware("AuthenticateLogin");

    //Xuất bán giftcode
    Route::get("xuat-ban-giftcode", ["as" => "xuat_ban_giftcode_page_path", "uses" => "PermissionController@index"])->middleware("AuthenticateLogin");
});