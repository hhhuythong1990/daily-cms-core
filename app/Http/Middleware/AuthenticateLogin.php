<?php
namespace App\Http\Middleware;

use Closure;
use Crypt;
use Illuminate\Contracts\Cookie\Factory;
use App\Model\User;
use App\Helpers\Utilities;

class AuthenticateLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function __construct() {
        $this->code_response = Utilities::apiCodeResponse();
    }

    public function handle($request, Closure $next)
    {
        $userLogin = $request->session()->has("profile");
        if(!$userLogin){
            $cookieProfile = $request->cookie('profileId');
            if(isset($cookieProfile)){
                $decryptCookieProfile = Crypt::decrypt($cookieProfile);
                $dataPost = [
                    "profile_id" => $decryptCookieProfile,
                    "is_online" => 'false'
                ];
                $dataResponse = User::changeOnlineStatus("POST", $dataPost);
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_update_successfully"]){
                        return redirect("dang-nhap");
                    }
                }
            }else {
                return redirect("dang-nhap");
            }
        }else {
            $userProfile = $request->session()->get("profile");
            $dataPost = [
                "profile_id" => $userProfile->_id
            ];
            $dataResponse = User::takeProfileChange("POST", $dataPost);
            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["check_profile_changed"]){
                    $dataCheck = $dataResponse->data;
                    if(isset($dataCheck)){
                        if(isset($dataCheck->locked)){
                            return redirect(route("dang_xuat_path"));
                        }else if(isset($dataCheck->dataUpdate)) {
                            session()->put("profile", $dataCheck->dataUpdate);
                        }
                    }
                }
            }
        }
        return $next($request);
    } 
}