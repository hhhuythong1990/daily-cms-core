<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Module;
use App\Model\Permission;
use App\Helpers\Utilities;
use App\Helpers\Helper;

class PermissionController extends Controller {
    private $code_response;
    private $default_limit;
    private $default_current_page;
    private $menu = null;
    private $permissions = null;
    private $user_id = null;
    private $data = null;
    private $is_viewed = false;
    private $is_created = false;
    private $is_edited = false;
    private $is_deleted = false;
    private $search_data = null;

    public function __construct() {
        $this->title_active = "Quyền hạn";
        $this->default_current_page = Utilities::pagination()["DEFAULT_CURRENT_PAGE"];
        $this->default_limit = Utilities::pagination()["DEFAULT_PAGE_10"];        
        $this->code_response = Utilities::apiCodeResponse();
        if(session()->has("profile")){
            $this->menu = session()->get("profile")->group_modules;
            $this->permissions = session()->get("profile")->permissions;
            $this->user_id = session()->get("profile")->_id;
            $this->is_viewed = in_array(Utilities::constantPermissions()["PERMISSION_VIEW"], $this->permissions);
            $this->is_created = in_array(Utilities::constantPermissions()["PERMISSION_CREATE"], $this->permissions);
            $this->is_edited = in_array(Utilities::constantPermissions()["PERMISSION_EDIT"], $this->permissions);
            $this->is_deleted = in_array(Utilities::constantPermissions()["PERMISSION_DELETE"], $this->permissions);
        }
    }

    public function index(Request $request) {        
        if($this->is_viewed){
            $dataResponse = Permission::takePermissionBySkipLimit($this->default_current_page, $this->default_limit);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                    $this->data["total"] = $dataResponse->total;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            
            return view("permission.index")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu)
                ->with("search_data", $this->search_data);
        }
        else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function create() {        
        if($this->is_created){
            return view("permission.create")
                ->with("title", $this->title_active)
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }        
    }

    public function createPermissionAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_created){
                $this->validate($request, [
                    "permission_name" => "required",
                    "publish" => "required"
                ],[
                    "permission_name.required" => "Tên quyền hạn không được để trống",
                    "publish.required" => "Công bố không được để trống"
                ]);
                $dataPost = [
                    "permission_name" => $request->permission_name,
                    "permission_name_slug" => strtoupper(Helper::rewriteUnderscore($request->permission_name)),
                    "publish" => $request->publish,
                    "user_create" => $this->user_id
                ];
                $dataResponse = Permission::createPermission("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_add_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }                
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function deletePermissionAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_deleted){
                $this->validate($request, [
                    "item_id" => "required"
                ],[
                    "item_id.required" => "Mã định danh quyền hạn không được để trống"
                ]);                
                $dataPost = [
                    "permission_id" => $request->item_id
                ];                
                $dataResponse = Permission::deletePermission("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_delete_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function takePermissionPageAjax(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){      
                $skip = ((int) $request->page != 0) ? $request->page : $this->default_current_page;        
                $dataSearch = $request->data_search;

                if($dataSearch == null){
                    $dataResponse = Permission::takePermissionBySkipLimit($skip, $this->default_limit);
                }else {
                    $permissionName = $request->data_search;
                    $dataResponse = Permission::takePermissionByDataSearchSkipLimit($skip, $this->default_limit, urlencode($permissionName));
                }
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_found_data"]){
                        $this->data = $dataResponse->data;
                        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], false, $this->drawDataPermission($this->data, $skip));
                    }
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function takePermissionSearchData(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){
                $permissionName = $request->permission_name;                
                if($permissionName != null){
                    $skip = $this->default_current_page;
                
                    $dataResponse = Permission::takePermissionByDataSearchSkipLimit($skip, $this->default_limit, urlencode($permissionName));

                    if(isset($dataResponse)) {
                        if($dataResponse->code == $this->code_response["code_found_data"]){
                            $this->data["data"] = $dataResponse->data;
                            $this->data["total"] = $dataResponse->total;
                            $this->search_data = $permissionName;
                        }else{
                            $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                        }
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                    }
                    
                    return view("permission.index")
                    ->with("title", $this->title_active)
                    ->with("data", $this->data["data"])
                    ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                    ->with("list_permission", $this->permissions)
                    ->with("list_menu", $this->menu)
                    ->with("search_data", $this->search_data);
                }else{
                    return redirect(route("quyen_han_page_path"));
                }
            }else {
                return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
            }
        }
    }

    public function update($permissionId, Request $request) {        
        if($this->is_edited){
            $this->title_active = "Quyền hạn";
            $dataResponse = Permission::takePermissionById($permissionId);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            
            return view("permission.edit")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu);
        }
        else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function checkPermissionExist($permissionId, Request $request){
        if($this->is_edited){
            $dataPost = [
                "permission_id" => $permissionId,
                "permission_name" => $request->permission_name
            ];
            $dataResponse = Permission::takePermissionByIdAndPermissionName("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    public function checkPermissionNameExist(Request $request){
        if($this->is_created){
            $dataPost = [
                "permission_name" => $request->permission_name
            ];
            $dataResponse = Permission::takePermissionByPermissionName("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    public function updatePermission(Request $request){
        if($this->is_edited){
            if(session()->has("profile")){        
                $this->validate($request, [
                    "permission_id" => "required",
                    "permission_name" => "required",
                    "publish" => "required"
                ],[
                    "permission_id.required" => "Mã quyền hạn không được để trống",
                    "permission_name.required" => "Tên quyền hạn không được để trống",
                    "publish.required" => "Công bố không được để trống"
                ]);
                $dataPost = [
                    "permission_id" => $request->permission_id,
                    "permission_name" => $request->permission_name,
                    "permission_name_slug" => strtoupper(Helper::rewriteUnderscore($request->permission_name)),
                    "publish" => $request->publish,
                    "user_update" => $this->user_id
                ];
                
                $dataResponse = Permission::updatePermission("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_update_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    private function drawDataPermission($data, $page_num) {
        if(!empty($data)){
            $str = "";
            $no = Utilities::numberIncrease($page_num, $this->default_limit);
            foreach($data as $key => $value){
                $str .= "<tr>";
                $str .= "<td>" . ($no + $key + 1) . "</td>";
                $str .= "<td>" . $value->_id . "</td>";
                $str .= "<td>" . $value->permission_name  . "</td>";
                $str .= "<td>" . (($value->is_publish)?"Công khai":"Không công khai") . "</td>";                
                $str .= "<td>" . $value->day_create_string . "</td>";
                $str .= "<td>" . $value->day_update_string . "</td>";
                $str .= "<td>" . $value->user_create->full_name . "</td>";
                $str .= "<td>" . ((isset($value->user_update)) ? $value->user_update->full_name : "" ). "</td>";
                $str .= "<td>";
                if($this->is_edited){
                    $str .= "<a href='/cai-dat/quyen-han/cap-nhat-quyen-han/" . $value->_id . "' class='btn btn-white btn-warning btn-xs'>
                            <i class='ace-icon fa fa-wrench  bigger-110 icon-only'></i>
                        </a>&nbsp;";
                }
                if($this->is_deleted){
                    $str .= "<button data-toggle='modal' data-target='#myModal' onclick='storeId(\"" . $value->_id ."\")' class='btn btn-white btn-danger btn-xs'>
                            <i class='ace-icon fa fa-trash-o  bigger-110 icon-only'></i>
                        </button>";
                }                
                $str .= "</td>";
                $str .= "</tr>";
            }
            return $str;
        }
    }

}