<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Model\Module;
use App\Model\GroupUser;
use App\Model\User;
use App\Model\Agency;
use App\Helpers\Utilities;
use App\Helpers\Helper;

class AgencyController extends Controller {
    private $code_response;
    private $default_limit;
    private $default_current_page;
    private $menu = null;
    private $permissions = null;
    private $user_id = null;
    private $data = null;
    private $is_viewed = false;
    private $is_created = false;
    private $is_edited = false;
    private $is_deleted = false;
    private $is_offline = false;
    private $is_locked = false;
    private $search_data = null;

    public function __construct() {
        $this->title_active = "đại lý";
        $this->default_current_page = Utilities::pagination()["DEFAULT_CURRENT_PAGE"];
        $this->default_limit = Utilities::pagination()["DEFAULT_PAGE_10"];        
        $this->code_response = Utilities::apiCodeResponse();
        if(session()->has("profile")){
            $this->menu = session()->get("profile")->group_modules;
            $this->permissions = session()->get("profile")->permissions;
            $this->user_id = session()->get("profile")->_id;
            $this->is_viewed = in_array(Utilities::constantPermissions()["AGENCY_VIEW"], $this->permissions);
            $this->is_created = in_array(Utilities::constantPermissions()["AGENCY_CREATE"], $this->permissions);
            $this->is_edited = in_array(Utilities::constantPermissions()["AGENCY_EDIT"], $this->permissions);
            $this->is_deleted = in_array(Utilities::constantPermissions()["AGENCY_DELETE"], $this->permissions);
            $this->is_offline = in_array(Utilities::constantPermissions()["USER_OFFLINE"], $this->permissions);
            $this->is_locked = in_array(Utilities::constantPermissions()["USER_BLOCK"], $this->permissions);
        }
    }

    public function index(Request $request) {        
        if($this->is_viewed){
            $dataResponse = Agency::takeAgencyBySkipLimit($this->default_current_page, $this->default_limit);
            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                    $this->data["total"] = $dataResponse->total;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            
            return view("agency.index")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu)
                ->with("search_data", $this->search_data);
        }
        else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function create() {        
        if($this->is_created){
            $dataResponse = Agency::getDataForCreateAgency();
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data = $dataResponse->data;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("agency.create")
                ->with("title", $this->title_active)
                ->with("groupModules", $this->data->groupModules)
                ->with("permissions", $this->data->permissions)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }        
    }

    public function createAgencyAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_created){
                $this->validate($request, [
                    "full_name" => "required",
                    "agency_code" => "required",
                    "user_name" => "required",
                    "password" => "required",
                    "group_modules" => "required",
                    "permissions" => "required",
                    "full_name_represent" => "required",
                    "id_card" => "required",
                    "card_front" => "required",
                    "card_behind" => "required",
                    "phone" => "required",
                    "email" => "required",
                    "address" => "required",
                    "payment_limit" => "required"
                ],[
                    "full_name.required" => "Họ và tên không được để trống",
                    "user_name.required" => "Tên đăng nhập không được để trống",
                    "agency_code.required" => "Mã đại lý không được để trống",
                    "password.required" => "Mật khẩu không được để trống",
                    "group_modules.required" => "Danh sách nhóm mô-đun không được để trống",
                    "permissions.required" => "Quyền hạn phải được chọn",
                    "full_name_represent" => "Họ và tên người đại diện không được để trống",
                    "id_card.required" => "Số chứng minh nhân dân không được để trống",
                    "card_front.required" => "Hình chứng minh mặt trước không được để trống",
                    "card_behind.required" => "Hình chứng minh mặt sau không được để trống",
                    "phone.required" => "Số điện thoại không được để trống",
                    "email.required" => "Email không được để trống",
                    "address.required" => "Địa chỉ không được để trống",
                    "payment_limit.required" => "Hạn mức không được để trống"
                ]);
                
                $group_parent = session()->get("profile")->group_parent;
                if($group_parent == null){
                    $dataPost = [
                        "super_id" => session()->get("profile")->group_user
                    ];
                    $groupAdmin = GroupUser::takeGroupAdmin("POST", $dataPost);
                    if(isset($groupAdmin) && ($groupAdmin->code == $this->code_response["code_found_data"])){
                        $group_parent = $groupAdmin->data;
                    }
                }else {
                    $group_parent = session()->get("profile")->group_user;
                }
                
                $dataPost = [
                    "full_name" => $request->full_name,
                    "user_name" => strtolower($request->user_name),
                    "password" => $request->password,
                    "agency_code" => strtoupper($request->agency_code),
                    "group_modules" => $request->group_modules,
                    "group_parent" => $group_parent,
                    "permissions" => $request->permissions,
                    "publish" => true,
                    "user_create" => $this->user_id,
                    "full_name_represent" => $request->full_name_represent,
                    "id_card" => $request->id_card,
                    "image_id_card_front" => $this->processUploadImage($request->card_front, $request->user_name."front"),
                    "image_id_card_behind" => $this->processUploadImage($request->card_behind, $request->user_name."behind"),
                    "phone" => $request->phone,
                    "email" => $request->email,
                    "address" => $request->address,
                    "is_locked" => false,
                    "payment_limit" => $request->payment_limit
                ];        
                $dataResponse = Agency::createAgency("POST", $dataPost);
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_add_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
                return redirect(route("tao_moi_dai_ly_page_path"));
            }else {
                return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
            }
        }
        // return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
        
    }

    public function checkAgencyCodeAjax(Request $request){
        if($this->is_created){
            $dataPost = [
                "agency_code" => $request->agency_code
            ];
            $dataResponse = Agency::takeAgencyByAgencyCode("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    private function processUploadImage($dataBase64, $nameImage){
        if(!empty($dataBase64)){
            $baseImage = explode(",", $dataBase64);
            $decoded = base64_decode($baseImage[1]);
            if(str_contains($baseImage[0], "jpeg")){
                $extension = "jpg";
            }else{
                $extension = "png";
            }
            
            $fileName = str_replace(' ','',$nameImage);
            $nameConvert = Helper::rewrite($fileName).time();
            $path = Config::get("constants.image_patch").$nameConvert.".".$extension;
            file_put_contents($path, $decoded);
            return $img_path = Config::get("constants.add_on_path").$nameConvert.".".$extension;
        }else{
            return $img_path = null;
        }
    }

    public function takeAgencySearchData(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){
                $agencyName = $request->agency_name;                
                if($agencyName != null){
                    $skip = $this->default_current_page;
                    $dataResponse = Agency::takeByDataSearchSkipLimit($skip, $this->default_limit, urlencode($agencyName));                    
                    if(isset($dataResponse)) {
                        if($dataResponse->code == $this->code_response["code_found_data"]){
                            $this->data["data"] = $dataResponse->data;
                            $this->data["total"] = $dataResponse->total;
                            $this->search_data = $agencyName;
                        }else{
                            $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                        }
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                    }
                    
                    return view("agency.index")
                    ->with("title", $this->title_active)
                    ->with("data", $this->data["data"])
                    ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                    ->with("list_permission", $this->permissions)
                    ->with("list_menu", $this->menu)
                    ->with("search_data", $this->search_data);
                }else{
                    return redirect(route("danh_sach_dai_ly_page_path"));
                }
            }else {
                return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
            }
        }
    }

    public function edit($id, Request $request) {
        if($this->is_edited){
            $dataResponse = Agency::takeAgencyByAgencyId($id);        
            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["agency"] = $dataResponse->data;
                    $dataUpdate = Agency::getDataForCreateAgency();
                    if(isset($dataUpdate)) {
                        if($dataUpdate->code == $this->code_response["code_found_data"]){
                            $this->data["update"] = $dataUpdate->data;
                            
                        }else {
                            $request->session()->flash(Utilities::status_alert()["status_error"] , $dataUpdate->msg);
                        }
                    } else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                    }
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            
            return view("agency.edit")
                ->with("title", $this->title_active)
                ->with("data", $this->data["agency"])
                ->with("permissions", $this->data["update"]->permissions)
                ->with("groupModules", $this->data["update"]->groupModules)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        } 
    }

    public function checkAgencyExist($agencyId, Request $request){
        if($this->is_edited){
            $dataPost = [
                "agency_id" => $agencyId,
                "user_name" => $request->user_name
            ];
            $dataResponse = Agency::takeAgencyByIdAndUsername("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    public function editAgencyAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_created){
                $this->validate($request, [
                    "profile_id" => "required",
                    "agency_id" => "required",
                    "full_name" => "required",
                    // "agency_code" => "required",
                    "user_name" => "required",
                    "group_modules" => "required",
                    "permissions" => "required",
                    "full_name_represent" => "required",
                    "id_card" => "required",
                    // "image_id_card_front" => "required",
                    // "image_id_card_behind" => "required",
                    "phone" => "required",
                    "email" => "required",
                    "address" => "required",
                    "payment_limit" => "required"
                ],[
                    "profile_id.required" => "Mã định danh hồ sơ không được để trống",
                    "agency_id.required" => "Mã định danh đại lý không được để trống",
                    "full_name.required" => "Họ và tên không được để trống",
                    "user_name.required" => "Tên đăng nhập không được để trống",
                    // "agency_code.required" => "Mã đại lý không được để trống",
                    "group_modules.required" => "Danh sách nhóm mô-đun không được để trống",
                    "permissions.required" => "Quyền hạn phải được chọn",
                    "full_name_represent" => "Họ và tên người đại diện không được để trống",
                    "id_card.required" => "Số chứng minh nhân dân không được để trống",
                    // "image_id_card_front.required" => "Hình chứng minh mặt trước không được để trống",
                    // "image_id_card_behind.required" => "Hình chứng minh mặt sau không được để trống",
                    "phone.required" => "Số điện thoại không được để trống",
                    "email.required" => "Email không được để trống",
                    "address.required" => "Địa chỉ không được để trống",
                    "payment_limit.required" => "Hạn mức không được để trống"
                ]);
                $group_parent = session()->get("profile")->group_parent;
                if($group_parent == null){
                    $dataPost = [
                        "super_id" => session()->get("profile")->group_user
                    ];
                    $groupAdmin = GroupUser::takeGroupAdmin("POST", $dataPost);
                    if(isset($groupAdmin) && ($groupAdmin->code == $this->code_response["code_found_data"])){
                        $group_parent = $groupAdmin->data;
                    }
                }else {
                    $group_parent = session()->get("profile")->group_user;
                }
                $cardFront = explode(",", $request->image_id_card_front);
                if(isset($cardFront[1])){
                    $cardFront = $this->processUploadImage($request->image_id_card_front, $request->user_name."front");
                }else {
                    $cardFront = "";
                }
                $cardBehind = explode(",", $request->image_id_card_behind);
                if(isset($cardBehind[1])){
                    $cardBehind = $this->processUploadImage($request->image_id_card_behind, $request->user_name."behind");
                }else {
                    $cardBehind = "";
                }

                $dataPost = [
                    "profile_id" => $request->profile_id,
                    "agency_id" => $request->agency_id,
                    "full_name" => $request->full_name,
                    "user_name" => $request->user_name,
                    "password" => $request->password,
                    // "agency_code" => $request->agency_code,
                    "group_modules" => $request->group_modules,
                    "group_parent" => $group_parent,
                    "permissions" => $request->permissions,
                    "publish" => true,
                    "user_update" => $this->user_id,
                    "full_name_represent" => $request->full_name_represent,
                    "id_card" => $request->id_card,
                    "image_id_card_front" => $cardFront,
                    "image_id_card_behind" => $cardBehind,
                    "phone" => $request->phone,
                    "email" => $request->email,
                    "address" => $request->address,
                    "is_locked" => false,
                    "payment_limit" => $request->payment_limit
                ];        
                $dataResponse = Agency::updateAgency("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_update_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function takeAgencySearchPageAjax(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){      
                $skip = ((int) $request->page != 0) ? $request->page : $this->default_current_page;        
                $dataSearch = $request->data_search;

                if($dataSearch == null){
                    $dataResponse = Agency::takeAgencyBySkipLimit($skip, $this->default_limit);
                }else {
                    $userName = $request->data_search;
                    $dataResponse = Agency::takeByDataSearchSkipLimit($skip, $this->default_limit, urlencode($userName));
                }
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_found_data"]){
                        $this->data = $dataResponse->data;
                        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], false, $this->drawAgency($this->data, $skip));
                    }
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    private function drawAgency($data, $page_num) {
        if(!empty($data)){
            $str = "";
            $no = Utilities::numberIncrease($page_num, $this->default_limit);;
            foreach($data as $key => $value){
                $str .= "<tr>";
                $str .= "<td>" . ($no + $key + 1) . "</td>";
                $str .= "<td>" . $value->full_name  . "</td>";
                $str .= "<td>" . $value->user_name  . "</td>";
                $str .= "<td>" . $value->is_agency->agency_code  . "</td>";
                $str .= "<td>" . $value->is_agency->full_name_represent  . "</td>";
                $str .= "<td>" . $value->is_agency->id_card . "</td>";
                $str .= "<td>" . $value->is_agency->phone . "</td>";
                $str .= "<td>" . $value->is_agency->email . "</td>";
                $str .= "<td>" . $value->is_agency->payment_limit . "</td>";
                $str .= "<td>" . (($value->is_publish)?"Công khai":"Không công khai") . "</td>";
                if($this->is_locked){
                    $str .= "<td>";
                    $str .= "<label class='inline'>";
                    $str .= "<input " . (($value->is_locked == true)?"checked":"") ." type='checkbox' class='ace ace-switch ace-switch-5' 
                        onchange='changeBlockStatus(\"". $value->_id . "\",\"" . (($value->is_locked)?0:1). "\")'/>";
                    $str .= "<span class='lbl middle'></span>";
                    $str .= "</label>";
                    $str .= "</td>";
                }
                if($this->is_offline){
                    $str .= "<td>";
                    $str .= "<label class='inline'>";
                    $str .= "<input ". (($value->is_online == true)?"checked":"" ). " type='checkbox' class='ace ace-switch ace-switch-5' " 
                        . (($value->is_online == true)?"":"disabled" ) . " onchange='changeOnlineStatus(\"".$value->_id ."\",\"". (($value->is_online)?1:0 ). "\")' />";
                    $str .= "<span class='lbl middle'></span>";
                    $str .= "</label>";
                    $str .= "</td>";
                }
                $str .= "<td>" . $value->day_create_string . "</td>";
                $str .= "<td>" . $value->day_update_string . "</td>";
                $str .= "<td>" . $value->user_create->full_name . "</td>";
                $str .= "<td>" . ((isset($value->user_update)) ? $value->user_update->full_name : "" ). "</td>";
                $str .= "<td>";
                if($this->is_edited){
                    $str .= "<a href='". route("cap_nhat_dai_ly_page_path", ["id" => $value->_id]) ."' class='btn btn-white btn-warning btn-xs'>
                            <i class='ace-icon fa fa-wrench  bigger-110 icon-only'></i>
                        </a>&nbsp;";
                }
                if($this->is_deleted){
                    $str .= "<button data-toggle='modal' data-target='#myModal' onclick='storeId(\"" . $value->_id ."\")' class='btn btn-white btn-danger btn-xs'>
                            <i class='ace-icon fa fa-trash-o  bigger-110 icon-only'></i>
                        </button>";
                }                
                $str .= "</td>";
                $str .= "</tr>";
            }
            return $str;
        }
    }

    public function deleteAgencyAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_deleted){
                $this->validate($request, [
                    "item_id" => "required"
                ],[
                    "item_id.required" => "Mã hồ sơ không được để trống"
                ]);                
                $dataPost = [
                    "profile_id" => $request->item_id
                ];                
                $dataResponse = User::deleteProfile("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_delete_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }
}

?>