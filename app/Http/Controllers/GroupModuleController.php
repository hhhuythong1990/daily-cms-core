<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Module;
use App\Model\GroupModule;
use App\Helpers\Utilities;
use App\Helpers\Helper;

class GroupModuleController extends Controller {
    private $code_response;
    private $default_limit;
    private $default_current_page;
    private $menu = null;
    private $permissions = null;
    private $user_id = null;
    private $data = null;
    private $is_viewed = false;
    private $is_created = false;
    private $is_edited = false;
    private $is_deleted = false;
    private $search_data = null;
    
    public function __construct() {
        $this->title_active = "Nhóm mô-đun";
        $this->default_current_page = Utilities::pagination()["DEFAULT_CURRENT_PAGE"];
        $this->default_limit = Utilities::pagination()["DEFAULT_PAGE_10"];        
        $this->code_response = Utilities::apiCodeResponse();
        if(session()->has("profile")){
            $this->menu = session()->get("profile")->group_modules;
            $this->permissions = session()->get("profile")->permissions;
            $this->user_id = session()->get("profile")->_id;
            $this->is_viewed = in_array(Utilities::constantPermissions()["GROUP_MODULE_VIEW"], $this->permissions);
            $this->is_created = in_array(Utilities::constantPermissions()["GROUP_MODULE_CREATE"], $this->permissions);
            $this->is_edited = in_array(Utilities::constantPermissions()["GROUP_MODULE_EDIT"], $this->permissions);
            $this->is_deleted = in_array(Utilities::constantPermissions()["GROUP_MODULE_DELETE"], $this->permissions);
        }
    }

    public function index(Request $request) {
        if($this->is_viewed){
            $dataResponse = GroupModule::takeGroupModuleBySkipLimit($this->default_current_page, $this->default_limit);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                    $this->data["total"] = $dataResponse->total;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("group_module.index")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu)
                ->with("search_data", $this->search_data);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function create() {
        if($this->is_created){
            $dataResponse = Module::takeAllModule();
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data = $dataResponse->data;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("group_module.create")
                ->with("title", $this->title_active)
                ->with("modules", $this->data)
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu);
        }else {
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);            
        }
    }

    public function edit($id, Request $request) {
        if($this->is_edited){            
            $dataResponse = GroupModule::takeGroupModule($id);
            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["groupModule"] = $dataResponse->data;
                    $dataModule = Module::takeAllModule();
                    
                    if(isset($dataModule)){
                        if($dataModule->code == $this->code_response["code_found_data"]){
                            $this->data["modules"] = $dataModule->data;
                        }else {
                            $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                        }
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                    }
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("group_module.edit")
                ->with("title", $this->title_active)
                ->with("modules", $this->data["modules"])
                ->with("data", $this->data["groupModule"])
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        } 
    }

    public function createGroupModuleAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_created){
                $this->validate($request, [
                    "group_module_description" => "required",
                    "group_module_name" => "required",
                    "modules" => "required",
                    "publish" => "required"
                ],[
                    "group_module_description.required" => "Mô tả nhóm mô-đun không được để trống",
                    "module_name.required" => "Tên nhóm mô-đun không được để trống",
                    "modules.required" => "Danh sách mô-đun không được để trống",
                    "publish.required" => "Công bố phải được chọn",
                ]);
                $dataPost = [
                    "group_module_description" => $request->group_module_description,
                    "group_module_name" => $request->group_module_name,
                    "group_name_slug" => Helper::rewrite($request->group_module_name),
                    "modules" => $request->modules,
                    "publish" => $request->publish,
                    "user_create" => $this->user_id
                ];
                $dataResponse = GroupModule::createGroupModule("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_add_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function takePermissionFollowPageAjax($page, Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){      
                $skip = ((int) $page != 0) ? $page : $this->default_current_page;        
                $dataResponse = Module::takeModuleBySkipLimit($skip, $this->default_limit);
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_found_data"]){
                        $this->data = $dataResponse->data;
                        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], false, $this->drawDataGroupModule($this->data, $skip));
                    }
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function takeGroupModuleSearchData(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){
                $groupModuleName = $request->group_module_name;                
                if($groupModuleName != null){
                    $skip = $this->default_current_page;
                    $dataResponse = GroupModule::takeGroupModuleByDataSearchSkipLimit($skip, $this->default_limit, urlencode($groupModuleName));                    
                    if(isset($dataResponse)) {
                        if($dataResponse->code == $this->code_response["code_found_data"]){
                            $this->data["data"] = $dataResponse->data;
                            $this->data["total"] = $dataResponse->total;
                            $this->search_data = $groupModuleName;
                        }else{
                            $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                        }
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                    }
                    
                    return view("group_module.index")
                    ->with("title", $this->title_active)
                    ->with("data", $this->data["data"])
                    ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                    ->with("list_permission", $this->permissions)
                    ->with("list_menu", $this->menu)
                    ->with("search_data", $this->search_data);
                }else{
                    return redirect(route("nhom_mo_dun_page_path"));
                }
            }else {
                return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
            }
        }
    }

    public function takeGroupModulePageAjax(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){      
                $skip = ((int) $request->page != 0) ? $request->page : $this->default_current_page;        
                $dataSearch = $request->data_search;

                if($dataSearch == null){
                    $dataResponse = GroupModule::takeGroupModuleBySkipLimit($skip, $this->default_limit);
                }else {
                    $permissionName = $request->data_search;
                    $dataResponse = GroupModule::takeGroupModuleByDataSearchSkipLimit($skip, $this->default_limit, urlencode($permissionName));
                }
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_found_data"]){
                        $this->data = $dataResponse->data;
                        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], false, $this->drawDataGroupModule($this->data, $skip));
                    }
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function checkGroupModuleNameExist(Request $request){
        if($this->is_created){
            $dataPost = [
                "group_module_description" => $request->group_module_description
            ];
            $dataResponse = GroupModule::checkGroupModuleByGroupModuleName("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    public function deleteGroupModuleAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_deleted){
                $this->validate($request, [
                    "item_id" => "required"
                ],[
                    "item_id.required" => "Mã định danh mô-đun không được để trống"
                ]);                
                $dataPost = [
                    "group_module_id" => $request->item_id
                ];                
                $dataResponse = GroupModule::deleteGroupModule("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_delete_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function checkGroupModuleExist($moduleId, Request $request){
        if($this->is_edited){
            $dataPost = [
                "group_module_id" => $moduleId,
                "group_module_description" => $request->group_module_description
            ];
            $dataResponse = GroupModule::takeGroupModuleByIdAndGroupModuleName("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    public function editGroupModuleAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_edited){            
                $this->validate($request, [
                    "group_module_description"=> "required",
                    "group_module_id" => "required",
                    "group_module_name" => "required",
                    "publish" => "required",
                    "modules" => "required",
                ],[
                    "group_module_description.required" => "Mô tả nhóm mô-đun không được để trống",
                    "group_module_id.required" => "Mã định danh nhóm mô-đun không được để trống",
                    "group_module_name.required" => "Tên nhóm mô-đun không được để trống",                    
                    "publish.required" => "Công bố phải được chọn",
                    "modules.required" => "Danh sách mô-đun không được để trống",
                ]);
                $group_module_name = $request->group_module_name;
                $dataPost = [
                    "group_module_description" => $request->group_module_description,
                    "group_module_id" => $request->group_module_id,
                    "group_module_name" => $group_module_name,
                    "group_name_slug" => Helper::rewrite($group_module_name),
                    "modules" => $request->modules,
                    "publish" => $request->publish,
                    "user_update" => $this->user_id
                ];

                $dataResponse = GroupModule::updateGroupModule("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_update_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    private function drawDataGroupModule($data, $page_num) {
        if(!empty($data)){
            $str = "";
            $no = Utilities::numberIncrease($page_num, $this->default_limit);;
            foreach($data as $key => $value){
                $str .= "<tr>";
                $str .= "<td>" . ($no + $key + 1) . "</td>";
                $str .= "<td>" . $value->group_name  . "</td>";
                $str .= "<td><ul>";
                foreach($value->modules as $k => $v){
                    $str .= "<li>".$v->module_name ."</li>";
                }
                $str .= "</ul></td>";
                $str .= "<td>" . (($value->is_publish)?"Công khai":"Không công khai") . "</td>";
                $str .= "<td>" . $value->day_create_string . "</td>";
                $str .= "<td>" . $value->day_update_string . "</td>";
                $str .= "<td>" . $value->user_create->full_name . "</td>";
                $str .= "<td>" . ((isset($value->user_update)) ? $value->user_update->full_name : "" ). "</td>";
                $str .= "<td>";
                if($this->is_edited){
                    $str .= "<a href='". route("cap_nhat_nhom_mo_dun_page_path", ["id" => $value->_id]) ."' class='btn btn-white btn-warning btn-xs'>
                            <i class='ace-icon fa fa-wrench  bigger-110 icon-only'></i>
                        </a>&nbsp;";
                }
                if($this->is_deleted){
                    $str .= "<button data-toggle='modal' data-target='#myModal' onclick='storeId(\"" . $value->_id ."\")' class='btn btn-white btn-danger btn-xs'>
                            <i class='ace-icon fa fa-trash-o  bigger-110 icon-only'></i>
                        </button>";
                }                
                $str .= "</td>";
                $str .= "</tr>";
            }
            return $str;
        }
    }
}