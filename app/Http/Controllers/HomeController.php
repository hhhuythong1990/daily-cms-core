<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Model\Module;
use App\Model\GroupUser;
use App\Model\User;
use App\Model\Agency;
use App\Helpers\Utilities;
use App\Helpers\Helper;

class HomeController extends Controller {
    private $code_response;
    private $default_limit;
    private $default_current_page;
    private $menu = null;
    private $permissions = null;
    private $user_id = null;
    private $data = null;
    private $is_viewed = false;
    private $is_created = false;
    private $is_edited = false;
    private $is_deleted = false;
    private $search_data = null;

    public function __construct() {
    }

    public function index(Request $request) {     
        if(session()->has("profile")){ 
            $userData = session()->get("profile");
            $redirect = $userData->group_modules[0]->modules[0]->route_path;            
            return redirect(route($redirect)); 
        }else {
            return redirect(route('dang_nhap_path'));
        }
    }

    public function dangNhap(Request $request){
        if($request->session()->has("profile")){ 
            $userData = session()->get("profile");
            $redirect = $userData->group_modules[0]->modules[0]->route_path;            
            return redirect(route($redirect)); 
        }else {
            return view("login");
        }
    }

}