<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Module;
use App\Model\GroupUser;
use App\Helpers\Utilities;
use App\Helpers\Helper;

class GroupUserController extends Controller {
    private $code_response;
    private $default_limit;
    private $default_current_page;
    private $menu = null;
    private $permissions = null;
    private $user_id = null;
    private $data = null;
    private $is_viewed = false;
    private $is_created = false;
    private $is_edited = false;
    private $is_deleted = false;
    
    public function __construct() {
        $this->title_active = "Nhóm người dùng";
        $this->default_current_page = Utilities::pagination()["DEFAULT_CURRENT_PAGE"];
        $this->default_limit = Utilities::pagination()["DEFAULT_PAGE_10"];        
        $this->code_response = Utilities::apiCodeResponse();
        if(session()->has("profile")){            
            $this->menu = session()->get("profile")->group_modules;
            $this->permissions = session()->get("profile")->permissions;
            $this->user_id = session()->get("profile")->_id;   
            $this->is_viewed = in_array(Utilities::constantPermissions()["GROUP_USER_VIEW"], $this->permissions);
            $this->is_created = in_array(Utilities::constantPermissions()["GROUP_USER_CREATE"], $this->permissions);
            $this->is_edited = in_array(Utilities::constantPermissions()["GROUP_USER_EDIT"], $this->permissions);
            $this->is_deleted = in_array(Utilities::constantPermissions()["GROUP_USER_DELETE"], $this->permissions);
        }
    }

    public function index(Request $request) {
        if($this->is_viewed){
            $dataResponse = GroupUser::takeGroupUserBySkipLimit($this->default_current_page, $this->default_limit);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                    $this->data["total"] = $dataResponse->total;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("group_user.index")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu);
        }else {
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function create() {
        if($this->is_created){
            return view("group_user.create")
                ->with("title", $this->title_active)
                ->with("list_menu", $this->menu);
        }else {
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function createGroupUserAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_created){
                $this->validate($request, [
                    "group_user_name" => "required",
                    "publish" => "required"
                ],[
                    "group_user_name.required" => "Tên nhóm người dùng không được để trống",
                    "publish.required" => "Công bố không được để trống"
                ]);

                $dataPost = [
                    "group_user_name" => $request->group_user_name,
                    "group_user_name_slug" => strtoupper(Helper::rewrite($request->group_user_name)),
                    "publish" => $request->publish,
                    "user_create" => $this->user_id
                ];
                $dataResponse = GroupUser::createGroupUser("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_add_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }
}