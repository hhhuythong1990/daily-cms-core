<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Module;
use App\Model\Permission;
use App\Helpers\Utilities;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Config;

class ModuleController extends Controller {
    private $code_response;
    private $default_limit;
    private $default_current_page;
    private $menu = null;
    private $permissions = null;
    private $user_id = null;
    private $data = null;
    private $is_viewed = false;
    private $is_created = false;
    private $is_edited = false;
    private $is_deleted = false;
    private $search_data = null;
    
    public function __construct() {
        $this->title_active = "Mô-đun";
        $this->default_current_page = Utilities::pagination()["DEFAULT_CURRENT_PAGE"];
        $this->default_limit = Utilities::pagination()["DEFAULT_PAGE_10"];        
        $this->code_response = Utilities::apiCodeResponse();         
        if(session()->has("profile")){
            $this->menu = session()->get("profile")->group_modules;
            $this->permissions = session()->get("profile")->permissions;
            $this->user_id = session()->get("profile")->_id;
            $this->is_viewed = in_array(Utilities::constantPermissions()["MODULE_VIEW"], $this->permissions);
            $this->is_created = in_array(Utilities::constantPermissions()["MODULE_CREATE"], $this->permissions);
            $this->is_edited = in_array(Utilities::constantPermissions()["MODULE_EDIT"], $this->permissions);
            $this->is_deleted = in_array(Utilities::constantPermissions()["MODULE_DELETE"], $this->permissions);
        }
    }

    public function index(Request $request) {
        if($this->is_viewed){
            $dataResponse = Module::takeModuleBySkipLimit($this->default_current_page, $this->default_limit);            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                    $this->data["total"] = $dataResponse->total;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("module.index")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu)
                ->with("search_data", $this->search_data);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }        
    }

    public function takeModulePageAjax(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){      
                $skip = ((int) $request->page != 0) ? $request->page : $this->default_current_page;        
                $dataSearch = $request->data_search;

                if($dataSearch == null){
                    $dataResponse = Module::takeModuleBySkipLimit($skip, $this->default_limit);
                }else {
                    $permissionName = $request->data_search;
                    $dataResponse = Module::takeModuleByDataSearchSkipLimit($skip, $this->default_limit, urlencode($permissionName));
                }
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_found_data"]){
                        $this->data = $dataResponse->data;
                        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], false, $this->drawDataModule($this->data, $skip));
                    }
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function edit($id, Request $request) {
        if($this->is_edited){
            $dataResponse = Module::takeModuleByModuleId($id);        
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data = $dataResponse->data;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("module.edit")
                ->with("title", $this->title_active)
                ->with("data", $this->data)
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        } 
    }

    public function editModuleAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_edited){            
                $this->validate($request, [
                    "module_name" => "required",
                    "module_id" => "required",
                    "publish" => "required",
                ],[
                    "module_name.required" => "Tên mô-đun không được để trống",
                    "module_id.required" => "Mã định danh mô-đun không được để trống",
                    "publish.required" => "Công bố phải được chọn"
                ]);
                $module_name = $request->module_name;
                $dataPost = [
                    "module_id" => $request->module_id,
                    "module_name" => $module_name,
                    "module_name_slug" => Helper::rewrite($module_name),
                    "route_path" => Helper::rewriteUnderscore($module_name) . Config::get("constants.add_on_page"),
                    "publish" => $request->publish,
                    "user_update" => $this->user_id
                ];

                $dataResponse = Module::updateModule("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_update_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }
    
    public function checkModuleExist($moduleId, Request $request){
        if($this->is_edited){
            $dataPost = [
                "module_id" => $moduleId,
                "module_name" => $request->module_name
            ];
            $dataResponse = Module::takeModuleByIdAndModuleName("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    public function create(Request $request) {    
        if($this->is_created){    
            return view("module.create")
                ->with("title", $this->title_active)
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function createModuleAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_created){
                $this->validate($request, [
                    "module_name" => "required",
                    "publish" => "required",
                ],[
                    "module_name.required" => "Tên mô-đun không được để trống",
                    "publish.required" => "Công bố phải được chọn"
                ]);
                $module_name = $request->module_name;
                $dataPost = [
                    "module_name" => $module_name,
                    "module_name_slug" => Helper::rewrite($module_name),
                    "route_path" => Helper::rewriteUnderscore($module_name) . Config::get("constants.add_on_page"),
                    "publish" => $request->publish,
                    "user_create" => $this->user_id
                ];
                $dataResponse = Module::createModule("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_add_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function deleteModuleAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_deleted){
                $this->validate($request, [
                    "item_id" => "required"
                ],[
                    "item_id.required" => "Mã định danh mô-đun không được để trống"
                ]);                
                $dataPost = [
                    "module_id" => $request->item_id
                ];                
                $dataResponse = Module::deleteModule("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_delete_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function takeModuleFollowPageAjax($page, Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){      
                $skip = ((int) $page != 0) ? $page : $this->default_current_page;        
                $dataResponse = Module::takeModuleBySkipLimit($skip, $this->default_limit);
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_found_data"]){
                        $this->data = $dataResponse->data;
                        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], false, $this->drawDataModule($this->data, $skip));
                    }
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function takeModuleSearchData(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){
                $moduleName = $request->module_name;                
                if($moduleName != null){
                    $skip = $this->default_current_page;
                
                    $dataResponse = Module::takeModuleByDataSearchSkipLimit($skip, $this->default_limit, urlencode($moduleName));
                    if(isset($dataResponse)) {
                        if($dataResponse->code == $this->code_response["code_found_data"]){
                            $this->data["data"] = $dataResponse->data;
                            $this->data["total"] = $dataResponse->total;
                            $this->search_data = $moduleName;
                        }else{
                            $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                        }
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                    }
                    
                    return view("module.index")
                    ->with("title", $this->title_active)
                    ->with("data", $this->data["data"])
                    ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                    ->with("list_permission", $this->permissions)
                    ->with("list_menu", $this->menu)
                    ->with("search_data", $this->search_data);
                }else{
                    return redirect(route("mo_dun_page_path"));
                }
            }else {
                return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
            }
        }
    }

    public function checkModuleNameExist(Request $request){
        if($this->is_created){
            $dataPost = [
                "module_name" => $request->module_name
            ];
            $dataResponse = Module::takeModuleByModuleName("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    private function drawDataModule($data, $page_num) {
        if(!empty($data)){
            $str = "";
            $no = Utilities::numberIncrease($page_num, $this->default_limit);;
            foreach($data as $key => $value){
                $str .= "<tr>";
                $str .= "<td>" . ($no + $key + 1) . "</td>";
                $str .= "<td>" . $value->module_name  . "</td>";
                $str .= "<td>" . $value->route_path . "</td>";
                $str .= "<td>" . (($value->is_publish)?"Công khai":"Không công khai") . "</td>";
                $str .= "<td>" . $value->day_create_string . "</td>";
                $str .= "<td>" . $value->day_update_string . "</td>";
                $str .= "<td>" . $value->user_create->full_name . "</td>";
                $str .= "<td>" . ((isset($value->user_update)) ? $value->user_update->full_name : "" ). "</td>";
                $str .= "<td>";
                if($this->is_edited){
                    $str .= "<a href='/cai-dat/mo-dun/cap-nhat-mo-dun/" . $value->_id . "' class='btn btn-white btn-warning btn-xs'>
                            <i class='ace-icon fa fa-wrench  bigger-110 icon-only'></i>
                        </a>&nbsp;";
                }
                if($this->is_deleted){
                    $str .= "<button data-toggle='modal' data-target='#myModal' onclick='storeId(\"" . $value->_id ."\")' class='btn btn-white btn-danger btn-xs'>
                            <i class='ace-icon fa fa-trash-o  bigger-110 icon-only'></i>
                        </button>";
                }                
                $str .= "</td>";
                $str .= "</tr>";
            }
            return $str;
        }
    }
}