<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Revenue;
use App\Helpers\Utilities;
use App\Helpers\Helper;
use Excel;

class RevenueController extends Controller {
    private $code_response;
    private $default_limit;
    private $default_current_page;
    private $menu = null;
    private $permissions = null;
    private $user_id = null;
    private $data = null;
    private $is_viewed = false;
    private $is_created = false;
    private $is_edited = false;
    private $is_deleted = false;
    private $search_data = null;

    public function __construct() {
        $this->title_active = "Doanh thu";
        $this->default_current_page = Utilities::pagination()["DEFAULT_CURRENT_PAGE"];
        $this->default_limit = Utilities::pagination()["DEFAULT_PAGE_10"];        
        $this->code_response = Utilities::apiCodeResponse();
        if(session()->has("profile")){
            $this->menu = session()->get("profile")->group_modules;
            $this->permissions = session()->get("profile")->permissions;
            $this->user_id = session()->get("profile")->_id;
            $this->is_viewed = in_array(Utilities::constantPermissions()["REVENUE_VIEW"], $this->permissions);
            $this->is_searched = in_array(Utilities::constantPermissions()["REVENUE_SEARCH"], $this->permissions);
            $this->is_exported = in_array(Utilities::constantPermissions()["REVENUE_EXPORT"], $this->permissions);
        }
    }

    public function index (Request $request){
        if($this->is_viewed){
            $dataPost = [
                "limit" => $this->default_limit,
                "skip" => $this->default_current_page,
                "data_search" => (object) [
                    "agency_name" => null,
                    "date_start" => null,
                    "date_end" => null
                ]
            ];

            $dataResponse = Revenue::getData("POST", $dataPost);
            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                    $this->data["total"] = $dataResponse->total;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("revenue.revenue_manage")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu)
                ->with("search_data", $this->search_data);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function searchRevenue (Request $request){
        if($this->is_searched){
            $agencyName = null;
            $dateStart = null;
            $dateEnd = null;

            if($request->agency_name != null){
                $agencyName = $request->agency_name;
            }
            if($request->date_start != null){
                $dateStart = $request->date_start . " 00:00:00";
            }
            if($request->date_end != null){
                $dateEnd = $request->date_end . " 23:59:59";
            }

            $dataPost = [
                "limit" => $this->default_limit,
                "skip" => $this->default_current_page,
                "data_search" => (object) [
                    "agency_name" => $agencyName,
                    "date_start" => $dateStart,
                    "date_end" => $dateEnd
                ]
            ];

            $dataResponse = Revenue::getData("POST", $dataPost);
            $this->search_data = [
                "agency_name" => $request->agency_name,
                "date_start" => $request->date_start,
                "date_end" => $request->date_end
            ];
            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                    $this->data["total"] = $dataResponse->total;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("revenue.revenue_manage")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu)
                ->with("search_data", $this->search_data);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function pagingRevenueAjax(Request $request){
        if(session()->has("profile")){
            if($this->is_searched){
                $skip = ((int) $request->page != 0) ? $request->page : $this->default_current_page;
                $agencyName = null;
                $dateStart = null;
                $dateEnd = null;
    
                if($request->agency_name != null){
                    $agencyName = $request->agency_name;
                }
                if($request->date_start != null){
                    $dateStart = $request->date_start . " 00:00:00";
                }
                if($request->date_end != null){
                    $dateEnd = $request->date_end . " 23:59:59";
                }
    
                $dataPost = [
                    "limit" => $this->default_limit,
                    "skip" => $skip,
                    "data_search" => (object) [
                        "agency_name" => $agencyName,
                        "date_start" => $dateStart,
                        "date_end" => $dateEnd
                    ]
                ];
    
                $dataResponse = Revenue::getData("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_found_data"]){
                        $this->data = $dataResponse->data;
                        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], false, $this->drawDataSearch($dataResponse->data, $skip));
                    }
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    private function drawDataSearch($data, $page_num) {
        if(!empty($data)){
            $str = "";
            $no = Utilities::numberIncrease($page_num, $this->default_limit);
            
            if(isset($data->listPaid)){
                $str .= "<tbody>";
                foreach($data->listPaid as $key => $value){
                    $str .= "<tr>";
                    $str .= "<td>" . ($no + $key + 1) . "</td>";
                    $str .= "<td>" . $value->transaction  . "</td>";
                    $str .= "<td>" . $value->agency_name  . "</td>";
                    $str .= "<td>" . $value->agency_package_name . "</td>";
                    $str .= "<td>" . Utilities::convertVND($value->price_original_paid, 0, "VNĐ") . "</td>";
                    $str .= "<td>" . $value->price_cycle . "</td>";
                    $str .= "<td>" . $value->amount_code_created . "</td>";
                    $str .= "<td>" . Utilities::convertVND($value->price_paid, 0, "VNĐ") . "</td>";
                    $str .= "<td>" . Utilities::convertVND($value->price_paid_difference, 0, "VNĐ") . "</td>";
                    $str .= "<td>" . $value->day_create_string . "</td>";
                    $str .= "<td>" . $value->user_create->full_name . "</td>";
                    $str .= "<td class='center'><div class='action-buttons'><a href='#' class='green bigger-140 show-details-btn' title='Show Details'>";
                    $str .= "<i class='ace-icon fa fa-angle-double-up'></i><span class='sr-only'>Details</span></a></div></td>";
                    $str .= "</tr>";
                    if(!empty($value->gift_code)){
                        $str .= "<tr class='detail-row'>";
                        $str .= "<td colspan='12'>";
                        $str .= "<table class='table table-bordered table-hover'>";
                        $str .= "<tr><th>#</th><th>Mã code</th><th>Serial</th><th>Thời gian tạo</th><th>Thời gian hết hạn</th></tr>";
                        foreach($value->gift_code as $i => $v){
                            $str .= "<tr>";
                            $str .= "<td>". (++$i) . "</td>";
                            $str .= "<td>". $v->pin_code . "</td>";
                            $str .= "<td>". $v->serial . "</td>";
                            $str .= "<td>" . $v->created_date . "</td>";
                            $str .= "<td>" . $v->expired_date . "</td>";
                            // $str .= "<td>" . (($v->active_status)?"Đã kích hoạt":"Chưa kích hoạt") ."</td>";
                            $str .= "</tr>";
                        }
                        $str .= "</table>";
                        $str .= "</td>";
                        $str .= "</tr>";
                        
                    }
                }
                $str .= "</tbody>";
                $str .= "<tbody>";
                if(isset($data->sumPaid)){
                    foreach($data->sumPaid as $index => $item){
                        $str .= "<tr>";
                        $str .= "<td class='center' colspan='6'><b>Tổng doanh thu</b></td>";
                        $str .= "<td><b>" . $item->totalAmount . "</b></td>";
                        $str .= "<td><b>" . Utilities::convertVND($item->totalPaid, 0, "VNĐ") . "</b></td>";
                        $str .= "<td><b>" . Utilities::convertVND($item->totalDifference, 0, "VNĐ") . "</b></td>";
                        $str .= "<td colspan='3'></td>";
                        $str .= "</tr>";
                    }
                }
                $str .= "</tbody>";
                $str .= "</tbody>";
            }
            return $str;
        }
    }

    public function exportData (Request $request){
        if($this->is_exported){
            $agencyName = null;
            $dateStart = null;
            $dateEnd = null;
            $data = array();
            $dataRevenue = array();
            $dataCode = array();
            $dataTemp = array();
            if($request->agency_name != null){
                $agencyName = $request->agency_name;
            }
            if($request->date_start != null){
                $dateStart = $request->date_start . " 00:00:00";
            }
            if($request->date_end != null){
                $dateEnd = $request->date_end . " 23:59:59";
            }

            $dataPost = [
                "data_search" => (object) [
                    "agency_name" => $agencyName,
                    "date_start" => $dateStart,
                    "date_end" => $dateEnd
                ]
            ];

            $dataResponse = Revenue::exportData("POST", $dataPost);
            $totalCode = 0;
            $totalPrice = 0;
            $totalDifference = 0;
            if(isset($dataResponse)){
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    foreach($dataResponse->data as $key=>$value){
                        $dataRevenue[$key]['Mã giao dịch'] = $value->transaction;
                        $dataRevenue[$key]['Mã đại lý'] = $value->agency_name;
                        $dataRevenue[$key]['Tên gói'] = $value->agency_package_name;
                        $dataRevenue[$key]['Tiền gốc'] = Utilities::convertVND($value->price_original_paid, 0, "VNĐ");
                        $dataRevenue[$key]['Chiết khấu'] = $value->price_cycle;
                        $dataRevenue[$key]['Số code tạo'] = $value->amount_code_created;
                        $dataRevenue[$key]['Tiền trả'] = Utilities::convertVND($value->price_paid, 0, "VNĐ");
                        $dataRevenue[$key]['Tiền chênh lệch'] = Utilities::convertVND($value->price_paid_difference, 0, "VNĐ");
                        $dataRevenue[$key]['Ngày tạo'] = $value->day_create_string;
                        $dataRevenue[$key]['Người tạo'] = $value->user_create->full_name;
                        $totalCode = $totalCode + $value->amount_code_created;
                        $totalPrice = $totalPrice + $value->price_paid;
                        $totalDifference = $totalDifference + $value->price_paid_difference;
                        if(!empty($value->gift_code)){
                            foreach($value->gift_code as $k => $v){
                                $v->transaction = $value->transaction;
                                array_push($dataTemp, $v);
                            }
                        }
                    }
                }
                $dataRevenue[count($dataResponse->data)]['Mã giao dịch'] = 0;
                $dataRevenue[count($dataResponse->data)]['Mã đại lý'] = 0;
                $dataRevenue[count($dataResponse->data)]['Tên gói'] = 0;
                $dataRevenue[count($dataResponse->data)]['Tiền gốc'] = 0;
                $dataRevenue[count($dataResponse->data)]['Chiết khấu'] = "TỔNG DOANH THU";
                $dataRevenue[count($dataResponse->data)]['Số code tạo'] = $totalCode;
                $dataRevenue[count($dataResponse->data)]['Tiền trả'] =  Utilities::convertVND($totalPrice, 0, "VNĐ");
                $dataRevenue[count($dataResponse->data)]['Tiền chênh lệch'] =  Utilities::convertVND($totalDifference, 0, "VNĐ");
                $dataRevenue[count($dataResponse->data)]['Ngày tạo'] = 0;
                $dataRevenue[count($dataResponse->data)]['Người tạo'] = 0;
                $data["revenue"] = $dataRevenue;
                if(!empty($dataTemp)){
                    foreach($dataTemp as $k => $v){
                        $dataCode[$k]['Mã giao dịch'] = $v->transaction;
                        $dataCode[$k]['Mã PIN'] = $v->pin_code;
                        $dataCode[$k]['Serial'] = $v->serial;
                        $dataCode[$k]['Thời gian tạo'] = $v->created_date;
                        $dataCode[$k]['Thời gian hết hạn'] = $v->expired_date;
                        // $dataCode[$k]['Trạng thái'] = ($v->active_status)?"Đã kích hoạt":"Chưa kích hoạt";
                    }
                    
                }
                $data["code"] = $dataCode;
            }
            Excel::create("Dai Ly", function($excel) use ($data) {
                $excel->sheet('Doanh thu', function($sheet) use ($data)
                {
                    $sheet->fromArray($data["revenue"]);
                });
                $excel->sheet('Danh sách code', function($sheet) use ($data)
                {
                    $sheet->fromArray($data["code"]);
                });
            })->export('xls');
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }
}