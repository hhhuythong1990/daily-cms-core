<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Model\AgencyPackage;
use App\Model\Agency;
use App\Helpers\Utilities;
use App\Helpers\Helper;

class AgencyPackageController extends Controller {
    private $code_response;
    private $default_limit;
    private $default_current_page;
    private $menu = null;
    private $permissions = null;
    private $user_id = null;
    private $data = null;
    private $is_viewed = false;
    private $is_created = false;
    private $is_edited = false;
    private $is_deleted = false;
    private $search_data = null;

    public function __construct() {
        $this->title_active = "Gói bán";
        $this->default_current_page = Utilities::pagination()["DEFAULT_CURRENT_PAGE"];
        $this->default_limit = Utilities::pagination()["DEFAULT_PAGE_1000"];        
        $this->code_response = Utilities::apiCodeResponse();
        if(session()->has("profile")){
            $this->menu = session()->get("profile")->group_modules;
            $this->permissions = session()->get("profile")->permissions;
            $this->user_id = session()->get("profile")->_id;
            $this->is_viewed = in_array(Utilities::constantPermissions()["AGENCY_PACKAGE_VIEW"], $this->permissions);
            $this->is_created = in_array(Utilities::constantPermissions()["AGENCY_PACKAGE_CREATE"], $this->permissions);
            $this->is_edited = in_array(Utilities::constantPermissions()["AGENCY_PACKAGE_EDIT"], $this->permissions);
            $this->is_deleted = in_array(Utilities::constantPermissions()["AGENCY_PACKAGE_DELETE"], $this->permissions);
            $this->is_get_code = in_array(Utilities::constantPermissions()["PACKAGE_GET_CODE"], $this->permissions);
        }
    }

    public function index(Request $request) {        
        if($this->is_viewed){
            $dataResponse = AgencyPackage::takeAgencyPackageBySkipLimit($this->default_current_page, $this->default_limit);
            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                    $this->data["total"] = $dataResponse->total;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            
            return view("agency_package.index")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu)
                ->with("search_data", $this->search_data);
        }
        else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function create(Request $request) {
        if($this->is_created){
            $dataResponse = AgencyPackage::takeAgencyPackageData();
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data = $dataResponse->data;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("agency_package.create")
                ->with("title", $this->title_active)
                ->with("packages", $this->data->getPackage)
                ->with("agencies", $this->data->getAgency)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }     
    }

    public function createAgencyPackageAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_created){
                
                $this->validate($request, [
                    "agency" => "required",
                    "package" => "required",
                    "cycle" => "required"
                ],[
                    "agency.required" => "Mã đại lý không được để trống",
                    "package.required" => "Mã gói không được để trống",
                    "cycle.required" => "Chiết khấu không được để trống"
                ]);
                $agencyData = explode("-", $request->agency);
                $packageData = explode("-", $request->package);
                $dataPackages = array();
                array_push($dataPackages, (object)array(
                    "agency_package_name" => $agencyData[1] . "-" . $packageData[1],
                    "package" => $packageData[0],
                    "cycle" => $request->cycle,
                    "agency_package_status" => isset($request->agency_package_status)?true:false
                ));
                $totalPackage = $request->total_package;
                if($totalPackage > 0){                    
                    $allParameter = $request->all();
                    for($i = 1; $i <= $totalPackage; $i++){
                        if(!empty($allParameter["package".$i]) && !empty($allParameter["cycle".$i])){
                            $packageData = explode("-", $allParameter["package".$i]);
                            array_push($dataPackages, (object)array(
                                "agency_package_name" => $agencyData[1] . "-" . $packageData[1],
                                "package" => $packageData[0],
                                "cycle" => $allParameter["cycle".$i],
                                "agency_package_status" => isset($allParameter["agency_package_status".$i])?true:false
                            ));
                        }
                    }
                }
                
                $dataPost = [
                    "agency" => $agencyData[0],
                    "packages" => $dataPackages,
                    "user_create" => $this->user_id
                ];
                $dataResponse = AgencyPackage::newAgencyPackage("POST", $dataPost);
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_add_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
                return redirect(route("tao_moi_goi_va_dai_ly_page_path"));
            }
        }
    }

    public function edit($id, Request $request) {
        if($this->is_edited){
            $dataResponse = AgencyPackage::takeAgencyPackageById($id);  
            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["agencyPackage"] = $dataResponse->data;
                    $dataEdit = AgencyPackage::takeAgencyPackageData();
                    if(isset($dataEdit)) {
                        if($dataEdit->code == $this->code_response["code_found_data"]){
                            $this->data["edit"] = $dataEdit->data;
                        }else {
                            $request->session()->flash(Utilities::status_alert()["status_error"] , $dataEdit->msg);
                        }
                    }else{
                        $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                    }
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            
            return view("agency_package.edit")
                ->with("title", $this->title_active)
                ->with("data", $this->data["agencyPackage"])
                ->with("packages", $this->data["edit"]->getPackage)
                ->with("agencies", $this->data["edit"]->getAgency)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        } 
    }

    public function editAgencyPackageAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_edited){
                $this->validate($request, [
                    "id" => "required",
                    "agency" => "required",
                    "package" => "required",
                    "cycle" => "required",
                    "agency_package_status" => "required"
                ],[
                    "id.required" => "Mã đình danh đại lý và gói không được để trống",
                    "agency.required" => "Mã đại lý không được để trống",
                    "package.required" => "Mã gói không được để trống",
                    "cycle.required" => "Chiết khấu không được để trống",
                    "agency_package_status.required" => "Trạng thái phải được chọn"
                ]);

                $agencyData = explode("-", $request->agency);
                $packageData = explode("-", $request->package);

                $dataPost = [
                    "id" => $request->id,
                    "agency_package_name" => $agencyData[1] . "-" . $packageData[1],
                    "agency" => $agencyData[0],
                    "package" => $packageData[0],
                    "cycle" => $request->cycle,
                    "agency_package_status" => $request->agency_package_status,
                    "user_update" => $this->user_id
                ];        
                $dataResponse = AgencyPackage::updateAgencyPackage("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_update_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }
    

    public function takeAgencyPackageSearchData(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){
                $agencyPackageName = $request->agency_package_name;                
                if($agencyPackageName != null){
                    $skip = $this->default_current_page;
                    $dataResponse = AgencyPackage::takeByDataSearchSkipLimit($skip, $this->default_limit, urlencode($agencyPackageName));                    
                    if(isset($dataResponse)) {
                        if($dataResponse->code == $this->code_response["code_found_data"]){
                            $this->data["data"] = $dataResponse->data;
                            $this->data["total"] = $dataResponse->total;
                            $this->search_data = $agencyPackageName;
                        }else{
                            $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                        }
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                    }
                    
                    return view("agency_package.index")
                    ->with("title", $this->title_active)
                    ->with("data", $this->data["data"])
                    ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                    ->with("list_permission", $this->permissions)
                    ->with("list_menu", $this->menu)
                    ->with("search_data", $this->search_data);
                }else{
                    return redirect(route("danh_sach_goi_ban_page_path"));
                }
            }else {
                return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
            }
        }
    }

    public function takeAgencyPackagePageAjax(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){      
                $skip = ((int) $request->page != 0) ? $request->page : $this->default_current_page;        
                $dataSearch = $request->data_search;

                if($dataSearch == null){
                    $dataResponse = AgencyPackage::takeAgencyPackageBySkipLimit($skip, $this->default_limit);
                }else {
                    $userName = $request->data_search;
                    $dataResponse = AgencyPackage::takeByDataSearchSkipLimit($skip, $this->default_limit, urlencode($userName));
                }
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_found_data"]){
                        $this->data = $dataResponse->data;
                        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], false, $this->drawAgencyPackage($this->data, $skip));
                    }
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    private function drawAgencyPackage($data, $page_num) {
        if(!empty($data)){
            $str = "";
            $no = Utilities::numberIncrease($page_num, $this->default_limit);;
            foreach($data as $key => $value){
                $str .= "<tr>";
                $str .= "<td>" . ($no + $key + 1) . "</td>";
                $str .= "<td>" . $value->agency_package_name  . "</td>";
                $str .= "<td>" . $value->agency->full_name  . "</td>";
                $str .= "<td>" . $value->package->package_name  . "</td>";
                $str .= "<td>" . $value->cycle  . "%</td>";
                $str .= "<td>" . (($value->agency_package_status)?"Kích hoạt":"Vô hiệu hóa") . "</td>";
                $str .= "<td>" . $value->day_create_string . "</td>";
                $str .= "<td>" . $value->day_update_string . "</td>";
                $str .= "<td>" . $value->user_create->full_name . "</td>";
                $str .= "<td>" . ((isset($value->user_update)) ? $value->user_update->full_name : "" ). "</td>";
                $str .= "<td>";
                if($this->is_edited){
                    $str .= "<a href='". route("cap_nhat_dai_ly_va_goi_page_path", ["id" => $value->_id]) ."' class='btn btn-white btn-warning btn-xs'>
                            <i class='ace-icon fa fa-wrench  bigger-110 icon-only'></i>
                        </a>&nbsp;";
                }
                if($this->is_deleted){
                    $str .= "<button data-toggle='modal' data-target='#myModal' onclick='storeId(\"" . $value->_id ."\")' class='btn btn-white btn-danger btn-xs'>
                            <i class='ace-icon fa fa-trash-o  bigger-110 icon-only'></i>
                        </button>";
                }                
                $str .= "</td>";
                $str .= "</tr>";
            }
            return $str;
        }
    }

    public function deleteAgencyPackageAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_deleted){
                $this->validate($request, [
                    "item_id" => "required"
                ],[
                    "item_id.required" => "Mã đình danh đại lý và gói không được để trống"
                ]);
                $dataResponse = AgencyPackage::removeAgencyPackage($request->item_id);
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_delete_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function getCode(Request $request) {        
        if($this->is_get_code){
            $dataResponse = AgencyPackage::getCode($this->default_current_page, $this->default_limit); 
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                    $this->data["total"] = $dataResponse->total;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("agency_package.code_manage")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu)
                ->with("search_data", $this->search_data);
        }
        else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function takeCodePageAjax(Request $request) {
        if(session()->has("profile")){  
            if($this->is_get_code){      
                $skip = ((int) $request->page != 0) ? $request->page : $this->default_current_page;        
                $dataSearch = $request->data_search;

                if($dataSearch == null){
                    $dataResponse = AgencyPackage::getCode($skip, $this->default_limit); 
                }else {
                    $userName = $request->data_search;
                    $dataResponse = AgencyPackage::takeCodeByDataSearchSkipLimit($skip, $this->default_limit, urlencode($userName));
                }
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_found_data"]){
                        $this->data = $dataResponse->data;
                        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], false, $this->drawCode($this->data, $skip));
                    }
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    private function drawCode($data, $page_num) {
        if(!empty($data)){
            $str = "";
            $no = Utilities::numberIncrease($page_num, $this->default_limit);;
            foreach($data as $key => $value){
                $str .= "<tr>";
                $str .= "<td>" . ($no + $key + 1) . "</td>";
                $str .= "<td>" . $value->pin_code  . "</td>";
                $str .= "<td>" . $value->serial  . "</td>";
                $str .= "<td>" . $value->created_date  . "</td>";
                $str .= "<td>" . $value->expired_date  . "</td>";
                //$str .= "<td>" . (($value->active_status)?"Đã kích hoạt":"Chưa kích hoạt")  . "</td>";
                $str .= "</tr>";
            }
            return $str;
        }
    }

    public function takeCodeSearchData(Request $request) {
        if(session()->has("profile")){  
            if($this->is_get_code){
                $codeId = $request->code_id;                
                if($codeId != null){
                    $skip = $this->default_current_page;
                    $dataResponse = AgencyPackage::takeCodeByDataSearchSkipLimit($skip, $this->default_limit, urlencode($codeId));
                    if(isset($dataResponse)) {
                        if($dataResponse->code == $this->code_response["code_found_data"]){
                            $this->data["data"] = $dataResponse->data;
                            $this->data["total"] = $dataResponse->total;
                            $this->search_data = $codeId;
                        }else{
                            $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                        }
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                    }
                    
                    return view("agency_package.code_manage")
                    ->with("title", $this->title_active)
                    ->with("data", $this->data["data"])
                    ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                    ->with("list_permission", $this->permissions)
                    ->with("list_menu", $this->menu)
                    ->with("search_data", $this->search_data);
                }else{
                    return redirect(route("danh_sach_code_page_path"));
                }
            }else {
                return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
            }
        }
    }
}