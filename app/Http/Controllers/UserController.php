<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Helpers\Utilities;
use App\Helpers\Helper;
use Illuminate\Contracts\Cookie\Factory;

class UserController extends Controller {
    private $code_response;
    private $default_limit;
    private $default_current_page;
    private $menu = null;
    private $permissions = null;
    private $user_id = null;
    private $data = null;
    private $is_viewed = false;
    private $is_created = false;
    private $is_edited = false;
    private $is_deleted = false;
    private $search_data = null;
    
    public function __construct() {
        $this->title_active = "Mô-đun";
        $this->default_current_page = Utilities::pagination()["DEFAULT_CURRENT_PAGE"];
        $this->default_limit = Utilities::pagination()["DEFAULT_PAGE_10"];        
        $this->code_response = Utilities::apiCodeResponse();
        if(session()->has("profile")){
            $this->menu = session()->get("profile")->group_modules;
            $this->permissions = session()->get("profile")->permissions;
            $this->user_id = session()->get("profile")->_id;
            $this->is_viewed = in_array(Utilities::constantPermissions()["USER_VIEW"], $this->permissions);
            $this->is_created = in_array(Utilities::constantPermissions()["USER_CREATE"], $this->permissions);
            $this->is_edited = in_array(Utilities::constantPermissions()["USER_EDIT"], $this->permissions);
            $this->is_deleted = in_array(Utilities::constantPermissions()["USER_DELETE"], $this->permissions);
            $this->is_offline = in_array(Utilities::constantPermissions()["USER_OFFLINE"], $this->permissions);
            $this->is_locked = in_array(Utilities::constantPermissions()["USER_BLOCK"], $this->permissions);
        }
    }

    public function index(Request $request) {
        if($this->is_viewed){
            $dataResponse = User::takeUserBySkipLimit($this->default_current_page, $this->default_limit);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                    $this->data["total"] = $dataResponse->total;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("user.index")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu)
                ->with("search_data", $this->search_data);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function create(Request $request) {
        if($this->is_created){
            $dataResponse = User::getDataForCreateUser();
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data = $dataResponse->data;
                    
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("user.create")
                ->with("title", $this->title_active)
                ->with("groupModules", $this->data->groupModules)
                ->with("groupUsers", $this->data->groupUsers)
                ->with("permissions", $this->data->permissions)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function createUserAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_created){
                $this->validate($request, [
                    "full_name" => "required",
                    "user_name" => "required",
                    "password" => "required",
                    "group_modules" => "required",
                    "group_user" => "required",
                    "permissions" => "required",
                    "publish" => "required"
                ],[
                    "full_name.required" => "Họ và tên không được để trống",
                    "user_name.required" => "Tên đăng nhập không được để trống",
                    "password.required" => "Mật khẩu không được để trống",
                    "group_modules.required" => "Danh sách nhóm mô-đun không được để trống",
                    "group_user.required" => "Nhóm người dùng phải được chọn",
                    "permissions.required" => "Quyền hạn phải được chọn",
                    "publish.required" => "Công bố phải được chọn"
                ]);
                
                $current_group = session()->get("profile")->group_user;
                $group_user =  $request->group_user;
                $group_parent = null;
                if($current_group != $group_user){
                    $group_parent = [ $current_group ];
                }

                $dataPost = [
                    "full_name" => $request->full_name,
                    "user_name" => $request->user_name,
                    "password" => $request->password,
                    "group_modules" => $request->group_modules,
                    "group_user" => $group_user,
                    "group_parent" => $group_parent,
                    "permissions" => $request->permissions,
                    "publish" => $request->publish,
                    "user_create" => $this->user_id
                ];        
                $dataResponse = User::createUser("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_add_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function edit($id, Request $request) {
        if($this->is_edited){
            $dataResponse = User::takeUserByUserId($id);  
            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data = $dataResponse->data;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            
            return view("user.edit")
                ->with("title", $this->title_active)
                ->with("data", $this->data)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        } 
    }

    public function editProfileAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_edited){            
                $this->validate($request, [
                    "profile_id" => "required",
                    "full_name" => "required",
                    "user_name" => "required",
                    "group_user" => "required",
                    "group_modules" => "required",
                    "permissions" => "required",
                    "publish" => "required"
                ],[
                    "profile_id.required" => "Mã định danh người dùng không được để trống",
                    "full_name.required" => "Tên đầy đủ không được để trống",
                    "user_name.required" => "Tên đăng nhập không được để trống",
                    "group_user.required" => "Nhóm người dùng không được để trống",
                    "group_modules.required" => "Danh sách mô-đun không được để trống",
                    "permissions.required" => "Danh sách quyền hạn không được để trống",
                    "publish.required" => "Công bố phải được chọn",
                ]);
                $group_module_name = $request->group_module_name;
                $dataPost = [
                    "profile_id" => trim($request->profile_id),
                    "full_name" => trim($request->full_name),
                    "user_name" => trim($request->user_name),
                    "password" => trim($request->password),
                    "permissions" => $request->permissions,
                    "group_user" => $request->group_user,
                    "group_parent" => $request->group_parent,
                    "group_modules" => $request->group_modules,
                    "publish" => $request->publish,
                    "user_update" => $this->user_id
                ];

                $dataResponse = User::updateProfile("POST", $dataPost);
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_update_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function checkUserNameExist(Request $request){
        if($this->is_created){
            $dataPost = [
                "user_name" => $request->user_name
            ];
            $dataResponse = User::checkUserNameExist("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    public function checkProfileExist($userId, Request $request){
        if($this->is_edited){
            $dataPost = [
                "user_id" => $userId,
                "user_name" => $request->user_name
            ];
            $dataResponse = User::takeProfileByIdAndUserName("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    public function deleteProfileAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_deleted){
                $this->validate($request, [
                    "item_id" => "required"
                ],[
                    "item_id.required" => "Mã hồ sơ không được để trống"
                ]);                
                $dataPost = [
                    "profile_id" => $request->item_id
                ];
                $dataResponse = User::deleteProfile("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_delete_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function takeProfileSearchData(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){
                $userName = $request->user_name;                
                if($userName != null){
                    $skip = $this->default_current_page;
                    $dataResponse = User::takeByDataSearchSkipLimit($skip, $this->default_limit, urlencode($userName));                    
                    if(isset($dataResponse)) {
                        if($dataResponse->code == $this->code_response["code_found_data"]){
                            $this->data["data"] = $dataResponse->data;
                            $this->data["total"] = $dataResponse->total;
                            $this->search_data = $userName;
                        }else{
                            $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                        }
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                    }
                    
                    return view("user.index")
                    ->with("title", $this->title_active)
                    ->with("data", $this->data["data"])
                    ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                    ->with("list_permission", $this->permissions)
                    ->with("list_menu", $this->menu)
                    ->with("search_data", $this->search_data);
                }else{
                    return redirect(route("nguoi_dung_page_path"));
                }
            }else {
                return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
            }
        }
    }

    public function userInfo(){
        if(session()->has("profile")){ 
            $userData = session()->get("profile");
            return view("layouts.user_info")->with("data", $userData)
                ->with("title", "Thông tin tài khoản")
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu);
        }else {
            return redirect(route('dang_nhap_path'));
        }
    }

    public function changePasswordAjax(Request $request, Factory $cookie){
        if(session()->has("profile")){  
            $this->validate($request, [
                "user_id" => "required",
                "old_password" => "required",
                "new_password" => "required"
            ],[
                "user_id.required" => "Mã hồ sơ không được để trống",
                "old_password.required" => "Mã hồ sơ không được để trống",
                "new_password.required" => "Mã hồ sơ không được để trống"
            ]);                
            $dataPost = [
                "user_id" => $request->user_id,
                "old_password" => $request->old_password,
                "new_password" => $request->new_password
            ];
            $dataResponse = User::changePassword("POST", $dataPost);
            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_update_successfully"]){
                    $dataPost = array(
                        "profile_id" => session()->get('profile')->_id
                    );
                    $dataResponse = User::logoutUser("POST", $dataPost);
                    if(isset($dataResponse)){
                        session()->forget('profile');
                        $cookie->queue($cookie->forget('profileId'));
                    }
                    $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            } else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function offlineAgencyAjax(Request $request){
        if(session()->has("profile")){
            if($this->is_offline){
                $this->validate($request, [
                    "profile_id" => "required",
                    "status" => "required",
                ],[
                    "profile_id.required" => "Mã hồ sơ không được để trống",
                    "status.required" => "Trạng thái không được để trống"
                ]);                
                $dataPost = [
                    "profile_id" => $request->profile_id,
                    "is_online" => $request->status
                ];
                $dataResponse = User::changeOnlineStatus("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_update_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function blockAgencyAjax(Request $request){
        if(session()->has("profile")){
            if($this->is_locked){
                $this->validate($request, [
                    "profile_id" => "required",
                    "status" => "required|boolean",
                ],[
                    "profile_id.required" => "Mã hồ sơ không được để trống",
                    "status.required" => "Trạng thái không được để trống",
                    "status.boolean" => "Trạng thái không đúng định dạng"
                ]);                
                $dataPost = [
                    "profile_id" => $request->profile_id,
                    "is_locked" => ($request->status == 1)?"true":"false"
                ];
                $dataResponse = User::changeBlockStatus("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_update_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function takeProfilePageAjax(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){      
                $skip = ((int) $request->page != 0) ? $request->page : $this->default_current_page;        
                $dataSearch = $request->data_search;

                if($dataSearch == null){
                    $dataResponse = User::takeUserBySkipLimit($skip, $this->default_limit);
                }else {
                    $userName = $request->data_search;
                    $dataResponse = User::takeByDataSearchSkipLimit($skip, $this->default_limit, urlencode($userName));
                }
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_found_data"]){
                        $this->data = $dataResponse->data;
                        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], false, $this->drawProfile($this->data, $skip));
                    }
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    private function drawProfile($data, $page_num) {
        if(!empty($data)){
            $str = "";
            $no = Utilities::numberIncrease($page_num, $this->default_limit);;
            foreach($data as $key => $value){
                $str .= "<tr>";
                $str .= "<td>" . ($no + $key + 1) . "</td>";
                $str .= "<td>" . $value->full_name  . "</td>";
                $str .= "<td>" . $value->user_name  . "</td>";
                $str .= "<td>" . (($value->is_publish)?"Công khai":"Không công khai") . "</td>";
                $str .= "<td>";
                if(isset($value->group_modules)){
                    $str .= "<ul>";
                        foreach($value->group_modules as $k => $v){
                            $str .= "<li>".$v->group_name ."</li>";
                        }
                    $str .= "</ul>";
                }
                $str .= "</td>";
                $str .= "<td>";
                if(isset($value->group_parent)){
                    $str .= "<ul>";
                        foreach($value->group_parent as $k => $v){
                            $str .= "<li>".$v->group_user_name ."</li>";
                        }
                    $str .= "</ul>";
                }
                $str .= "</td>";
                $str .= "<td>";
                if(isset($value->permissions)){
                    $str .= "<ul>";
                        foreach($value->permissions as $k => $v){
                            $str .= "<li>".$v->permission_name ."</li>";
                        }
                    $str .= "</ul>";
                }
                $str .= "</td>";
                $str .= "<td>" . ((!$value->is_locked)?"":"Bị khóa") . "</td>";
                $str .= "<td>" . $value->day_create_string . "</td>";
                $str .= "<td>" . $value->day_update_string . "</td>";
                $str .= "<td>" . $value->user_create->full_name . "</td>";
                $str .= "<td>" . ((isset($value->user_update)) ? $value->user_update->full_name : "" ). "</td>";
                $str .= "<td>";
                if($this->is_edited){
                    $str .= "<a href='". route("cap_nhat_nguoi_dung_page_path", ["id" => $value->_id]) ."' class='btn btn-white btn-warning btn-xs'>
                            <i class='ace-icon fa fa-wrench  bigger-110 icon-only'></i>
                        </a>&nbsp;";
                }
                if($this->is_deleted){
                    $str .= "<button data-toggle='modal' data-target='#myModal' onclick='storeId(\"" . $value->_id ."\")' class='btn btn-white btn-danger btn-xs'>
                            <i class='ace-icon fa fa-trash-o  bigger-110 icon-only'></i>
                        </button>";
                }                
                $str .= "</td>";
                $str .= "</tr>";
            }
            return $str;
        }
    }
}