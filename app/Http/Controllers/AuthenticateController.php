<?php
namespace App\Http\Controllers;

use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Contracts\Cookie\Factory;
use Crypt;
use Illuminate\Support\Facades\Config;
use App\Helpers\Utilities;

class AuthenticateController extends Controller
{
    private $code_response;
    private $data;

    public function __construct()
    {
        $this->code_response = Utilities::apiCodeResponse();
    }

    public function login(Request $request, Factory $cookie){
        if(!session()->has("profile")){
            $this->validate($request,[
                'username' => 'required',
                'password' => 'required|min:8|max:255',
            ],[
                'username.required' => 'Tên đăng nhập không được để trống',
                'password.required' => 'Mật khẩu đăng nhập không được để trống',
                'password.min'=> 'Mật khẩu đăng nhập phải lớn hơn :min kí tự'
            ]);
    
            $dataPost = array(
                "user_name" => $request->username,
                "password" => $request->password
            );
    
            $dataResponse = User::authenticateUser("POST", $dataPost);
            
            if(isset($dataResponse)){
                if($dataResponse->code == $this->code_response["login_successfully"]){
                    session()->put("profile", $dataResponse->data);
                    $redirect = $dataResponse->data->group_modules[0]->modules[0]->route_path;
                    $encryptData = Crypt::encrypt($dataResponse->data->_id);
                    $cookie->queue($cookie->forever('profileId', $encryptData));
                    return redirect(route($redirect));                
                }
                else if($dataResponse->code == $this->code_response["custom_message"]){
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);                
                } else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , "Tên đăng nhập hoặc mật khẩu không đúng");                
                }
            }else{ 
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);            
            }
        }       
        return redirect("dang-nhap");
    }

    public function logout(Request $request, Factory $cookie){
        if(session()->has("profile")){
            $dataPost = array(
                "profile_id" => session()->get('profile')->_id
            );
        }else {
            $cookieProfile = $request->cookie('profileId');
            $decryptCookieProfile = Crypt::decrypt($cookieProfile);
            $dataPost = [
                "profile_id" => $decryptCookieProfile
            ];
        }
        $dataResponse = User::logoutUser("POST", $dataPost);
        if(isset($dataResponse)){
            session()->forget('profile');
            $cookie->queue($cookie->forget('profileId'));
            return redirect('dang-nhap');
        }
    }


}