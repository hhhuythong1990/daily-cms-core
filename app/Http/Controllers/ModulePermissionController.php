<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ModulePermission;
use App\Model\Permission;
use App\Helpers\Utilities;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Config;

class ModulePermissionController extends Controller {
    private $code_response;
    private $default_limit;
    private $default_current_page;
    private $menu = null;
    private $permissions = null;
    private $user_id = null;
    private $data = null;
    private $is_viewed = false;
    private $is_created = false;
    private $is_edited = false;
    private $is_deleted = false;
    private $search_data = null;
    
    public function __construct() {
        $this->title_active = "Mô-đun và quyền hạn";
        $this->default_current_page = Utilities::pagination()["DEFAULT_CURRENT_PAGE"];
        $this->default_limit = Utilities::pagination()["DEFAULT_PAGE_10"];        
        $this->code_response = Utilities::apiCodeResponse();         
        if(session()->has("profile")){
            $this->menu = session()->get("profile")->group_modules;
            $this->permissions = session()->get("profile")->permissions;
            $this->user_id = session()->get("profile")->_id;
            $this->is_viewed = in_array(Utilities::constantPermissions()["MODULE_PERMISSION_VIEW"], $this->permissions);
            $this->is_created = in_array(Utilities::constantPermissions()["MODULE_PERMISSION_CREATE"], $this->permissions);
            $this->is_edited = in_array(Utilities::constantPermissions()["MODULE_PERMISSION_EDIT"], $this->permissions);
            $this->is_deleted = in_array(Utilities::constantPermissions()["MODULE_PERMISSION_DELETE"], $this->permissions);
        }
    }

    public function index(Request $request) {
        if($this->is_viewed){
            $dataResponse = ModulePermission::takeModulePermissionBySkipLimit($this->default_current_page, $this->default_limit);            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                    $this->data["total"] = $dataResponse->total;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("module_permission.index")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu)
                ->with("search_data", $this->search_data);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }        
    }

    public function create(Request $request) {
        if($this->is_created){
            $dataResponse = ModulePermission::getDataForCreate();            

            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data = $dataResponse->data;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            
            return view("module_permission.create")
                ->with("title", $this->title_active)
                ->with("modules", $this->data->modules)
                ->with("permissions", $this->data->permissions)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function createModulePermission(Request $request) {
        if(session()->has("profile")){
            if($this->is_created){
                $this->validate($request, [
                    "module" => "required",
                    "permissions" => "required",
                    "publish" => "required",
                    "module_permission" => "required"
                ],[
                    "module.required" => "Mô-đun không được để trống",
                    "permissions.required" => "Quyền hạn không được để trống",
                    "publish.required" => "Công bố phải được chọn",
                    "module_permission.required" => "Tên mô-đun và quyền hạn không được để trống"
                ]);
                
                $dataPost = [
                    "module" => $request->module,
                    "permissions" => $request->permissions,
                    "publish" => $request->publish,
                    "module_permission" => $request->module_permission,
                    "user_create" => $this->user_id
                ];        
                $dataResponse = ModulePermission::createModulePermission("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_add_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
                return redirect(route("tao_moi_mo_dun_va_quyen_han_page_path"));
            }else{
                return view("errors.permission_denied")
                    ->with("list_menu", $this->menu);
            }
        }
    }

    public function takeModulePermissionPageAjax(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){      
                $skip = ((int) $request->page != 0) ? $request->page : $this->default_current_page;
                $dataResponse = ModulePermission::takeModulePermissionBySkipLimit($skip, $this->default_limit);
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_found_data"]){
                        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], false, $this->drawModulePermission($dataResponse->data, $skip));
                    }
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    private function drawModulePermission($data, $page_num) {
        if(!empty($data)){
            $str = "";
            $no = Utilities::numberIncrease($page_num, $this->default_limit);;
            foreach($data as $key => $value){
                $str .= "<tr>";
                $str .= "<td>" . ($no + $key + 1) . "</td>";
                $str .= "<td>" . $value->module_permission_name  . "</td>";
                $str .= "<td>" . $value->module->module_name  . "</td>";
                $str .= "<td>";
                if(isset($value->permissions)){
                    $str .= "<ul>";
                        foreach($value->permissions as $k => $v){
                            $str .= "<li>".$v->permission_name ."</li>";
                        }
                    $str .= "</ul>";
                }
                $str .= "</td>";
                $str .= "<td>" . (($value->is_publish)?"Công khai":"Không công khai") . "</td>";
                $str .= "<td>" . $value->day_create_string . "</td>";
                $str .= "<td>" . $value->day_update_string . "</td>";
                $str .= "<td>" . $value->user_create->full_name . "</td>";
                $str .= "<td>" . ((isset($value->user_update)) ? $value->user_update->full_name : "" ). "</td>";
                $str .= "<td>";
                if($this->is_edited){
                    $str .= "<a href='". route("cap_nhat_mo_dun_va_quyen_han_page_path", ["id" => $value->_id]) ."' class='btn btn-white btn-warning btn-xs'>
                            <i class='ace-icon fa fa-wrench  bigger-110 icon-only'></i>
                        </a>&nbsp;";
                }
                if($this->is_deleted){
                    $str .= "<button data-toggle='modal' data-target='#myModal' onclick='storeId(\"" . $value->_id ."\")' class='btn btn-white btn-danger btn-xs'>
                            <i class='ace-icon fa fa-trash-o  bigger-110 icon-only'></i>
                        </button>";
                }                
                $str .= "</td>";
                $str .= "</tr>";
            }
            return $str;
        }
    }

    public function edit($id, Request $request) {
        if($this->is_edited){
            $dataResponse = ModulePermission::takeModulePermissionById($id);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["modulePermission"] = $dataResponse->data;
                    $dataUpdate = ModulePermission::getDataForCreate();
                    if(isset($dataUpdate)) {
                        if($dataUpdate->code == $this->code_response["code_found_data"]){
                            $this->data["update"] = $dataUpdate->data;
                        }else {
                            $request->session()->flash(Utilities::status_alert()["status_error"] , $dataUpdate->msg);
                        }
                    } else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                    }
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }

            return view("module_permission.edit")
                ->with("list_permission", $this->permissions)
                ->with("title", $this->title_active)
                ->with("data", $this->data["modulePermission"])
                ->with("permissions", $this->data["update"]->permissions)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        } 
    }

    public function editModulePermission(Request $request) {
        if(session()->has("profile")){
            if($this->is_created){
                $this->validate($request, [
                    "_id" => "required",
                    "permissions" => "required",
                    "publish" => "required",
                    "module_permission" => "required"
                ],[
                    "_id.required" => "Mã mô-đun và quyền hạn không được để trống",
                    "permissions.required" => "Quyền hạn không được để trống",
                    "publish.required" => "Công bố phải được chọn",
                    "module_permission.required" => "Tên mô-đun và quyền hạn không được để trống"
                ]);
                $modulePermission = $request->_id;
                $dataPost = [
                    "id" => $modulePermission,
                    "permissions" => $request->permissions,
                    "publish" => $request->publish,
                    "module_permission" => $request->module_permission,
                    "user_update" => $this->user_id
                ];        
                $dataResponse = ModulePermission::updateModulePermission("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_update_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
                return redirect(route("cap_nhat_mo_dun_va_quyen_han_page_path", ['id'=>$modulePermission]));
            }else{
                return view("errors.permission_denied")
                    ->with("list_menu", $this->menu);
            }
        }
    }

    public function deleteModulePermissionAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_deleted){
                $this->validate($request, [
                    "item_id" => "required"
                ],[
                    "item_id.required" => "Mã hồ sơ không được để trống"
                ]);                
                $dataPost = [
                    "module_permission_id" => $request->item_id
                ];
                $dataResponse = ModulePermission::deleteModulePermission("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_delete_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }
}