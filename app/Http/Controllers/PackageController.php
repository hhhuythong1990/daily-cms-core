<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Model\Package;
use App\Model\User;
use App\Helpers\Utilities;
use App\Helpers\Helper;

class PackageController extends Controller {
    private $code_response;
    private $default_limit;
    private $default_current_page;
    private $menu = null;
    private $permissions = null;
    private $user_id = null;
    private $data = null;
    private $is_viewed = false;
    private $is_created = false;
    private $is_edited = false;
    private $is_deleted = false;
    private $search_data = null;

    public function __construct() {
        $this->title_active = "Gói";
        $this->default_current_page = Utilities::pagination()["DEFAULT_CURRENT_PAGE"];
        $this->default_limit = Utilities::pagination()["DEFAULT_PAGE_10"];        
        $this->code_response = Utilities::apiCodeResponse();
        if(session()->has("profile")){
            $this->menu = session()->get("profile")->group_modules;
            $this->permissions = session()->get("profile")->permissions;
            $this->user_id = session()->get("profile")->_id;
            $this->is_viewed = in_array(Utilities::constantPermissions()["PACKAGE_VIEW"], $this->permissions);
            $this->is_created = in_array(Utilities::constantPermissions()["PACKAGE_CREATE"], $this->permissions);
            $this->is_edited = in_array(Utilities::constantPermissions()["PACKAGE_EDIT"], $this->permissions);
            $this->is_deleted = in_array(Utilities::constantPermissions()["PACKAGE_DELETE"], $this->permissions);
        }
    }

    public function index(Request $request) {        
        if($this->is_viewed){
            $dataResponse = Package::takePackageBySkipLimit($this->default_current_page, $this->default_limit); 
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data["data"] = $dataResponse->data;
                    $this->data["total"] = $dataResponse->total;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            
            return view("package.index")
                ->with("title", $this->title_active)
                ->with("data", $this->data["data"])
                ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu)
                ->with("search_data", $this->search_data);
        }
        else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }
    }

    public function edit($id, Request $request) {
        if($this->is_edited){
            $dataResponse = Package::takePackageByPackageId($id); 
            
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){
                    $this->data = $dataResponse->data;
                }else{
                    $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                }
            }else {
                $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
            }
            return view("package.edit")
                ->with("title", $this->title_active)
                ->with("data", $this->data)
                ->with("list_permission", $this->permissions)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        } 
    }

    public function create() {        
        if($this->is_created){
            
            return view("package.create")
                ->with("title", $this->title_active)
                ->with("list_menu", $this->menu);
        }else{
            return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
        }        
    }

    public function createPackageAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_created){
                $this->validate($request, [
                    "package_name" => "required",
                    "plan_id" => "required",
                    "package_price" => "required",
                    "package_description" => "required",
                    "package_status" => "required",
                ],[
                    "package_name.required" => "Tên gói không được để trống",
                    "plan_id.required" => "Plan id không được để trống",
                    "package_description.required" => "Giá của gói không được để trống",
                    "package_price.required" => "Mô tả của gói không được để trống",
                    "package_status.required" => "Trạng thái của gói phải được chọn",
                ]);               

                $dataPost = [
                    "package_name" => $request->package_name,
                    "plan_id" => $request->plan_id,
                    "package_description" => $request->package_description,
                    "package_price" => $request->package_price,
                    "package_status" => $request->package_status,
                    "user_create" => session()->get("profile")->_id,
                ];        
                $dataResponse = Package::createPackage("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_add_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function checkPackageNameAjax(Request $request){
        if($this->is_created){
            $dataPost = [
                "package_name" => $request->package_name    
            ];
            $dataResponse = Package::takePackageByPackageName("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    public function checkPlanIdAjax(Request $request){
        if($this->is_created){
            $dataPost = [
                "plan_id" => $request->plan_id
            ];
            $dataResponse = Package::takePackageByPlanId("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    public function checkPackagePlanExist($packageId, Request $request){
        if($this->is_edited){
            $dataPost = [
                "package_id" => $packageId,
                "plan_id" => $request->plan_id
            ];
            $dataResponse = Package::takePackageByPackageIdAndPlanId("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    public function checkPackageNameExist($packageId, Request $request){
        if($this->is_edited){
            $dataPost = [
                "package_id" => $packageId,
                "package_name" => $request->package_name
            ];
            $dataResponse = Package::takePackageByPackageIdAndPackageName("POST", $dataPost);
            if(isset($dataResponse)) {
                if($dataResponse->code == $this->code_response["code_found_data"]){                    
                    echo(json_encode(false));
                }else{
                    echo(json_encode(true));
                }
            }
        }
    }

    public function editPackageAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_edited){            
                $this->validate($request, [
                    "package_id" => "required",
                    "package_name" => "required",
                    "plan_id" => "required",
                    "package_price" => "required",
                    "package_description" => "required",
                    "package_status" => "required",
                ],[
                    "package_id.required" => "Mã định danh gói không được để trống",
                    "package_name.required" => "Tên gói không được để trống",
                    "plan_id.required" => "Plan id không được để trống",
                    "package_description.required" => "Giá của gói không được để trống",
                    "package_price.required" => "Mô tả của gói không được để trống",
                    "package_status.required" => "Trạng thái của gói phải được chọn",
                ]);               

                $dataPost = [
                    "package_id" => $request->package_id,
                    "package_name" => $request->package_name,
                    "plan_id" => $request->plan_id,
                    "package_description" => $request->package_description,
                    "package_price" => $request->package_price,
                    "package_status" => $request->package_status,
                    "user_update" => $this->user_id
                ];        
                $dataResponse = Package::updatePackage("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_update_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function takePackageSearchData(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){
                $packageName = $request->package_name;                
                if($packageName != null){
                    $skip = $this->default_current_page;
                
                    $dataResponse = Package::takePackageByDataSearchSkipLimit($skip, $this->default_limit, urlencode($packageName));
                    if(isset($dataResponse)) {
                        if($dataResponse->code == $this->code_response["code_found_data"]){
                            $this->data["data"] = $dataResponse->data;
                            $this->data["total"] = $dataResponse->total;
                            $this->search_data = $packageName;
                        }else{
                            $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                        }
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                    }
                    
                    return view("package.index")
                    ->with("title", $this->title_active)
                    ->with("data", $this->data["data"])
                    ->with("pages", Utilities::processPagination($this->data["total"], $this->default_limit))
                    ->with("list_permission", $this->permissions)
                    ->with("list_menu", $this->menu)
                    ->with("search_data", $this->search_data);
                }else{
                    return redirect(route("danh_sach_goi_page_path"));
                }
            }else {
                return view("errors.permission_denied")
                ->with("list_menu", $this->menu);
            }
        }
    }

    public function deletePackageAjax(Request $request) {
        if(session()->has("profile")){
            if($this->is_deleted){
                $this->validate($request, [
                    "item_id" => "required"
                ],[
                    "item_id.required" => "Mã định danh mô-đun không được để trống"
                ]);                
                $dataPost = [
                    "package_id" => $request->item_id
                ];                
                $dataResponse = Package::deletePackage("POST", $dataPost);
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_delete_successfully"]){
                        $request->session()->flash(Utilities::status_alert()["status_success"] , $dataResponse->msg);
                    }else {
                        $request->session()->flash(Utilities::status_alert()["status_error"] , $dataResponse->msg);
                    }
                } else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    public function takeModulePageAjax(Request $request) {
        if(session()->has("profile")){  
            if($this->is_viewed){      
                $skip = ((int) $request->page != 0) ? $request->page : $this->default_current_page;        
                $dataSearch = $request->data_search;

                if($dataSearch == null){
                    $dataResponse = Package::takePackageBySkipLimit($skip, $this->default_limit);
                }else {
                    $permissionName = $request->data_search;
                    $dataResponse = Package::takePackageByDataSearchSkipLimit($skip, $this->default_limit, urlencode($permissionName));
                }
                
                if(isset($dataResponse)) {
                    if($dataResponse->code == $this->code_response["code_found_data"]){
                        $this->data = $dataResponse->data;
                        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], false, $this->drawDataPackage($this->data, $skip));
                    }
                }else {
                    $request->session()->flash(Utilities::status_alert()["status_error"] , Utilities::message()["error_connect_api"]);
                }
            }
        }
        return Utilities::jsonResponse(Utilities::httpCode()["http_200"], true);
    }

    private function drawDataPackage($data, $page_num) {
        if(!empty($data)){
            $str = "";
            $no = Utilities::numberIncrease($page_num, $this->default_limit);;
            foreach($data as $key => $value){
                $str .= "<tr>";
                $str .= "<td>" . ($no + $key + 1) . "</td>";
                $str .= "<td>" . $value->package_name  . "</td>";
                $str .= "<td>" . $value->plan_id . "</td>";
                $str .= "<td>" . $value->package_price . "</td>";
                $str .= "<td>" . $value->package_description . "</td>";
                $str .= "<td>" . (($value->package_status)?"Vô hiệu":"Hoạt động") . "</td>";
                $str .= "<td>" . $value->day_create_string . "</td>";
                $str .= "<td>" . $value->day_update_string . "</td>";
                $str .= "<td>" . $value->user_create->full_name . "</td>";
                $str .= "<td>" . ((isset($value->user_update)) ? $value->user_update->full_name : "" ). "</td>";
                $str .= "<td>";
                if($this->is_edited){
                    $str .= "<a href='". route('cap_nhat_goi_page_path', ['id'=>$value->_id]). "' class='btn btn-white btn-warning btn-xs'>
                            <i class='ace-icon fa fa-wrench  bigger-110 icon-only'></i>
                        </a>&nbsp;";
                }
                if($this->is_deleted){
                    $str .= "<button data-toggle='modal' data-target='#myModal' onclick='storeId(\"" . $value->_id ."\")' class='btn btn-white btn-danger btn-xs'>
                            <i class='ace-icon fa fa-trash-o  bigger-110 icon-only'></i>
                        </button>";
                }                
                $str .= "</td>";
                $str .= "</tr>";
            }
            return $str;
        }
    }
}