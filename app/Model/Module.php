<?php
namespace App\Model;

use \App\Helpers\Helper as Helpers;
use Illuminate\Support\Facades\Config;

class Module {

    public static function createModule($method, $data) {
        $url = Config::get("constants.host_static")."/module/create";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takeAllModule() {
        $url = Config::get("constants.host_static")."/module/takeAll";
        return Helpers::curl_dai_ly($url);
    }

    public static function takeModuleBySkipLimit($skip, $limit) {
        $url = Config::get("constants.host_static")."/module/takeBySkipLimit?skip=" . $skip . "&limit=" . $limit;
        return Helpers::curl_dai_ly($url);
    }

    public static function takeModuleByDataSearchSkipLimit($skip, $limit, $moduleName){
        $url = Config::get("constants.host_static")."/module/takeByDataSearchSkipLimit?skip=".$skip."&limit=".$limit."&moduleName=".$moduleName;                
        return Helpers::curl_dai_ly($url);
    }

    public static function takeModuleByModuleId($id) {
        $url = Config::get("constants.host_static")."/module/takeModule?module_id=" . $id;
        return Helpers::curl_dai_ly($url);
    }

    public static function updateModule($method, $data) {
        $url = Config::get("constants.host_static")."/module/update";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function deleteModule($method, $data) {
        $url = Config::get("constants.host_static")."/module/remove";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takeModuleByModuleName($method, $data){
        $url = Config::get("constants.host_static")."/module/takeModuleByModuleName";                
        return Helpers::curl_dai_ly($url, $method, $data);
    } 

    public static function takeModuleByIdAndModuleName($method, $data){
        $url = Config::get("constants.host_static")."/module/takeModuleByIdAndModuleName";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
}