<?php
namespace App\Model;

use \App\Helpers\Helper as Helpers;
use Illuminate\Support\Facades\Config;

class Permission {

    public static function createPermission($method, $data) {
        $url = Config::get("constants.host_static")."/permission/create";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takeAllPermission(){
        $url = Config::get("constants.host_static")."/permission/takeAllPermission";                
        return Helpers::curl_dai_ly($url);
    }

    public static function takePermissionBySkipLimit($skip, $limit){
        $url = Config::get("constants.host_static")."/permission/takeBySkipLimit?skip=".$skip."&limit=".$limit;                
        return Helpers::curl_dai_ly($url);
    }

    public static function takePermissionByDataSearchSkipLimit($skip, $limit, $permissionName){
        $url = Config::get("constants.host_static")."/permission/takeByDataSearchSkipLimit?skip=".$skip."&limit=".$limit."&permissionName=".$permissionName;                
        return Helpers::curl_dai_ly($url);
    }

    public static function deletePermission($method, $data) {
        $url = Config::get("constants.host_static")."/permission/remove";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
    
    public static function takePermissionById($permission){
        $url = Config::get("constants.host_static")."/permission/takePermissionById?permission_id=".$permission;                
        return Helpers::curl_dai_ly($url);
    }

    public static function takePermissionByIdAndPermissionName($method, $data){
        $url = Config::get("constants.host_static")."/permission/takePermissionByIdAndPermissionName";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function updatePermission($method, $data){
        $url = Config::get("constants.host_static")."/permission/update";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
    
    public static function takePermissionByPermissionName($method, $data){
        $url = Config::get("constants.host_static")."/permission/takePermissionByPermissionName";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
    
}