<?php
namespace App\Model;

use \App\Helpers\Helper as Helpers;
use Illuminate\Support\Facades\Config;

class Package {
    public static function takePackageBySkipLimit($skip, $limit) {
        $url = Config::get("constants.host_static")."/package/takeBySkipLimit?skip=" . $skip . "&limit=" . $limit;
        return Helpers::curl_dai_ly($url);
    }

    public static function takePackageByDataSearchSkipLimit($skip, $limit, $packageName){
        $url = Config::get("constants.host_static")."/package/takeByDataSearchSkipLimit?skip=".$skip."&limit=".$limit."&packageName=".$packageName;                
        return Helpers::curl_dai_ly($url);
    }

    public static function createPackage($method, $data) {
        $url = Config::get("constants.host_static")."/package/create";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takePackageByPackageName($method, $data) {
        $url = Config::get("constants.host_static")."/package/takePackageByPackageName";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takePackageByPlanId($method, $data) {
        $url = Config::get("constants.host_static")."/package/takePackageByPlanId";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takePackageByPackageId($id) {
        $url = Config::get("constants.host_static")."/package/getPackageById?package_id=" . $id;
        return Helpers::curl_dai_ly($url);
    }

    public static function takePackageByPackageIdAndPlanId($method, $data){
        $url = Config::get("constants.host_static")."/package/takePackageByIdAndPlanId";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takePackageByPackageIdAndPackageName($method, $data){
        $url = Config::get("constants.host_static")."/package/takePackageByIdAndPackageName";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function updatePackage($method, $data){
        $url = Config::get("constants.host_static")."/package/update";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
    
    public static function deletePackage($method, $data){
        $url = Config::get("constants.host_static")."/package/remove";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
    
}

?>