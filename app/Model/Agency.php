<?php
namespace App\Model;

use \App\Helpers\Helper as Helpers;
use Illuminate\Support\Facades\Config;

class Agency {

    public static function createAgency($method, $data) {
        $url = Config::get("constants.host_static")."/agency/create";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takeAgencyByAgencyCode($method, $data) {
        $url = Config::get("constants.host_static")."/agency/takeAgencyByAgencyCode";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takeAgencyBySkipLimit($skip, $limit) {
        $url = Config::get("constants.host_static")."/agency/takeAgencyBySkipLimit?skip=" . $skip . "&limit=" . $limit;
        return Helpers::curl_dai_ly($url);
    } 

    public static function takeByDataSearchSkipLimit($skip, $limit, $agencyName) {
        $url = Config::get("constants.host_static")."/agency/takeDataSearchAgency?skip=" . $skip . "&limit=" . $limit."&agency_name=".$agencyName;
        return Helpers::curl_dai_ly($url);
    }    
    
    public static function takeAgencyByAgencyId($agencyId) {
        $url = Config::get("constants.host_static")."/agency/takeAgencyById?agency_id=" . $agencyId;
        return Helpers::curl_dai_ly($url);
    }

    public static function takeAgencyByIdAndUsername($method, $data) {
        $url = Config::get("constants.host_static")."/agency/takeAgencyByIdAndUsername";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function getDataForCreateAgency() {
        $url = Config::get("constants.host_static")."/agency/dataRelation";                
        return Helpers::curl_dai_ly($url);
    }

    public static function updateAgency($method, $data) {
        $url = Config::get("constants.host_static")."/agency/update";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
}

?>