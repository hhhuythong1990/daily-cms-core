<?php
namespace App\Model;

use \App\Helpers\Helper as Helpers;
use Illuminate\Support\Facades\Config;

class Revenue {

    public static function getData($method, $data) {
        $url = Config::get("constants.host_static")."/requestGiftCode/getData";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function exportData($method, $data) {
        $url = Config::get("constants.host_static")."/requestGiftCode/export";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
    
}