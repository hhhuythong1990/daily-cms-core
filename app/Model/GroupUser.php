<?php
namespace App\Model;

use \App\Helpers\Helper as Helpers;
use Illuminate\Support\Facades\Config;

class GroupUser {

    public static function createGroupUser($method, $data) {
        $url = Config::get("constants.host_static")."/groupUser/create";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takeGroupUserBySkipLimit($skip, $limit) {
        $url = Config::get("constants.host_static")."/groupUser/takeBySkipLimit?skip=" . $skip . "&limit=" . $limit;
        return Helpers::curl_dai_ly($url);
    }

    public static function takeGroupAdmin($method, $data) {
        $url = Config::get("constants.host_static")."/groupUser/takeGroupAdmin";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
    
}