<?php
namespace App\Model;

use \App\Helpers\Helper as Helpers;
use Illuminate\Support\Facades\Config;

class GroupModule {

    public static function createGroupModule($method, $data) {
        $url = Config::get("constants.host_static")."/groupModule/create";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takeGroupModuleBySkipLimit($skip, $limit) {
        $url = Config::get("constants.host_static")."/groupModule/takeBySkipLimit?skip=" . $skip . "&limit=" . $limit;
        return Helpers::curl_dai_ly($url);
    }

    public static function takeGroupModuleByDataSearchSkipLimit($skip, $limit, $groupModuleName){
        $url = Config::get("constants.host_static")."/groupModule/takeByDataSearchSkipLimit?skip=".$skip."&limit=".$limit."&groupModuleName=".$groupModuleName;                
        return Helpers::curl_dai_ly($url);
    }

    public static function takeAllGroupModule() {
        $url = Config::get("constants.host_static")."/groupModule/takeAll";
        return Helpers::curl_dai_ly($url);
    }

    public static function checkGroupModuleByGroupModuleName($method, $data){
        $url = Config::get("constants.host_static")."/groupModule/takeGroupModuleByGroupModuleName";                
        return Helpers::curl_dai_ly($url, $method, $data);
    } 

    public static function takeGroupModule($id) {
        $url = Config::get("constants.host_static")."/groupModule/takeGroupModule?group_module_id=" . $id;
        return Helpers::curl_dai_ly($url);
    }

    public static function deleteGroupModule($method, $data) {
        $url = Config::get("constants.host_static")."/groupModule/remove";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takeGroupModuleByIdAndGroupModuleName($method, $data){
        $url = Config::get("constants.host_static")."/groupModule/takeGroupModuleByIdAndGroupModuleName";                
        return Helpers::curl_dai_ly($url, $method, $data);
    } 

    public static function updateGroupModule($method, $data) {
        $url = Config::get("constants.host_static")."/groupModule/update";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
}