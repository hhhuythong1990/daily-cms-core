<?php
namespace App\Model;

use \App\Helpers\Helper as Helpers;
use Illuminate\Support\Facades\Config;

class User {

    public static function createUser($method, $data) {
        $url = Config::get("constants.host_static")."/profile/create";
        return Helpers::curl_dai_ly($url, $method, $data);
    } 

    public static function authenticateUser($method, $data) {
        $url = Config::get("constants.host_static")."/profile/authentication";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function getDataForCreateUser() {
        $url = Config::get("constants.host_static")."/profile/dataRelation";                
        return Helpers::curl_dai_ly($url);
    }

    public static function takeUserBySkipLimit($skip, $limit) {
        $url = Config::get("constants.host_static")."/profile/takeAll?skip=" . $skip . "&limit=" . $limit;
        return Helpers::curl_dai_ly($url);
    }

    public static function takeUserByUserId($id) {
        $url = Config::get("constants.host_static")."/profile/takeProfile?profile_id=" . $id;
        return Helpers::curl_dai_ly($url);
    }

    public static function checkUserNameExist($method, $data) {
        $url = Config::get("constants.host_static")."/profile/takeProfileByUserName";
        return Helpers::curl_dai_ly($url, $method, $data);
    }  

    public static function takeProfileByIdAndUserName($method, $data) {
        $url = Config::get("constants.host_static")."/profile/takeProfileByIdAndUserName";
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function deleteProfile($method, $data) {
        $url = Config::get("constants.host_static")."/profile/remove";
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takeByDataSearchSkipLimit($skip, $limit, $userName) {
        $url = Config::get("constants.host_static")."/profile/takeByDataSearchSkipLimit?skip=" . $skip . "&limit=" . $limit."&userName=".$userName;
        return Helpers::curl_dai_ly($url);
    }
    
    public static function updateProfile($method, $data) {
        $url = Config::get("constants.host_static")."/profile/update";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function changePassword($method, $data) {
        $url = Config::get("constants.host_static")."/profile/changePassword";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
    

    public static function changeOnlineStatus($method, $data) {
        $url = Config::get("constants.host_static")."/profile/changeOnline";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function changeBlockStatus($method, $data) {
        $url = Config::get("constants.host_static")."/profile/changeBlock";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function logoutUser($method, $data) {
        $url = Config::get("constants.host_static")."/profile/logout";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takeProfileChange($method, $data) {
        $url = Config::get("constants.host_static")."/profile/takeProfileChange";
        return Helpers::curl_dai_ly($url, $method, $data);
    }
}