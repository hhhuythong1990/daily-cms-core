<?php
namespace App\Model;

use \App\Helpers\Helper as Helpers;
use Illuminate\Support\Facades\Config;

class ModulePermission {

    public static function createModulePermission($method, $data) {
        $url = Config::get("constants.host_static")."/modulePermission/create";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }

    public static function takeModulePermissionBySkipLimit($skip, $limit) {
        $url = Config::get("constants.host_static")."/modulePermission/takeBySkipLimit?skip=" . $skip . "&limit=" . $limit;
        return Helpers::curl_dai_ly($url);
    }

    public static function getDataForCreate() {
        $url = Config::get("constants.host_static")."/modulePermission/dataRelation";                
        return Helpers::curl_dai_ly($url);
    }

    public static function takeModulePermissionById($id) {
        $url = Config::get("constants.host_static")."/modulePermission/getModulePermissionById?module_permission_id=" . $id;
        return Helpers::curl_dai_ly($url);
    }
    
    public static function updateModulePermission($method, $data) {
        $url = Config::get("constants.host_static")."/modulePermission/update";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
    
    public static function deleteModulePermission($method, $data) {
        $url = Config::get("constants.host_static")."/modulePermission/remove";
        return Helpers::curl_dai_ly($url, $method, $data);
    }
}