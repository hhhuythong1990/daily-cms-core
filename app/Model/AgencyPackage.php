<?php
namespace App\Model;

use \App\Helpers\Helper as Helpers;
use Illuminate\Support\Facades\Config;

class AgencyPackage {

    public static function takeAgencyPackageData() {
        $url = Config::get("constants.host_static")."/agencyPackage/dataRelation";                
        return Helpers::curl_dai_ly($url);
    }

    public static function newAgencyPackage($method, $data) {
        $url = Config::get("constants.host_static")."/agencyPackage/create";                
        return Helpers::curl_dai_ly($url,$method, $data);
    }

    public static function takeAgencyPackageBySkipLimit($skip, $limit) {
        $url = Config::get("constants.host_static")."/agencyPackage/takeBySkipLimit?skip=" . $skip . "&limit=" . $limit;
        return Helpers::curl_dai_ly($url);
    }

    public static function takeByDataSearchSkipLimit($skip, $limit, $agencyPackageName) {
        $url = Config::get("constants.host_static")."/agencyPackage/takeByDataSearchSkipLimit?skip=" . $skip . "&limit=" . $limit."&agencyPackageName=".$agencyPackageName;
        return Helpers::curl_dai_ly($url);
    }

    public static function takeAgencyPackageById($id) {
        $url = Config::get("constants.host_static")."/agencyPackage/takeAgencyPackageById?agency_package_id=" . $id;
        return Helpers::curl_dai_ly($url);
    }

    public static function updateAgencyPackage($method, $data) {
        $url = Config::get("constants.host_static")."/agencyPackage/update";                
        return Helpers::curl_dai_ly($url, $method, $data);
    }
    
    public static function removeAgencyPackage($id) {
        $url = Config::get("constants.host_static")."/agencyPackage/remove?agency_package=".$id;
        return Helpers::curl_dai_ly($url);
    }

    public static function getCode($skip, $limit) {
        $url = Config::get("constants.host_static")."/code/takeBySkipLimit?skip=" . $skip . "&limit=" . $limit;
        return Helpers::curl_dai_ly($url);
    }

    public static function takeCodeByDataSearchSkipLimit($skip, $limit, $codeId) {
        $url = Config::get("constants.host_static")."/code/takeByDataSearchSkipLimit?skip=" . $skip . "&limit=" . $limit. "&code_id=".$codeId;
        return Helpers::curl_dai_ly($url);
    }
}

?>