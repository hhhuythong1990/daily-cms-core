<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/21/2017
 * Time: 3:32 PM
 */
namespace App\Model;

use \App\Helpers\Helper as Helpers;
use Illuminate\Support\Facades\Config;

class Authenticate {


    public static function getAuth($method, $data)
    {
        $url = Config::get("constants.host_static")."/api/authentication";
        return Helpers::curl_epost($url, $method, $data);
    }
}