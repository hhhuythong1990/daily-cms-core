<?php
namespace App\Helpers;

class Utilities {
    public static function apiCodeResponse(){
        $code = [
            "code_found_data" => 6,
            "code_add_successfully" => 8,
            "code_update_successfully" => 10,
            "code_delete_successfully" => 12,
            "login_successfully" => 14,
            "check_profile_changed" => 18,
            "custom_message" => 99,

            // "code_query_error" => 4,
            // "code_exist_object" => 5,
            // "code_not_exist_object" => 6,
            
            // "code_not_found_data" => 8,            
            // "code_add_fail" => 10,
            
            // "code_update_fail" => 12,
            
            // "login_incorrect_user_name" => 14,
            // "login_incorrect_password" => 15,
            // "login_successfully" => 16,
        ];
        return $code;
    }

    public static function convertVND($price, $round, $strMoney){
        return number_format($price, $round, ',', '.') . " " .$strMoney;
    }

    public static function pagination() {
        $page_number = [
            "DEFAULT_CURRENT_PAGE" => 1,
            "DEFAULT_PAGE_10" => 10,
            "DEFAULT_PAGE_50" => 50,
            "DEFAULT_PAGE_100" => 100,
            "DEFAULT_PAGE_1000" => 1000
        ];
        return $page_number;
    }

    public static function httpCode(){
        $httpCode = [
            "http_200" => 200,
            "http_500" => 500
        ];
        return $httpCode;
    }

    public static function message(){
        $msg = [
            "error_connect_api" => "Lỗi kết nối API",
            "dang-nhap" => "/dang-nhap"
        ];
        return $msg;
    }

    public static function status_alert(){
        $status = [
            "status_success" => "alert-success",
            "status_error" => "alert-danger"
        ];
        return $status;
    }

    public static function jsonResponse($httpCode, $status, $data =  null){
        $returnData = array();
        $returnData["status"] = $httpCode;
        $returnData["refresh"] = $status;
        $returnData["data"] = $data;
        return response()->json($returnData);
    }

    public static function numberIncrease($page_num, $default_limit){
        return ($page_num - 1) * $default_limit;
    }

    // public static function jsonResponse($httpCode, $errorStatus, $customKey ,$data){
    //     $returnData = array();
    //     $returnData["status"] = $httpCode;
    //     $returnData["error"] = $errorStatus;
    //     $returnData[$customKey] = $data;
    //     return json_encode($returnData);
    // }

    public static function arrayResponse($status ,$msg){
        $returnData = array();
        $returnData["status"] = $status;
        $returnData["msg"] = $msg;
        return $returnData;
    }

    public static function processPagination($total, $limit) {
        $totalPages = ceil($total / $limit);
        return $totalPages;
    }

    public static function constantPermissions(){
        $str = [
            //Quyen han
            "PERMISSION_VIEW" => "5a261bd81b33952c6cba167c",
            "PERMISSION_CREATE" => "5a261bc91b33952c6cba167b",
            "PERMISSION_EDIT" => "5a261bfd1b33952c6cba167d",
            "PERMISSION_DELETE" => "5a261c051b33952c6cba167e",

            //Mo-dun
            "MODULE_VIEW" => "5a261c181b33952c6cba167f",
            "MODULE_CREATE" => "5a261c2e1b33952c6cba1680",
            "MODULE_EDIT" => "5a261c401b33952c6cba1681",
            "MODULE_DELETE" => "5a261c4b1b33952c6cba1682",
            
            //Nhom do-dun
            "GROUP_MODULE_VIEW" => "5a261c671b33952c6cba1683",
            "GROUP_MODULE_CREATE" => "5a261c6f1b33952c6cba1684",
            "GROUP_MODULE_EDIT" => "5a261c791b33952c6cba1685",
            "GROUP_MODULE_DELETE" => "5a261c831b33952c6cba1686",

            //Nhom nguoi dung
            "GROUP_USER_VIEW" => "5a261ce11b33952c6cba1687",
            "GROUP_USER_CREATE" => "5a261cf91b33952c6cba1688",
            "GROUP_USER_EDIT" => "5a261d171b33952c6cba1689",
            "GROUP_USER_DELETE" => "5a261d1f1b33952c6cba168a",

            //Nguoi dung
            "USER_VIEW" => "5a261d321b33952c6cba168b",
            "USER_CREATE" => "5a261d481b33952c6cba168c",
            "USER_EDIT" => "5a261d511b33952c6cba168d",
            "USER_DELETE" => "5a261d581b33952c6cba168e",
            "USER_BLOCK" => "5bff543742d5bd7651b5a9a9",
            "USER_OFFLINE" => "5bfd11feb7f54d19341897d2",

            //Đại lý
            "AGENCY_VIEW" => "5b7284a8b9ccf23c9cd4d62a",
            "AGENCY_CREATE" => "5b72848db9ccf23c9cd4d629",
            "AGENCY_EDIT" => "5b7284b6b9ccf23c9cd4d62b",
            "AGENCY_DELETE" => "5b7284c5b9ccf23c9cd4d62c",

            //Gói
            "PACKAGE_VIEW" => "5b6bb95b1882c3197036f062",
            "PACKAGE_CREATE" => "5b6bb9b31882c3197036f063",
            "PACKAGE_EDIT" => "5b6bb9e21882c3197036f064",
            "PACKAGE_DELETE" => "5b6bb9fd1882c3197036f065",

            "PACKAGE_GET_CODE" => "5b977b182250802ebc963753",
            "CODE_SEARCH" => "5c22f7e1ec0b893c3aebf59c",
            "CODE_EXPORT" => "5c22f7f1ec0b893c3aebf59d",

            //Đại lý và gói
            "AGENCY_PACKAGE_VIEW" => "5b6be1b71882c3197036f06a",
            "AGENCY_PACKAGE_CREATE" => "5b6be1db1882c3197036f06b",
            "AGENCY_PACKAGE_EDIT" => "5b6be2011882c3197036f06c",
            "AGENCY_PACKAGE_DELETE" => "5b6be2111882c3197036f06d",

            //Mô-đun và quyền hạn
            "MODULE_PERMISSION_VIEW" => "5bb47e3d07d016108c17fbc3",
            "MODULE_PERMISSION_CREATE" => "5bb47e2307d016108c17fbc2",
            "MODULE_PERMISSION_EDIT" => "5bb47e6207d016108c17fbc4",
            "MODULE_PERMISSION_DELETE" => "5bb47e6a07d016108c17fbc5",

            //Doanh thu
            "REVENUE_VIEW" => "5c187fec6dd00b2a92f18314",
            "REVENUE_SEARCH" => "5c187ffb6dd00b2a92f18315",
            "REVENUE_EXPORT" => "5c1880066dd00b2a92f18316",
        ];
        return $str;
    }

}