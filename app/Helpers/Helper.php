<?php
namespace App\Helpers;

use App;
use Session;
use Illuminate\Support\Facades\Config;

class Helper
{

    public static function curl($url, $method = "GET", $params = array(), $type = 'object') {

        $secure_key = "WEBknfl6isBTdaRIJNv6vjmm8ifdieKnx9CtO";
        $expire_time = time() + 3*3600;

        $uri = str_replace("https://api.fptplay.net", "", $url);

        $uri_secure = $uri;
        if(strrpos($uri, "?")){
            $uri_secure = substr($uri, 0, strrpos($uri, "?"));
        }

        $secure_link = $secure_key.$expire_time.$uri_secure;
        $secure_link = md5($secure_link, TRUE);
        $secure_link = base64_encode($secure_link);
        $secure_link = str_replace("+","-",$secure_link);
        $secure_link = str_replace("/","_",$secure_link);
        $secure_link = str_replace("=", "", $secure_link);

        $browserName = self::getBrowserName();
        $browserName = sprintf('%s(version:%s,model:%s)', $browserName->model, $browserName->version, $browserName->model);

        $user = Session::get('user');
        if ($user === null) {
            $user = "null";
        } else {
            $user = $user['user_id'];
        }

        if(strrpos($uri, "?")){
            $url = $url . "&st=$secure_link&e=$expire_time" . "&version=fptplay_2.20&device=". urlencode($browserName) ."&nettype=wifi&user_id=".$user;
        } else{
            $url = $url . "?st=$secure_link&e=$expire_time" . "&version=fptplay_2.20&device=". urlencode($browserName) ."&nettype=wifi&user_id=".$user;
        }
//        var_dump($url);die;

        if(!empty($params) && $type === 'json'){
            $data_string = json_encode($params);
        } else if(!empty($params) && $method == "POST" && $type !== 'json'){
            $data_string = '';
            foreach($params as $key=>$value) { $data_string .= $key.'='.$value.'&'; }
        }

        $ch = curl_init();

        $country_code = '';
        if(isset($_SERVER['HTTP_X_GEOIP_COUNTRY_CODE'])){
            $country_code = 'X-GeoIP-Country-Code: ' . $_SERVER['HTTP_X_GEOIP_COUNTRY_CODE'];
        }

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(isset($params['get_header']))
        {
            curl_setopt($ch, CURLOPT_HEADER, 1);
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        if(!empty($params) && $method == "POST" && $type !== 'json'){
            curl_setopt($ch, CURLOPT_POST, count($params));
            //curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }
        $user = Session::get('user');

        $headers = array();
        if (isset($_SERVER['HTTP_X_VID'])) {
            $headers = array_merge($headers, array(
                'X-VID: '.$_SERVER['HTTP_X_VID'],
                'X-VTYPE: '.$_SERVER['HTTP_X_VTYPE'],
            ));
        }

        if($type === 'json'){
            if(isset($user)){
                curl_setopt($ch, CURLOPT_POST, count($params));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($headers, array(
                    'Content-Type: application/json',
                    'Content-Length: '. strlen($data_string),
                    $country_code,
                    'Authorization: Bearer ' . $user['token'],
                    'X-DID: null'
                )));
            } else{
                $is_authorization = '';
                if(isset($params['token'])){
                    $is_authorization = 'Authorization: Bearer ' . $params['token'];
                }
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($headers, array(
                    'Content-Type: application/json',
                    $country_code,
                    $is_authorization,
                    'Content-Length: ' . strlen($data_string),
                    'X-DID: null'
                )));
            }
        } else{
            if(isset($user)){
                curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($headers, array(
                    'Authorization: Bearer ' . $user['token'],
                    $country_code,
                    'X-DID: null'
                )));
            } else{
                curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($headers, array(
                    $country_code,
                    'X-DID: null'
                )));
            }
        }

        //execute post
        $result = curl_exec($ch);
        if(isset($params['get_header']))
        {
            list($headers, $response) = explode("\r\n\r\n", $result, 2);

            $headers = explode("\r\n", $headers);

            foreach($headers as $key => $header)
            {
                $temp = explode(": ", $header);
                if(isset($temp[1]))
                {
                    $header_array[$temp[0]] = $temp[1];
                }
                else
                {
                    $header_array[$temp[0]] = '';
                }
            }

            $result = json_decode($response, true);
            $result['header'] = $header_array;
            $result = json_encode($result);
        }
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

//        print_r(curl_errno($ch));die;
//        if($errno = curl_errno($ch)) {
//            abort(500);
//        }
        if($httpcode !== 200){
            if($httpcode === 410){
                abort(404);
            }
            if($httpcode === 406){
                return array('httpcode' => $httpcode, 'data' => json_decode($result, true));
            }
            if($httpcode === 401){
                return array('httpcode' => $httpcode, 'data' => json_decode($result, true));
            }
            return array('httpcode' => $httpcode);
        }

        //close connection
        curl_close($ch);

        if($type === 'array'){
            return json_decode($result, true);
        } else if($type === 'html' || $type === 'json'){
            return $result;
        } else{
            return json_decode($result);
        }
    }

    public static function rewriteUnderscore($text) {
        $text = trim($text);
        $text = str_replace(
            array("’s", "'s",'.',' - ', '...', ',', '- ', ' ', '%', "/", "\\", '"', '?', '<', '>', "#", "^", "`", "'", "=", "!", ":", ",,", "..", "*", "&", "__", "▄", "-", "─", "–", "—", "―", "‗", "−", "─", "(", ")"), 
            array('_', '_', '_', '_', '', '', '_', '_', '', '', '', '', '', '', '', '', '', '', '', '_', '', '_', '', '', '', "va", "_", "_", "_", "", "", "", "", "", "", "", "", ""), 
        $text);

        //$chars = array("a", "A", "e", "E", "o", "O", "u", "U", "i", "I", "d", "D", "y", "Y");
        $chars = array("a", "A", "e", "E", "o", "O", "u", "U", "i", "I", "d", "D", "y", "Y");

        $uni[0] = array("á", "à", "ạ", "ả", "ã", "â", "ấ", "ầ", "ậ", "ẩ", "ẫ", "ă", "ắ", "ằ", "ặ", "ẳ", "� �");
        $uni[1] = array("Á", "À", "Ạ", "Ả", "Ã", "Â", "Ấ", "Ầ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ắ", "Ằ", "Ặ", "Ẳ", "� �");
        $uni[2] = array("é", "è", "ẹ", "ẻ", "ẽ", "ê", "ế", "ề", "ệ", "ể", "ễ", "é");
        $uni[3] = array("É", "È", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ế", "Ề", "Ệ", "Ể", "Ễ");
        $uni[4] = array("ó", "ò", "ọ", "ỏ", "õ", "ô", "ố", "ồ", "ộ", "ổ", "ỗ", "ơ", "ớ", "ờ", "ợ", "ở", "ỡ", "� �");
        $uni[5] = array("Ó", "Ò", "Ọ", "Ỏ", "Õ", "Ô", "Ố", "Ồ", "Ộ", "Ổ", "Ỗ", "Ơ", "Ớ", "Ờ", "Ợ", "Ở", "Ỡ", "� �");
        $uni[6] = array("ú", "ù", "ụ", "ủ", "ũ", "ư", "ứ", "ừ", "ự", "ử", "ữ", "ụ");
        $uni[7] = array("Ú", "Ù", "Ụ", "Ủ", "Ũ", "Ư", "Ứ", "Ừ", "Ự", "Ử", "Ữ");
        $uni[8] = array("í", "ì", "ị", "ỉ", "ĩ");
        $uni[9] = array("Í", "Ì", "Ị", "Ỉ", "Ĩ");
        $uni[10] = array("đ");
        $uni[11] = array("Đ");
        $uni[12] = array("ý", "ỳ", "ỵ", "ỷ", "ỹ", "ỷ");
        $uni[13] = array("Ý", "Ỳ", "Ỵ", "Ỷ", "Ỹ");

        for ($i = 0; $i <= 13; $i++) {
            $text = str_replace($uni[$i], $chars[$i], $text);
        }

        return strtolower($text);
    }

    public static function rewrite($text) {
        $text = trim($text);
        $text = str_replace(
            array("’s", "'s",'.',' - ', '...', ',', '- ', ' ', '%', "/", "\\", '"', '?', '<', '>', "#", "^", "`", "'", "=", "!", ":", ",,", "..", "*", "&", "__", "▄", "-", "─", "–", "—", "―", "‗", "−", "─", "(", ")"), 
            array('-', '-', '-', '-', '', '', '-', '-', '', '', '', '', '', '', '', '', '', '', '', '-', '', '-', '', '', '', "va", "-", "-", "-", "", "", "", "", "", "", "", "", ""), 
        $text);

        //$chars = array("a", "A", "e", "E", "o", "O", "u", "U", "i", "I", "d", "D", "y", "Y");
        $chars = array("a", "A", "e", "E", "o", "O", "u", "U", "i", "I", "d", "D", "y", "Y");

        $uni[0] = array("á", "à", "ạ", "ả", "ã", "â", "ấ", "ầ", "ậ", "ẩ", "ẫ", "ă", "ắ", "ằ", "ặ", "ẳ", "� �");
        $uni[1] = array("Á", "À", "Ạ", "Ả", "Ã", "Â", "Ấ", "Ầ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ắ", "Ằ", "Ặ", "Ẳ", "� �");
        $uni[2] = array("é", "è", "ẹ", "ẻ", "ẽ", "ê", "ế", "ề", "ệ", "ể", "ễ", "é");
        $uni[3] = array("É", "È", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ế", "Ề", "Ệ", "Ể", "Ễ");
        $uni[4] = array("ó", "ò", "ọ", "ỏ", "õ", "ô", "ố", "ồ", "ộ", "ổ", "ỗ", "ơ", "ớ", "ờ", "ợ", "ở", "ỡ", "� �");
        $uni[5] = array("Ó", "Ò", "Ọ", "Ỏ", "Õ", "Ô", "Ố", "Ồ", "Ộ", "Ổ", "Ỗ", "Ơ", "Ớ", "Ờ", "Ợ", "Ở", "Ỡ", "� �");
        $uni[6] = array("ú", "ù", "ụ", "ủ", "ũ", "ư", "ứ", "ừ", "ự", "ử", "ữ", "ụ");
        $uni[7] = array("Ú", "Ù", "Ụ", "Ủ", "Ũ", "Ư", "Ứ", "Ừ", "Ự", "Ử", "Ữ");
        $uni[8] = array("í", "ì", "ị", "ỉ", "ĩ");
        $uni[9] = array("Í", "Ì", "Ị", "Ỉ", "Ĩ");
        $uni[10] = array("đ");
        $uni[11] = array("Đ");
        $uni[12] = array("ý", "ỳ", "ỵ", "ỷ", "ỹ", "ỷ");
        $uni[13] = array("Ý", "Ỳ", "Ỵ", "Ỷ", "Ỹ");

        for ($i = 0; $i <= 13; $i++) {
            $text = str_replace($uni[$i], $chars[$i], $text);
        }

        return strtolower($text);
    }

    public static function getBrowserName() {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $rMsie = '/(msie\s|trident.*rv:)([\w.]+)/i';
        $rEdge = '/(edge)\/([\w.]+)/i';
        $rFirefox = '/(firefox)\/([\w.]+)/i';
        $rOpera = '/(opera).+version\/([\w.]+)/i';
        $rChrome = '/(chrome)\/([\w.]+)/i';
        $rSafari = '/version\/([\w.]+).*(safari)/i';

        if (preg_match($rEdge, $userAgent, $m) > 0) {
            return (object) array( 'model' => $m[1], 'version' => count($m) >= 3? $m[2]: 0 );
        }
        if (preg_match($rMsie, $userAgent, $m) > 0) {
            return (object) array( 'model' => 'IE', 'version' => count($m) >= 3? $m[2]: 0 );
        }
        if (preg_match($rFirefox, $userAgent, $m) > 0) {
            return (object) array( 'model' => $m[1], 'version' => count($m) >= 3? $m[2]: 0 );
        }
        if (preg_match($rOpera, $userAgent, $m) > 0) {
            return (object) array( 'model' => $m[1], 'version' => count($m) >= 3? $m[2]: 0 );
        }
        if (preg_match($rChrome, $userAgent, $m) > 0) {
            return (object) array( 'model' => $m[1], 'version' => count($m) >= 3? $m[2]: 0 );
        }
        if (preg_match($rSafari, $userAgent, $m) > 0) {
            return (object) array( 'model' => $m[2], 'version' => $m[1] );
        }
        return (object) array( 'model' => 'Web', 'version' => 0 );
    }


    public static function curl_dai_ly($url="", $method = "GET", $params = array()) {
        $token = "$2a$10$6JmCf7GhoiLeSZWxC7uK2OjnMot1VYreLhaUOld5A8ue98572osVS";
        $expire_time = time() + 3*3600;
        $header = array(
            "Content-Type: application/json",
            "I-API-CORE:". $token,
        );

        $ch = curl_init();
        if($method == "POST"){
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true); // for post
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));  // for post
        }else{
            curl_setopt($ch, CURLOPT_URL, $url);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $expire_time);
        curl_setopt($ch, CURLOPT_TIMEOUT, $expire_time);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }

}